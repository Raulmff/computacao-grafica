/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>

#include "hpp/ViewPort.hpp"
#include "hpp/Window.hpp"
#include "hpp/DrawingArea.hpp"
#include "hpp/InterfaceMenuObjeto.hpp"
#include "hpp/InterfaceTransformacao.hpp"
#include "hpp/InterfacePrincipal.hpp"
#include "hpp/InterfaceObjeto.hpp"
#include "hpp/Poligono.hpp"
#include "hpp/Ponto.hpp"
#include "hpp/Reta.hpp"
#include "hpp/Objeto.hpp"

InterfaceMenuObjeto *interfaceMenuObjeto;
InterfacePrincipal *interfacePrincipal;
InterfaceObjeto *interfaceObjeto;
InterfaceTransformacao* iTransformacao;
DrawingArea *drawingArea;

Window *window;
ViewPort *viewport;

GtkWidget *windowPrincipal;
GtkBuilder *builder;
cairo_surface_t *surface;

void configure(GtkWidget *widget, GdkEventConfigure *event, gpointer data) {
	g_print("configure\n");

	if (surface)
   	cairo_surface_destroy (surface);

  	surface = gdk_window_create_similar_surface (
		gtk_widget_get_window (widget),
		CAIRO_CONTENT_COLOR,
		gtk_widget_get_allocated_width (widget),
		gtk_widget_get_allocated_height (widget)
	);
	
	cairo_t *cairo;
	cairo = cairo_create (surface);
	
	DrawingArea::set_rgb(cairo, 255, 255, 255);
	
	cairo_paint (cairo);
	cairo_destroy (cairo);
}
void draw_configure(GtkWidget *widget, cairo_t *cairo,  gpointer data) {
	g_print("draw_configure\n");
	
	cairo_set_source_surface (cairo, surface, 0, 0);
	cairo_paint (cairo);
}
void draw() {
	g_print("draw\n");
	viewport->draw(drawingArea, surface, window);
}
void fechar_programa() {
	gtk_main_quit();
}


void iprincipal_objeto_novo() {
	interfaceObjeto->open();
}
void iprincipal_objeto_remover() {}
void iprincipal_button_right() {
	g_print("iprincipal_button_right\n");
	int passo;
	passo = interfacePrincipal->get_window_passo();
	
	window->deslocar_right(passo);
	draw();
}
void iprincipal_button_left() {
	g_print("iprincipal_button_left\n");
	int passo;
	passo = interfacePrincipal->get_window_passo();
	
	window->deslocar_left(passo);
	draw();
}
void iprincipal_button_up() {
	g_print("iprincipal_button_up\n");
	int passo;
	passo = interfacePrincipal->get_window_passo();
	
	window->deslocar_up(passo);
	draw();
}
void iprincipal_button_down() {
	g_print("iprincipal_button_down\n");
	int passo;
	passo = interfacePrincipal->get_window_passo();
	
	window->deslocar_down(passo);
	draw();
}
void iprincipal_button_in() {
	g_print("iprincipal_button_in\n");
	int passo;
	passo = interfacePrincipal->get_window_passo();
	
	window->zoom_in(passo);
	draw();
}
void iprincipal_button_out() {
	g_print("iprincipal_button_out\n");
	int passo;
	passo = interfacePrincipal->get_window_passo();
	
	window->zoom_out(passo);
	draw();
}


void iobjeto_cancelar() {
	interfaceObjeto->hide();
	interfaceObjeto->limpar_campos();
}
void iobjeto_aplicar() {
	Objeto *objeto;
	objeto = interfaceObjeto->get_objeto();
	
	interfaceMenuObjeto->add_objeto( objeto->get_nome(), objeto->get_tipo() );
	window->add_objeto(objeto);
	draw();
	
	iobjeto_cancelar();
}
void iobjeto_poligono_novo_ponto() {
	interfaceObjeto->poligono_novo_ponto();
}
void iobjeto_changed_tipo_radio() {
	interfaceObjeto->changed_tipo_radio();
}
void iobjeto_adicionar() {}
void iobjeto_remover() {}


void itransformacao_aplicar() {
	iTransformacao->transformar_objeto();
	iTransformacao->hide();
	
	draw();
}
void itransformacao_cancelar() {
	iTransformacao->hide();
}
void itransformacao_adicionar() {}
void itransformacao_remover() {}
void itransformacao_changed_rotacao_radio() {
	iTransformacao->changed_rotacao_radio();
}

void load() {
	windowPrincipal = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "window_principal") );
	g_signal_connect (windowPrincipal, "destroy", G_CALLBACK (fechar_programa), NULL);
}


/* TESTE INICIO */

void teste_teste (GtkTreeView *treeview, GtkTreePath *path, GtkTreeViewColumn *col, gpointer userdata) {
	GtkTreeModel *model;
	GtkTreeIter iter;
	
	model = gtk_tree_view_get_model(treeview);
	
	if ( gtk_tree_model_get_iter(model, &iter, path) ) {
		gchar *name;
		
		gtk_tree_model_get(model, &iter, 1, &name, -1);
		
		Objeto *objeto;
		objeto = window->lista_objeto_search(name);
		
		iTransformacao->open(objeto);
		
		g_free(name);
	}
}

/* TESTE FIM */

int main(int argc, char *argv[]) {
	gtk_init(&argc, &argv);
	
	builder = gtk_builder_new();
  	gtk_builder_add_from_file(builder, "interface.glade", NULL);
	
	window = new Window(new Ponto(0,0), new Ponto(500,500));
	viewport = new ViewPort(new Ponto(0,0), new Ponto(250,250));
	
	interfacePrincipal = new InterfacePrincipal(builder);
	interfaceObjeto = new InterfaceObjeto(builder);
	interfaceMenuObjeto = new InterfaceMenuObjeto(builder);
	iTransformacao = new InterfaceTransformacao(builder);
	
	drawingArea = new DrawingArea(builder);
	
	load();
	
	gtk_builder_connect_signals(builder, NULL);
  	gtk_widget_show_all(windowPrincipal);
  	gtk_main ();
	
	return 0;
}
