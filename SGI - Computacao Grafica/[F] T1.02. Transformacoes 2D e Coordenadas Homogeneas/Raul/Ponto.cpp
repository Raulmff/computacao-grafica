/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef PONTO_CPP
#define PONTO_CPP

#include <gtk/gtk.h>
#include <cmath>

#include "hpp/Ponto.hpp"

#define PI 3.14159265

Ponto::Ponto(int x, int y) : Objeto() {
	this->tipo = "PONTO";
	
	this->x = x;
	this->y = y;
	this->z = 1;
}
Ponto::Ponto(std::string nome, int x, int y) {
	this->nome = nome;
	this->tipo = "PONTO";
	
	this->x = x;
	this->y = y;
	this->z = 1;
}

void Ponto::deslocar_x(int x) {
	this->x += x;
}
void Ponto::deslocar_y(int y) {
	this->y += y;
}

void Ponto::set(int x, int y) {
	this->x = x;
	this->y = y;
}
void Ponto::set_x(int x) {
	this->x = x;
}
void Ponto::set_y(int y) {
	this->y = y;
}

int Ponto::get_x() {
	return this->x;
}
int Ponto::get_y() {
	return this->y;
}

void Ponto::transladar(int Dx, int Dy) {
	double matriz1[1][3];
	matriz1[0][0] = this->x; matriz1[0][1] = this->y; matriz1[0][2] = this->z;

	double matriz2[3][3];
	matriz2[0][0] = 1; matriz2[0][1] = 0; matriz2[0][2] = 0;
	matriz2[1][0] = 0; matriz2[1][1] = 1; matriz2[1][2] = 0;
	matriz2[2][0] = Dx; matriz2[2][1] = Dy; matriz2[2][2] = 1;

	double matriz3[1][3];
	matriz3[0][0] = matriz1[0][0] + matriz2[2][0]; matriz3[0][1] = matriz1[0][1] + matriz2[2][1]; matriz3[0][2] = 1;

	this->x = matriz3[0][0];
	this->y = matriz3[0][1];
}

void Ponto::escalonar(int Sx, int Sy, int centroX, int centroY) {

	double  matriz1[1][3];
	matriz1[0][0] = this->x; matriz1[0][1] = this->y; matriz1[0][2] = this->z;

	double  matriz2[3][3];
	matriz2[0][0] = Sx; matriz2[0][1] = 0; matriz2[0][2] = 0;
	matriz2[1][0] = 0; matriz2[1][1] = Sy; matriz2[1][2] = 0;
	matriz2[2][0] = centroX - (Sx*centroY); matriz2[2][1] = centroY - (Sy*centroY); matriz2[2][2] = 1;

	double  matriz3[1][3];
	matriz3[0][0] = (matriz1[0][0]*matriz2[0][0])+matriz2[2][0]; 
	matriz3[0][1] = (matriz1[0][1]*matriz2[1][1])+matriz2[2][1];
	matriz3[0][2] = 1;

	this->x = matriz3[0][0];
	this->y = matriz3[0][1];

}

void Ponto::rotacionar_sobre_ponto(int angulo, Ponto *p) {
	
}

void Ponto::rotacionar_centro_objeto(int teta, int centroX, int centroY) {
	double cosTeta = cos (teta * PI / 180);
	double senTeta = sin (teta*PI/180);

	double matriz1[1][3];
	matriz1[0][0] = this->x; matriz1[0][1] = this->y; matriz1[0][2] = this->z;

	double matriz2[3][3];
	matriz2[0][0] = cosTeta; matriz2[0][1] = senTeta; matriz2[0][2] = 0;
	matriz2[1][0] = senTeta; matriz2[1][1] = cosTeta; matriz2[1][2] = 0;
	matriz2[2][0] = centroX-((centroX*cosTeta) + (centroY*senTeta)); matriz2[2][1] = centroY+((centroX*senTeta)-(centroY*cosTeta)); matriz2[2][2] = 1;

	double matriz3[1][3];
	matriz3[0][0] = (matriz1[0][0]*matriz2[0][0])+(matriz1[0][1]*matriz2[1][0])+(matriz2[2][0]); 
	matriz3[0][1] = (matriz1[0][1]*matriz2[0][0])-(matriz1[0][0]*matriz2[1][0])+(matriz2[2][1]); 
	matriz3[0][2] = 1;

	this->x = matriz3[0][0];
	this->y = matriz3[0][1];
}

void Ponto::rotacionar_origem(int angulo) {
	this->rotacionar_centro_objeto(angulo, 0,0);
}


#endif