/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef POLIGONO_CPP
#define POLIGONO_CPP

#include <gtk/gtk.h>
#include <math.h>
#include <stdlib.h>

#include "hpp/Poligono.hpp"
#include "ListaEnc.cpp"

#define PI 3.14159265


Poligono::Poligono(std::string nome) : Objeto() {
	this->nome = nome;
	this->tipo = "POLIGONO";
	this->lista = new ListaEnc<Ponto*>();
}

void Poligono::add_ponto(Ponto *ponto) {
	this->lista->adiciona(ponto);
}


void Poligono::rotacionar_sobre_ponto(int angulo, Ponto *p) {}






void Poligono::transladar(int Dx, int Dy) {
	//Ponto::translacao(Dx, Dy);
	
	int c;
	for (c = 0; c < this->lista->get_size(); c++) {
		Ponto *p;
		p = this->lista->get(c);
		
		p->transladar(Dx, Dy);
	}
}

void Poligono::escalonar(int Sx, int Sy) {
	//Ponto::escalonar(Sx, Sy);

	int centroX, centroY;
	
	int c;
	for (c = 0; c < this->lista->get_size(); c++) {
		Ponto *p;
		p = this->lista->get(c);
		
		centroX += p->get_x();
		centroY += p->get_y();
	}

	centroX = centroX/c;
	centroY = centroY/c;

	int i;
	for (i = 0; i < this->lista->get_size(); i++) {
		Ponto *p;
		p = this->lista->get(i);
		
		p->escalonar(Sx, Sy, centroX, centroY);
	}
}
/*
void Poligono::rotacionar_sobre_ponto(int angulo, int a, int b) {
	double cosAngulo, senAngulo;
	cosAngulo = cos (angulo * PI / 180);
	senAngulo = sin (angulo * PI / 180);
	
	double centroX, centroY;
	centroX = 0;
	centroY = 0;
	
	int c;
	for (c = 0; c < this->lista->get_size(); c++) {
		Ponto *p;
		p = this->lista->get(c);
		
		centroX += p->get_x();
		centroY += p->get_y();
	}
	
	double centroXN, centroYN;
	centroXN = (centroX*cosAngulo - centroY*senAngulo);
	centroYN = (centroX*senAngulo + centroY*cosAngulo);
	
	
	
	for (c = 0; c < this->lista->get_size(); c++) {
		Ponto *p;
		p = this->lista->get(c);
		
		p->set_x(p->get_x() - centroX - a);
		p->set_y(p->get_y() - centroY - b);
		
		p->rotacionar_origem(angulo);
		
		p->set_x(p->get_x() + centroXN + a);
		p->set_y(p->get_y() + centroYN + b);
	}
	
	*/
	/*
	this->x0 -= centroX + a;
	this->y0 -= centroY + b;
	this->x -= centroX + a;
	this->y -= centroY + b;
	
	double x0, y0;
	x0 = (this->x0*cosAngulo - this->y0*senAngulo);
	y0 = (this->x0*senAngulo + this->y0*cosAngulo);
	
	double x, y;
	x = (this->x*cosAngulo - this->y*senAngulo);
	y = (this->x*senAngulo + this->y*cosAngulo);
	
	this->x0 = x0 + centroXN + a;
	this->y0 = y0 + centroYN + b;
	this->x = x + centroXN + a;
	this->y = y + centroYN + b;*/
//}

void Poligono::rotacionar_centro_objeto(int angulo) {
	double centroX, centroY;
	
	centroX = 0;
	centroY = 0;
	
	int c;
	for (c = 0; c < this->lista->get_size(); c++) {
		Ponto *p;
		p = this->lista->get(c);
		
		centroX += p->get_x();
		centroY += p->get_y();
	}

	centroX = centroX/c;
	centroY = centroY/c;
	
	int i;
	for (i = 0; i < this->lista->get_size(); i++) {
		Ponto *p;
		p = this->lista->get(i);
		
		p->rotacionar_centro_objeto(angulo, centroX, centroY);
	}

}

void Poligono::rotacionar_origem(int angulo) {
	int i;
	for (i = 0; i < this->lista->get_size(); i++) {
		Ponto *p;
		p = this->lista->get(i);
		
		p->rotacionar_origem(angulo);
	}
}

ListaEnc<Ponto*>* Poligono::get_lista() {
	return this->lista;
}

#endif