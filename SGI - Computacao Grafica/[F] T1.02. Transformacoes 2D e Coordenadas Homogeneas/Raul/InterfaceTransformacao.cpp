/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef INTERFACE_TRANSFORMACAO_CPP
#define INTERFACE_TRANSFORMACAO_CPP

#include "hpp/InterfaceTransformacao.hpp"

#include <gtk/gtk.h>

void itransformacao_aplicar();
void itransformacao_cancelar();
void itransformacao_adicionar();
void itransformacao_remover();

void itransformacao_changed_rotacao_radio();

InterfaceTransformacao::InterfaceTransformacao(GtkBuilder *builder) {
	this->load_button(builder);
	this->load_button_radio(builder);
	this->load_entry(builder);
	
	this->window = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "window_objeto_transformacao") );
}

void InterfaceTransformacao::load_button(GtkBuilder *builder) {
	g_print("InterfaceTransformacao: load_button\n");

	this->aplicar = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button_transformacao_aplicar") );
	this->cancelar = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button1") );
	this->adicionar = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button3") );
	this->remover = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button4") );
	
	/* DESATIVADO POR ENQUANTO - INICIO */
	gtk_widget_set_sensitive( this->adicionar, FALSE );
	gtk_widget_set_sensitive( this->remover, FALSE );
	/* DESATIVADO POR ENQUANTO - FIM */
	
	g_signal_connect (this->aplicar, "clicked", G_CALLBACK (itransformacao_aplicar), NULL);
	g_signal_connect (this->cancelar, "clicked", G_CALLBACK (itransformacao_cancelar), NULL);
	g_signal_connect (this->adicionar, "clicked", G_CALLBACK (itransformacao_adicionar), NULL);
	g_signal_connect (this->remover, "clicked", G_CALLBACK (itransformacao_remover), NULL);
}
void InterfaceTransformacao::load_button_radio(GtkBuilder *builder) {
	g_print("InterfaceTransformacao: load_button_radio\n");
	
	this->radioRotOrigem = GTK_RADIO_BUTTON( gtk_builder_get_object(GTK_BUILDER(builder), "radiobutton_rotacao_opcoes_origem") );
	this->radioRotPonto = GTK_RADIO_BUTTON( gtk_builder_get_object(GTK_BUILDER(builder), "radiobutton_rotacao_opcoes_ponto") );
	this->radioRotCentro = GTK_RADIO_BUTTON( gtk_builder_get_object(GTK_BUILDER(builder), "radiobutton_rotacao_opcoes_centro") );
	
	g_signal_connect (this->radioRotOrigem, "toggled", G_CALLBACK (itransformacao_changed_rotacao_radio), NULL); // Equivale ao grupo (radioRotPonto)
	//g_signal_connect (this->radioRotPonto, "toggled", G_CALLBACK (itransformacao_changed_rotacao_radio), NULL);
	g_signal_connect (this->radioRotCentro, "toggled", G_CALLBACK (itransformacao_changed_rotacao_radio), NULL); // Equivale ao grupo (radioRotPonto)
}
void InterfaceTransformacao::load_entry(GtkBuilder *builder) {
	g_print("InterfaceTransformacao: load_entry\n");
	
	this->entryTraHorizontal = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton_translacao_horizontal_valor"));
  	this->entryTraVertical = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton_translacao_vertical_valor"));
	
	this->entryRotAngulo = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton_rotacao_angulo"));
  	this->entryRotX = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton_rotacao_x"));
	this->entryRotY = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton_rotacao_y"));
  	
	this->entryEscX = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton_escalonamento_sx"));
	this->entryEscY = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton_escalonamento_sy"));
}

void InterfaceTransformacao::open(Objeto *objeto) {
	g_print("InterfaceTransformacao: open\n");
	this->objeto = objeto;
	this->limpar_campos();
	
	gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON(this->radioRotOrigem), TRUE );
	this->changed_rotacao_radio();
	
	gtk_widget_show(window);
}
void InterfaceTransformacao::hide() {
	g_print("InterfaceTransformacao: hide\n");
	gtk_widget_hide(window);
}

void InterfaceTransformacao::changed_rotacao_radio() {
	g_print("InterfaceTransformacao: changed_rotacao_radio\n");
	
	if (gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(this->radioRotOrigem) )) {
		this->activate_sobre_ponto(false);
	}
	if (gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(this->radioRotPonto) )) {
		this->activate_sobre_ponto(true);
	}
	if (gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(this->radioRotCentro) )) {
		this->activate_sobre_ponto(false);
	}
}

int InterfaceTransformacao::get_translacao_entry_horizontal() {
	return atoi(gtk_entry_get_text(this->entryTraHorizontal));
}
int InterfaceTransformacao::get_translacao_entry_vertical() {
	return atoi(gtk_entry_get_text(this->entryTraVertical));
}
int InterfaceTransformacao::get_rotacao_entry_angulo() {
	return atoi(gtk_entry_get_text(this->entryRotAngulo));
}
int InterfaceTransformacao::get_rotacao_entry_x() {
	return atoi(gtk_entry_get_text(this->entryRotX));
}
int InterfaceTransformacao::get_rotacao_entry_y() {
	return atoi(gtk_entry_get_text(this->entryRotY));
}
int InterfaceTransformacao::get_escalonamento_entry_x() {
	return atoi(gtk_entry_get_text(this->entryEscX));
}
int InterfaceTransformacao::get_escalonamento_entry_y() {
	return atoi(gtk_entry_get_text(this->entryEscY));
}

void InterfaceTransformacao::limpar_campos() {
	g_print("InterfaceTransformacao: limpar_campos\n");
	
	gtk_entry_set_text(this->entryTraHorizontal, "0");
	gtk_entry_set_text(this->entryTraVertical, "0");
	
	gtk_entry_set_text(this->entryRotAngulo, "0");
	gtk_entry_set_text(this->entryRotX, "0");
	gtk_entry_set_text(this->entryRotY, "0");
	
	gtk_entry_set_text(this->entryEscX, "1");
	gtk_entry_set_text(this->entryEscY, "1");
}
void InterfaceTransformacao::transformar_objeto() {
	int traHorizontal, traVertical;
	traHorizontal = this->get_translacao_entry_horizontal();
	traVertical = this->get_translacao_entry_vertical();
	
	int rotX, rotY, rotAngulo;
	rotX = this->get_rotacao_entry_x();
	rotY = this->get_rotacao_entry_y();
	rotAngulo = this->get_rotacao_entry_angulo();
	
	int escX, escY;
	escX = this->get_escalonamento_entry_x();
	escY = this->get_escalonamento_entry_y();
	
	
	
	g_print("Transladar - Horizontal: %d | Vertical: %d\n", traHorizontal, traVertical);
	this->objeto->transladar(traHorizontal, traVertical);
	
	g_print("Escalonar - X: %d | Y: %d\n", escX, escY);
	this->objeto->escalonar(escX, escY);
	
	g_print("Rotacionar - Angulo: %d\n", rotAngulo);
	if (gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(this->radioRotOrigem) )) {
		this->objeto->rotacionar_origem(rotAngulo);
	}
	if (gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(this->radioRotPonto) )) {
		this->objeto->rotacionar_sobre_ponto(rotAngulo, rotX, rotY);
	}
	if (gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(this->radioRotCentro) )) {
		this->objeto->rotacionar_centro_objeto(rotAngulo);
	}
	
}

void InterfaceTransformacao::activate_sobre_ponto(bool activate) {
	gtk_widget_set_sensitive( (GtkWidget*)this->entryRotX, activate );
	gtk_widget_set_sensitive( (GtkWidget*)this->entryRotY, activate );
}

#endif
