/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef RETA_CPP
#define RETA_CPP

#include <gtk/gtk.h>
#include <math.h>
#include <stdlib.h>

#include "hpp/Reta.hpp"

#define PI 3.14159265

Reta::Reta(Ponto *min, Ponto *max, int z) : Objeto() {
	this->tipo = "RETA";
	
	this->pontoMin = min;
	this->pontoMax = max;

  	this->z = 1;
}
Reta::Reta(std::string nome, Ponto *min, Ponto *max, int z) {
	this->nome = nome;
	this->tipo = "RETA";
	
	this->pontoMin = min;
	this->pontoMax = max;

  	this->z = 1;
}

Ponto* Reta::get_ponto_min() {
	return this->pontoMin;
}
Ponto* Reta::get_ponto_max() {
	return this->pontoMax;
}

void Reta::transladar(int Dx, int Dy) {
	this->pontoMin->transladar(Dx, Dy);
	this->pontoMax->transladar(Dx, Dy);
}

void Reta::escalonar(int Sx, int Sy) {
	int centroX = ((this->pontoMin->get_x() + this->pontoMax->get_x()) / 2);
	int centroY = ((this->pontoMin->get_y() + this->pontoMax->get_y()) / 2);

	this->pontoMin->escalonar(Sx, Sy, centroX, centroY);
	this->pontoMax->escalonar(Sx, Sy, centroX, centroY);
}

void Reta::rotacionar_sobre_ponto(int angulo, Ponto *p) { // (int angulo, int a, int b)
	
}

void Reta::rotacionar_centro_objeto(int angulo) {
	int centroX = ((this->pontoMin->get_x() + this->pontoMax->get_x()) / 2);
	int centroY = ((this->pontoMin->get_y() + this->pontoMax->get_y()) / 2);
	
	this->pontoMin->rotacionar_centro_objeto(angulo, centroX, centroY);
	this->pontoMax->rotacionar_centro_objeto(angulo, centroX, centroY);
}

void Reta::rotacionar_origem(int angulo) {
	this->pontoMin->rotacionar_origem(angulo);
	this->pontoMax->rotacionar_origem(angulo);
}

#endif