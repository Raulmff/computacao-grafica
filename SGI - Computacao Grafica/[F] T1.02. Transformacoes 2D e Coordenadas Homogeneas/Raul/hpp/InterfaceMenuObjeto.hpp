/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef INTERFACE_MENU_OBJETO_HPP
#define INTERFACE_MENU_OBJETO_HPP

#include <gtk/gtk.h>
#include<string>

class InterfaceMenuObjeto {
private:
	GtkTreeView *treeView;
	GtkListStore *listStore;
	
	void load(GtkBuilder *builder);

public:
	InterfaceMenuObjeto(GtkBuilder *builder);
	
	void add_objeto(std::string nome, std::string tipo);

};

#endif
