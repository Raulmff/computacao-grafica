/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef PONTO_HPP
#define PONTO_HPP

#include <gtk/gtk.h>
#include "Objeto.hpp"

class Ponto : public Objeto {
protected:
	int x, y, z;

public:
	Ponto(int x, int y);
	Ponto(std::string nome, int x, int y);
	
	int get_x();
	int get_y();
	
	void set(int x, int y);
	void set_x(int x);
	void set_y(int y);
	
	void deslocar_x(int x);
	void deslocar_y(int y);
	
	void transladar(int Dx, int Dy);
	void escalonar(int Sx, int Sy, int centroX, int centroY);
	
	void rotacionar_origem(int angulo);
	void rotacionar_centro_objeto(int angulo, int centroX, int centroY);
	void rotacionar_sobre_ponto(int angulo, Ponto *p);
	
};

#endif