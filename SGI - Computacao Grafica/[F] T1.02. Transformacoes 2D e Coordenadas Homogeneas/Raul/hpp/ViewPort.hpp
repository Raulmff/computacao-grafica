/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef VIEWPORT_HPP
#define VIEWPORT_HPP

#include<gtk/gtk.h>

class DrawingArea;

#include "Window.hpp"
#include "Ponto.hpp"
#include "Reta.hpp"
#include "Poligono.hpp"
#include "DrawingArea.hpp"

class ViewPort {
private:
	Ponto *pontoMin, *pontoMax;

public:
	ViewPort(Ponto *min, Ponto *max);
	
	void draw(DrawingArea *drawing, cairo_surface_t *surface, Window *window);
	void limpar_tela(cairo_surface_t *surface);
	
	static double transformada_x(double xW, double xWMin, double xWMax, double xVMin, double xVMax);
	static double transformada_y(double yW, double yWMin, double yWMax, double yVMin, double yVMax);
	
	Ponto* get_ponto_min();
	Ponto* get_ponto_max();

};

#endif
