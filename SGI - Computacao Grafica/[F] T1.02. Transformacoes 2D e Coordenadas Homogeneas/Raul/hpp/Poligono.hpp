/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef POLIGONO_HPP
#define POLIGONO_HPP

#include <gtk/gtk.h>

#include "ListaEnc.hpp"
#include "Reta.hpp"
#include "Ponto.hpp"
#include "Objeto.hpp"

class Poligono : public Objeto {
private:
	ListaEnc<Ponto*> *lista;

public:
	Poligono(std::string nome);
	
	void add_ponto(Ponto *ponto);
	ListaEnc<Ponto*>* get_lista();
	
	void transladar(int Dx, int Dy);
	void escalonar(int Sx, int Sy);
	
	void rotacionar_origem(int angulo);
	void rotacionar_centro_objeto(int angulo);
	void rotacionar_sobre_ponto(int angulo, Ponto *p);

};

#endif
