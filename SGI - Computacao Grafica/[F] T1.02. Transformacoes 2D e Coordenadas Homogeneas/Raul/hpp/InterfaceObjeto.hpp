/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef INTERFACE_OBJETO_HPP
#define INTERFACE_OBJETO_HPP

#include <gtk/gtk.h>
#include<string>

#include "Objeto.hpp"
#include "Ponto.hpp"
#include "Reta.hpp"
#include "Poligono.hpp"

class InterfaceObjeto {
private:
	Poligono *poligono;
	GtkWidget *window;
	
	GtkEntry *entryNome;
	GtkEntry *entryPontoX, *entryPontoY;
	GtkEntry *entryPoligonoX, *entryPoligonoY;
	GtkEntry *entryRetaX, *entryRetaY;
	GtkEntry *entryRetaX0, *entryRetaY0;
	
	GtkWidget *aplicar, *cancelar;
	GtkWidget *adicionar, *remover;
	GtkWidget *poligonoNovoPonto;
	
	GtkRadioButton *radioPonto, *radioReta, *radioPoligono;
	
	int nomeContPonto, nomeContReta, nomeContPoligono;
	
	void set_nome(std::string nome);
	
	void activate_ponto(bool active);
	void activate_reta(bool active);
	void activate_poligono(bool active);
	
	void load_button(GtkBuilder *builder);
	void load_button_radio(GtkBuilder *builder);
	void load_entry(GtkBuilder *builder);
	
	int get_ponto_entry_x();
	int get_ponto_entry_y();
	int get_poligono_entry_x();
	int get_poligono_entry_y();
	
	int get_reta_entry_x();
	int get_reta_entry_y();
	int get_reta_entry_x0();
	int get_reta_entry_y0();
	
	std::string get_entry_nome();
	
	Ponto* get_ponto();
	Reta* get_reta();
	Poligono* get_poligono();

public:
	InterfaceObjeto(GtkBuilder *builder);
	
	void open();
	void hide();
	
	void limpar_campos();
	void changed_tipo_radio();
	void poligono_novo_ponto();
	
	Objeto* get_objeto();

};

#endif
