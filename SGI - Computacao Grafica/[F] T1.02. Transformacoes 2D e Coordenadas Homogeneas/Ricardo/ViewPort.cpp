/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef VIEWPORT_CPP
#define VIEWPORT_CPP

#include "hpp/ViewPort.hpp"
#include "ListaEnc.cpp"

ViewPort::ViewPort(Ponto *min, Ponto *max) {
	this->pontoMin = min;
	this->pontoMax = max;
}

void ViewPort::limpar_tela(cairo_surface_t *surface) {
	cairo_t *cairo;
  	cairo = cairo_create (surface);

  	DrawingArea::set_rgb(cairo, 255, 255, 255);
  	
	cairo_paint (cairo);
  	cairo_destroy (cairo);
}
void ViewPort::draw(DrawingArea *drawing, cairo_surface_t *surface, Window *window) {
	this->limpar_tela(surface);
	
	ListaEnc<Objeto*> *lista;
	lista = window->get_objetos();
	
	int c;
	for (c = 0; c < lista->get_size(); c++) {
		Objeto *objeto;
		objeto = lista->get(c);
		
		if (objeto->get_tipo() == "PONTO") {
			drawing->draw_ponto((Ponto*)objeto, surface, window->get_ponto_min(), window->get_ponto_max(), this->get_ponto_min(), this->get_ponto_max());
		}
		if (objeto->get_tipo() == "RETA") {
			drawing->draw_reta((Reta*)objeto, surface, window->get_ponto_min(), window->get_ponto_max(), this->get_ponto_min(), this->get_ponto_max());
		}
		if (objeto->get_tipo() == "POLIGONO") {
			drawing->draw_poligono((Poligono*)objeto, surface, window->get_ponto_min(), window->get_ponto_max(), this->get_ponto_min(), this->get_ponto_max());
		}
	}
	
	drawing->draw();
}

Ponto* ViewPort::get_ponto_min() {
	return this->pontoMin;
}
Ponto* ViewPort::get_ponto_max() {
	return this->pontoMax;
}

double ViewPort::transformada_x(double xW, double xWMin, double xWMax, double xVMin, double xVMax) {
	return ( (xW - xWMin) / (xWMax - xWMin) ) * (xVMax - xVMin);
}
double ViewPort::transformada_y(double yW, double yWMin, double yWMax, double yVMin, double yVMax) {
	return ( 1 - ( (yW - yWMin) / (yWMax - yWMin) ) ) * (yVMax - yVMin);
}

#endif
