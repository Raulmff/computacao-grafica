/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef OBJETO_CPP
#define OBJETO_CPP

#include <gtk/gtk.h>
#include "hpp/Objeto.hpp"

Objeto::Objeto() {
	
}

GtkTreeIter Objeto::get_iter() {
	return this->iter;
}

std::string Objeto::get_nome() {
	return this->nome;
}
std::string Objeto::get_tipo() {
	return this->tipo;
}


void Objeto::transladar(int Dx, int Dy) {}
void Objeto::escalonar(int Sx, int Sy) {}
void Objeto::rotacionar_origem(int angulo) {}
void Objeto::rotacionar_centro_objeto(int angulo) {}
void Objeto::rotacionar_sobre_ponto(int angulo, int x, int y) {}

#endif
