/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef INTERFACE_PRINCIPAL_CPP
#define INTERFACE_PRINCIPAL_CPP

#include<gtk/gtk.h>
#include<string>

#include "hpp/InterfacePrincipal.hpp"

void iprincipal_button_right();
void iprincipal_button_left();
void iprincipal_button_up();
void iprincipal_button_down();

void iprincipal_button_in();
void iprincipal_button_out();

void iprincipal_objeto_novo();

InterfacePrincipal::InterfacePrincipal(GtkBuilder *builder) {
	this->load_button(builder);
	this->load_entry(builder);
}

void InterfacePrincipal::load_button(GtkBuilder *builder) {
	this->buttonRight = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button_window_right") );
	this->buttonLeft = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button_window_left") );
	this->buttonUp = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button_window_up") );
	this->buttonDown = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button_window_down") );
	this->buttonObjetoNovo = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button9") );
	
	g_signal_connect (this->buttonRight, "clicked", G_CALLBACK(iprincipal_button_right), NULL);
	g_signal_connect (this->buttonLeft, "clicked", G_CALLBACK(iprincipal_button_left), NULL);
	g_signal_connect (this->buttonUp, "clicked", G_CALLBACK(iprincipal_button_up), NULL);
	g_signal_connect (this->buttonDown, "clicked", G_CALLBACK(iprincipal_button_down), NULL);
	g_signal_connect (this->buttonObjetoNovo, "clicked", G_CALLBACK(iprincipal_objeto_novo), NULL);
	
	this->buttonIn = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button_window_in") );
	this->buttonOut = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button_window_out") );
	
	g_signal_connect (this->buttonIn, "clicked", G_CALLBACK(iprincipal_button_in), NULL);
	g_signal_connect (this->buttonOut, "clicked", G_CALLBACK(iprincipal_button_out), NULL);
}
void InterfacePrincipal::load_entry(GtkBuilder *builder) {
	this->inputPasso = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton_passo"));
}

int InterfacePrincipal::get_window_passo() {
	return atoi( gtk_entry_get_text(this->inputPasso) );
}
int InterfacePrincipal::get_window_rotacao_grau() {
	// Implementar
	return -1;
}


#endif
