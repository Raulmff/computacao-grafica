/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef INTERFACE_MENU_OBJETO_CPP
#define INTERFACE_MENU_OBJETO_CPP

#include <gtk/gtk.h>
#include "hpp/InterfaceMenuObjeto.hpp"

void teste_teste(GtkTreeView *treeview, GtkTreePath *path, GtkTreeViewColumn *col, gpointer userdata);
void open_window_transformacao();

InterfaceMenuObjeto::InterfaceMenuObjeto(GtkBuilder *builder) {
	this->load(builder);
}

void InterfaceMenuObjeto::load(GtkBuilder *builder) {
	GtkCellRenderer *renderer;
	
	treeView = GTK_TREE_VIEW( gtk_builder_get_object( builder, "treeview3" ) );
	listStore = GTK_LIST_STORE( gtk_builder_get_object( builder, "liststore_objeto" ) );
	
	listStore = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_STRING);
	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_insert_column_with_attributes(treeView, -1, "Nome", renderer, "text", 1, NULL);
	
	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_insert_column_with_attributes(treeView, -1, "Tipo", renderer, "text", 0, NULL);
	
	gtk_tree_view_set_model (treeView, GTK_TREE_MODEL (listStore));
	gtk_tree_view_column_set_min_width ( gtk_tree_view_get_column (treeView, 0), 100 );
	gtk_tree_view_column_set_alignment ( gtk_tree_view_get_column (treeView, 0), 0.5 );
	gtk_tree_view_column_set_alignment ( gtk_tree_view_get_column (treeView, 1), 0.5 );
	
	g_signal_connect (treeView, "row-activated", G_CALLBACK (teste_teste), NULL);
	
	GtkTreeSelection *select;

	select = gtk_tree_view_get_selection (GTK_TREE_VIEW (treeView));
	gtk_tree_selection_set_mode (select, GTK_SELECTION_SINGLE);
	
	//g_signal_connect (G_OBJECT (select), "changed", G_CALLBACK (open_window_transformacao), NULL);
}

void InterfaceMenuObjeto::add_objeto(std::string nome, std::string tipo) {
	GtkTreeIter iter;
	
	gtk_list_store_append(listStore, &iter);
	gtk_list_store_set(listStore, &iter, 1, nome.c_str(), 0, tipo.c_str(), -1);
}

#endif
