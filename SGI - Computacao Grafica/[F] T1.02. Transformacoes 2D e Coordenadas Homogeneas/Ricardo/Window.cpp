/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef WINDOW_CPP
#define WINDOW_CPP

#include<gtk/gtk.h>
#include<typeinfo>

#include "hpp/Window.hpp"
#include "ListaEnc.cpp"

Window::Window(Ponto *min, Ponto *max) {
	this->lista = new ListaEnc<Objeto*>();
	
	this->pontoMin = min;
	this->pontoMax = max;
}

void Window::add_objeto(Objeto *objeto) {
	this->lista->adiciona(objeto);
}


void Window::deslocar_x(int passoX0, int passoX) {
	this->pontoMin->deslocar_x(passoX0);
	this->pontoMax->deslocar_x(passoX);
}
void Window::deslocar_y(int passoY0, int passoY) {
	this->pontoMin->deslocar_y(passoY0);
	this->pontoMax->deslocar_y(passoY);
}

void Window::deslocar_left(int passo) {
	this->deslocar_x(-passo, -passo);
}
void Window::deslocar_right(int passo) {
	this->deslocar_x(passo, passo);
}
void Window::deslocar_up(int passo) {
	this->deslocar_y(passo, passo);
}
void Window::deslocar_down(int passo) {
	this->deslocar_y(-passo, -passo);
}
void Window::zoom_in(int passo) {
	this->deslocar_x(passo, -passo);
	this->deslocar_y(passo, -passo);
}
void Window::zoom_out(int passo) {
	this->deslocar_x(-passo, passo);
	this->deslocar_y(-passo, passo);
}


Ponto* Window::get_ponto_min() {
	return this->pontoMin;
}
Ponto* Window::get_ponto_max() {
	return this->pontoMax;
}

ListaEnc<Objeto*>* Window::get_objetos() {
	return this->lista;
}

Objeto* Window::lista_objeto_search(std::string nome) {
	Objeto *objeto;
	
	int c;
	for (c = 0; c < this->lista->get_size(); c++) {
		objeto = this->lista->get(c);
		
		if (objeto->get_nome() == nome) {
			return objeto;
		}
	}
	
	return NULL;
}

#endif
