/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef INTERFACE_PRINCIPAL_HPP
#define INTERFACE_PRINCIPAL_HPP

#include<gtk/gtk.h>

class InterfacePrincipal {
private:
	GtkWidget *pontoAdicionar, *retaAdicionar, *poligonoAdicionar;
	GtkWidget *poligonoNovo;
	
	GtkEntry *inputPasso;
	
	GtkWidget *buttonIn, *buttonOut;
	GtkWidget *buttonRight, *buttonLeft, *buttonUp, *buttonDown;
	GtkWidget *buttonObjetoNovo;
	
	void load_button(GtkBuilder *builder);
	void load_entry(GtkBuilder *builder);

public:
	InterfacePrincipal(GtkBuilder *builder);
	
	int get_window_passo();
	int get_window_rotacao_grau();

};

#endif