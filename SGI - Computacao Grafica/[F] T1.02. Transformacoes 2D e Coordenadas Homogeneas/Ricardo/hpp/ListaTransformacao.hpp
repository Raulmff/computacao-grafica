/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef LISTA_TRANSFORMACAO_HPP
#define LISTA_TRANSFORMACAO_HPP

#include <gtk/gtk.h>
#include "ListaEnc.hpp"
#include "Ponto.hpp"

class ListaTransformacao {
private:
	//Ponto *objeto_transformado;
	// ListaEnc <Transformacao*> ...

public:
	ListaTransformacao();
	
	void set_objeto_transformado();
	void get_objeto_transformado();

};

#endif
