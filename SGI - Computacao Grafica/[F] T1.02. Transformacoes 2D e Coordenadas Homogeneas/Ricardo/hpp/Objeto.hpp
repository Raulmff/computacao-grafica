/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef OBJETO_HPP
#define OBJETO_HPP

#include <gtk/gtk.h>
#include <string>

class Objeto {
protected:
	GtkEntry *inputX, *inputY;
	GtkEntry *inputNome;
	
	GtkTreeIter iter;
	
	std::string tipo;
	std::string nome;

public:
	Objeto();
	
	std::string get_nome();
	std::string get_tipo();
	
	GtkTreeIter get_iter();
	
	virtual void transladar(int Dx, int Dy);
	virtual void escalonar(int Sx, int Sy);
	
	virtual void rotacionar_origem(int angulo);
	virtual void rotacionar_centro_objeto(int angulo);
	virtual void rotacionar_sobre_ponto(int angulo, int x, int y);
	
};

#endif