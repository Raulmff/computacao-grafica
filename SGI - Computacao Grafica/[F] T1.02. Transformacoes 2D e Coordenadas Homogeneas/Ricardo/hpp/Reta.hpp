/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef RETA_HPP
#define RETA_HPP

#include <gtk/gtk.h>

#include "Ponto.hpp"
#include "Objeto.hpp"

class Reta : public Objeto {
private:
	Ponto *pontoMin, *pontoMax;
	int z;

public:
	Reta(std::string nome, Ponto *min, Ponto *max, int z);
	Reta(Ponto *min, Ponto *max, int z);
	
	Ponto* get_ponto_min();
	Ponto* get_ponto_max();
	
	void transladar(int Dx, int Dy);
	void escalonar(int Sx, int Sy);
	
	void rotacionar_origem(int angulo);
	void rotacionar_centro_objeto(int angulo);
	void rotacionar_sobre_ponto(int angulo, int x, int y);
	
};

#endif