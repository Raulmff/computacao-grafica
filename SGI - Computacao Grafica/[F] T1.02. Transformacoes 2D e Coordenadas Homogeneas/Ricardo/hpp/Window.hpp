/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef WINDOW_HPP
#define WINDOW_HPP

#include<gtk/gtk.h>
#include<string>

#include "ListaEnc.hpp"
#include "Objeto.hpp"
#include "Ponto.hpp"
#include "Poligono.hpp"
#include "Reta.hpp"

class Window {
private:
	ListaEnc<Objeto*> *lista;
	
	Ponto *pontoMin, *pontoMax;
	
	void deslocar_x(int passoX0, int passoX);
	void deslocar_y(int passoY0, int passoY);

public:
	Window(Ponto *min, Ponto *max);
	
	void add_objeto(Objeto* objeto);
	
	void deslocar_left(int passo);
	void deslocar_right(int passo);
	void deslocar_up(int passo);
	void deslocar_down(int passo);
	
	void zoom_in(int passo);
	void zoom_out(int passo);
	
	Ponto* get_ponto_min();
	Ponto* get_ponto_max();
	
	ListaEnc<Objeto*>* get_objetos();
	Objeto* lista_objeto_search(std::string nome);

};

#endif