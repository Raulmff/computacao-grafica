/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef INTERFACE_TRANSFORMACAO_HPP
#define INTERFACE_TRANSFORMACAO_HPP

#include <gtk/gtk.h>
#include "Objeto.hpp"

class InterfaceTransformacao {
private:
	Objeto *objeto;
	GtkWidget *window;
	
	GtkEntry *entryTraHorizontal, *entryTraVertical;
	GtkEntry *entryRotAngulo;
	GtkEntry *entryRotX, *entryRotY;
	GtkEntry *entryEscX, *entryEscY;
	
	GtkWidget *aplicar, *cancelar;
	GtkWidget *adicionar, *remover;
	
	GtkRadioButton *radioRotOrigem, *radioRotPonto, *radioRotCentro;
	
	void load_button(GtkBuilder *builder);
	void load_button_radio(GtkBuilder *builder);
	void load_entry(GtkBuilder *builder);
	
	void limpar_campos();
	void activate_sobre_ponto(bool activate);
	
	int get_translacao_entry_horizontal();
	int get_translacao_entry_vertical();
	int get_rotacao_entry_angulo();
	int get_rotacao_entry_x();
	int get_rotacao_entry_y();
	int get_escalonamento_entry_x();
	int get_escalonamento_entry_y();

public:
	InterfaceTransformacao(GtkBuilder *builder);
	
	void open(Objeto *objeto);
	void hide();
	
	void changed_rotacao_radio();
	void transformar_objeto();

};

#endif
