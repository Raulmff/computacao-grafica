/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef INTERFACE_OBJETO_CPP
#define INTERFACE_OBJETO_CPP

#include <gtk/gtk.h>
#include "hpp/InterfaceObjeto.hpp"

void iobjeto_aplicar();
void iobjeto_cancelar();
void iobjeto_adicionar();
void iobjeto_remover();
void iobjeto_poligono_novo_ponto();

void iobjeto_changed_tipo_radio();

InterfaceObjeto::InterfaceObjeto(GtkBuilder *builder) {
	g_print("InterfaceObjeto: InterfaceObjeto\n");
	this->load_button(builder);
	this->load_button_radio(builder);
	this->load_entry(builder);
	
	this->window = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "window1") );
	//g_signal_connect (this->window, "destroy", G_CALLBACK (iobjeto_cancelar), NULL);
	
	this->nomeContPonto = 0;
	this->nomeContReta = 0;
	this->nomeContPoligono = 0;
}

void InterfaceObjeto::load_button(GtkBuilder *builder) {
	g_print("InterfaceObjeto: load_button\n");
	
	this->aplicar = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button2") );
	this->cancelar = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button5") );
	this->adicionar = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button6") );
	this->remover = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button7") );
	this->poligonoNovoPonto = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button10") );
	
	/* DESATIVADO POR ENQUANTO - INICIO */
	gtk_widget_set_sensitive( this->adicionar, FALSE );
	gtk_widget_set_sensitive( this->remover, FALSE );
	/* DESATIVADO POR ENQUANTO - FIM */
	
	g_signal_connect (this->aplicar, "clicked", G_CALLBACK (iobjeto_aplicar), NULL);
	g_signal_connect (this->cancelar, "clicked", G_CALLBACK (iobjeto_cancelar), NULL);
	g_signal_connect (this->adicionar, "clicked", G_CALLBACK (iobjeto_adicionar), NULL);
	g_signal_connect (this->remover, "clicked", G_CALLBACK (iobjeto_remover), NULL);
	g_signal_connect (this->poligonoNovoPonto, "clicked", G_CALLBACK (iobjeto_poligono_novo_ponto), NULL);
}
void InterfaceObjeto::load_button_radio(GtkBuilder *builder) {
	g_print("InterfaceObjeto: load_button_radio\n");
	
	this->radioPonto = GTK_RADIO_BUTTON( gtk_builder_get_object(GTK_BUILDER(builder), "radiobutton6") );
	this->radioReta = GTK_RADIO_BUTTON( gtk_builder_get_object(GTK_BUILDER(builder), "radiobutton7") );
	this->radioPoligono = GTK_RADIO_BUTTON( gtk_builder_get_object(GTK_BUILDER(builder), "radiobutton8") );
	
	g_signal_connect (this->radioPonto, "toggled", G_CALLBACK (iobjeto_changed_tipo_radio), NULL); // Equivale ao grupo (radioReta)
	//g_signal_connect (this->radioReta, "toggled", G_CALLBACK (iobjeto_changed_tipo_radio), NULL);
	g_signal_connect (this->radioPoligono, "toggled", G_CALLBACK (iobjeto_changed_tipo_radio), NULL); // Equivale ao grupo (radioReta)
}
void InterfaceObjeto::load_entry(GtkBuilder *builder) {
	g_print("InterfaceObjeto: load_entry\n");
	this->entryNome = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "entry2"));
	
	this->entryPontoX = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton9"));
  	this->entryPontoY = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton10"));
	
	this->entryPoligonoX = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton15"));
  	this->entryPoligonoY = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton16"));
	
	this->entryRetaX0 = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton11"));
  	this->entryRetaY0 = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton12"));
	this->entryRetaX = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton13"));
  	this->entryRetaY = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton14"));
}

void InterfaceObjeto::open() {
	g_print("InterfaceObjeto: open\n");
	this->limpar_campos();
	
	gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON(this->radioPonto), TRUE );
	this->changed_tipo_radio();
	
	gtk_widget_show(window);
}
void InterfaceObjeto::hide() {
	g_print("InterfaceObjeto: hide\n");
	gtk_widget_hide(window);
}

void InterfaceObjeto::activate_ponto(bool activate) {
	gtk_widget_set_sensitive( (GtkWidget*)this->entryPontoX, activate );
	gtk_widget_set_sensitive( (GtkWidget*)this->entryPontoY, activate );
}
void InterfaceObjeto::activate_reta(bool activate) {
	gtk_widget_set_sensitive( (GtkWidget*)this->entryRetaX, activate );
	gtk_widget_set_sensitive( (GtkWidget*)this->entryRetaY, activate );
	gtk_widget_set_sensitive( (GtkWidget*)this->entryRetaX0, activate );
	gtk_widget_set_sensitive( (GtkWidget*)this->entryRetaY0, activate );
}
void InterfaceObjeto::activate_poligono(bool activate) {
	gtk_widget_set_sensitive( (GtkWidget*)this->entryPoligonoX, activate );
	gtk_widget_set_sensitive( (GtkWidget*)this->entryPoligonoY, activate );
}

Ponto* InterfaceObjeto::get_ponto() {
	std::string nome;
	nome = get_entry_nome();
	
	this->nomeContPonto++;
	
	int x, y;
	x = get_ponto_entry_x();
	y = get_ponto_entry_y();
	
	return new Ponto(nome, x, y);
}
Reta* InterfaceObjeto::get_reta() {
	std::string nome;
	nome = get_entry_nome();
	
	this->nomeContReta++;
	
	int x0, y0;
	x0 = get_reta_entry_x0();
	y0 = get_reta_entry_y0();
	
	int x, y;
	x = get_reta_entry_x();
	y = get_reta_entry_y();
	
	Ponto *min, *max;
	min = new Ponto(x0, y0);
	max = new Ponto(x, y);
	
	return new Reta(nome, min, max, 1);
}
Poligono* InterfaceObjeto::get_poligono() {
	this->poligono_novo_ponto(); // Resgatar o ultimo ponto (nao foi adicionado com botao adicionar)
	
	this->nomeContPoligono++;
	
	return this->poligono;
}
void InterfaceObjeto::poligono_novo_ponto() {
	int x, y;
	x = get_poligono_entry_x();
	y = get_poligono_entry_y();
	
	this->limpar_campos();
	
	this->poligono->add_ponto( new Ponto(x, y) );
}

void InterfaceObjeto::changed_tipo_radio() {
	g_print("InterfaceObjeto: changed_tipo_radio\n");
	
	GtkToggleButton *togle = NULL;
	
	if (gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(this->radioPonto) )) {
		togle = GTK_TOGGLE_BUTTON(this->radioPonto);
		
		std::string nome;
		nome = "ponto" + std::to_string(this->nomeContPonto);
		
		this->set_nome(nome);
		
		this->activate_ponto(true);
		this->activate_reta(false);
		this->activate_poligono(false);
	}
	if (gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(this->radioReta) )) {
		togle = GTK_TOGGLE_BUTTON(this->radioReta);
		
		std::string nome;
		nome = "reta" + std::to_string(this->nomeContReta);
		
		this->set_nome(nome);
		
		this->activate_ponto(false);
		this->activate_reta(true);
		this->activate_poligono(false);
	}
	if (gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(this->radioPoligono) )) {
		togle = GTK_TOGGLE_BUTTON(this->radioPoligono);
		
		std::string nome;
		nome = "poligono" + std::to_string(this->nomeContPoligono);
		
		this->set_nome(nome);
		this->poligono = new Poligono(nome);
		
		this->activate_ponto(false);
		this->activate_reta(false);
		this->activate_poligono(true);
	}
}

void InterfaceObjeto::set_nome(std::string nome) {
	gchar* n;
	n = (gchar*) nome.c_str();

	gtk_entry_set_text(this->entryNome, n);
}
std::string InterfaceObjeto::get_entry_nome() {
	return gtk_entry_get_text(this->entryNome);
}
int InterfaceObjeto::get_ponto_entry_x() {
	return atoi(gtk_entry_get_text(this->entryPontoX));
}
int InterfaceObjeto::get_ponto_entry_y() {
	return atoi(gtk_entry_get_text(this->entryPontoY));
}
int InterfaceObjeto::get_poligono_entry_x() {
	return atoi(gtk_entry_get_text(this->entryPoligonoX));
}
int InterfaceObjeto::get_poligono_entry_y() {
	return atoi(gtk_entry_get_text(this->entryPoligonoY));
}
int InterfaceObjeto::get_reta_entry_x() {
	return atoi(gtk_entry_get_text(this->entryRetaX));
}
int InterfaceObjeto::get_reta_entry_y() {
	return atoi(gtk_entry_get_text(this->entryRetaY));
}
int InterfaceObjeto::get_reta_entry_x0() {
	return atoi(gtk_entry_get_text(this->entryRetaX0));
}
int InterfaceObjeto::get_reta_entry_y0() {
	return atoi(gtk_entry_get_text(this->entryRetaY0));
}
Objeto* InterfaceObjeto::get_objeto() {
	if (gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(this->radioPonto) )) {
		return this->get_ponto();
	}
	if (gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(this->radioReta) )) {
		return this->get_reta();
	}
	if (gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(this->radioPoligono) )) {
		return this->get_poligono();
	}
}

void InterfaceObjeto::limpar_campos() {
	g_print("InterfaceObjeto: limpar_campos\n");
	gtk_entry_set_text(this->entryNome, "");
	
	gtk_entry_set_text(this->entryPontoX, "0");
	gtk_entry_set_text(this->entryPontoY, "0");
	
	gtk_entry_set_text(this->entryPoligonoX, "0");
	gtk_entry_set_text(this->entryPoligonoY, "0");
	
	gtk_entry_set_text(this->entryRetaX, "0");
	gtk_entry_set_text(this->entryRetaY, "0");
	gtk_entry_set_text(this->entryRetaX0, "0");
	gtk_entry_set_text(this->entryRetaY0, "0");
}

#endif
