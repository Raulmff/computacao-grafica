/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef OBJETO_CPP
#define OBJETO_CPP

#include "hpp/Objeto.hpp"

Objeto::Objeto() {
	this->tipo = OBJETO;
}
void Objeto::set_nome(std::string nome) {
	this->nome = nome;
}

std::string Objeto::get_nome() {
	return this->nome;
}
std::string Objeto::get_tipo_to_string() {
	switch (this->tipo) {
		case PONTO:
			return "PONTO";
		case RETA:
			return "RETA";
		case POLIGONO:
			return "POLIGONO";
		case CURVA:
			return "CURVA";
		default:
			return "OBJETO";
	}
}
Object Objeto::get_tipo() {
	return this->tipo;
}

void Objeto::transladar(double dX, double dY, double dZ) {
	g_print("Objeto - Metodo nao implementado\n");
}
void Objeto::escalonar(double sX, double sY, double sZ) {
	g_print("Objeto - Metodo nao implementado\n");
}
void Objeto::rotacionar_origem(double angulo) {
	g_print("Objeto - Metodo nao implementado\n");
}
void Objeto::rotacionar_centro_objeto(double angulo) {
	g_print("Objeto - Metodo nao implementado\n");
}
void Objeto::rotacionar_sobre_ponto(double angulo, double x, double y, double z) {
	g_print("Objeto - Metodo nao implementado\n");
}

void Objeto::recalcular_scn() {
	g_print("Objeto - Metodo nao implementado\n");
}
void Objeto::transladar_scn(double dX, double dY, double dZ) {
	g_print("Objeto - Metodo nao implementado\n");
}
void Objeto::escalonar_scn(double sX, double sY, double sZ) {
	g_print("Objeto - Metodo nao implementado\n");
}
void Objeto::rotacionar_scn(double angulo, double xC, double yC, double zC) {
	g_print("Objeto - Metodo nao implementado\n");
}







/* ARRUMAR DEPOIS */

void Objeto::transladar_3D(double dX, double dY, double dZ) {
	g_print("Objeto - Metodo nao implementado\n");
}
void Objeto::escalonar_3D(double sX, double sY, double sZ) {
	g_print("Objeto - Metodo nao implementado\n");
}






#endif
