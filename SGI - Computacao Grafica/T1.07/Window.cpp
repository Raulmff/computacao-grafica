/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef WINDOW_CPP
#define WINDOW_CPP

#include<typeinfo>

#include "hpp/Window.hpp"
#include "ListaEnc.cpp"

Window::Window(double width, double height) {
	this->lista = new ListaEnc<Objeto*>();
	
	this->pontoMin = new Ponto(-1,-1,-1);
	this->pontoMax = new Ponto(1,1,1);
	this->pontoCentro = new Ponto(0, 0, 0);

	this->width = width;
	this->height = height;

	this->anguloX = 0;
	this->anguloY = 0;
	this->anguloZ = 0;

	this->transladarX = 0;
	this->transladarY = 0;
	this->transladarZ = 0;
}

void Window::add_objeto(Objeto *objeto) {
	g_print("Window::add_objeto\n");
	this->lista->adiciona(objeto);
	this->recalcular_scn();
}

void Window::deslocar_left(double passo) {
	this->transladar(-passo, 0, 0);
}
void Window::deslocar_right(double passo) {
	this->transladar(passo, 0, 0);
}
void Window::deslocar_up(double passo) {
	this->transladar(0, passo, 0);
}
void Window::deslocar_down(double passo) {
	this->transladar(0, -passo, 0);
}
void Window::zoom_in(double passo) {
	passo /= 100;
	
	this->pontoMin->transladar(passo, passo, passo);
	this->pontoMax->transladar(-passo, -passo, -passo);

	this->recalcular_scn();
}
void Window::zoom_out(double passo) {
	passo /= 100;

	this->pontoMin->transladar(-passo, -passo, -passo);
	this->pontoMax->transladar(passo, passo, passo);

	this->recalcular_scn();
}
void Window::transladar(double dX, double dY, double dZ) {
	this->transladarX += dX;
	this->transladarY += dY;
	this->transladarZ += dZ;

	this->recalcular_scn();	
}
void Window::recalcular_scn() {
	double transladarX, transladarY;
	// Translada o mundo para o centro da window e soma com o deslocamento do usuario
	transladarX = this->transladarX + ( 2 / (this->width) );
	transladarY = this->transladarY + ( 2 / (this->height) );
	transladarZ = this->transladarZ;

	double escalaX, escalaY, escalaZ;
	// Escalona o mundo para caber dentro da window
	escalaX = 2 / (this->width);
	escalaY = 2 / (this->height);
	escalaZ = 1;

	Objeto *objeto;
	int c;
	for (c = 0; c < this->lista->get_size(); c++) {
		objeto = this->lista->get(c);

		objeto->recalcular_scn();
		objeto->transladar_scn(-transladarX, -transladarY, -transladarZ);
		objeto->rotacionar_scn(-this->anguloY, this->pontoCentro->get_x(), this->pontoCentro->get_y(), this->pontoCentro->get_z());
		objeto->escalonar_scn(escalaX, escalaY, escalaZ);
	}
}
void Window::rotacionar(double angulo) {
	/*
	switch(e) {
		case X:
			this->anguloX += angulo;
			if (this->anguloX >= 360) {
				this->anguloX -= 360;
			}
			break;
		case Y:
			this->anguloY += angulo;
			if (this->anguloY >= 360) {
				this->anguloY -= 360;
			}
			break;
		case Z:
			this->anguloZ += angulo;
			if (this->anguloZ >= 360) {
				this->anguloZ -= 360;
			}
			break;
	}
	*/

	this->anguloY += angulo;
	if (this->anguloY >= 360) {
		this->anguloY -= 360;
	}

	this->recalcular_scn();

	g_print("Window::rotacionar(%g, %g, %g)\n", this->anguloX, this->anguloY, this->anguloZ);
}

Ponto* Window::get_ponto_min() {
	return this->pontoMin;
}
Ponto* Window::get_ponto_max() {
	return this->pontoMax;
}
Ponto* Window::get_ponto_centro() {
	return this->pontoCentro;
}

ListaEnc<Objeto*>* Window::get_objetos() {
	return this->lista;
}

Objeto* Window::lista_objeto_search(std::string nome) {
	Objeto *objeto;
	
	int c;
	for (c = 0; c < this->lista->get_size(); c++) {
		objeto = this->lista->get(c);
		
		if (objeto->get_nome() == nome) {
			return objeto;
		}
	}
	
	return NULL;
}

#endif
