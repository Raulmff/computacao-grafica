/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef INTERFACE_TRANSFORMACAO_CPP
#define INTERFACE_TRANSFORMACAO_CPP

#include "hpp/InterfaceTransformacao.hpp"

void itransformacao_aplicar();
void itransformacao_cancelar();
void itransformacao_adicionar();
void itransformacao_remover();

void itransformacao_changed_rotacao_radio();

InterfaceTransformacao::InterfaceTransformacao(GtkBuilder *builder) {
	g_print("InterfaceTransformacao::InterfaceTransformacao\n");
	this->load_button(builder);
	this->load_button_radio(builder);
	this->load_entry(builder);
	
	this->window = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "window_objeto_transformacao") );
}

void InterfaceTransformacao::load_button(GtkBuilder *builder) {
	//g_print("InterfaceTransformacao: load_button\n");

	this->aplicar = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button_transformacao_aplicar") );
	this->cancelar = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button1") );
	
	g_signal_connect (this->aplicar, "clicked", G_CALLBACK (itransformacao_aplicar), NULL);
	g_signal_connect (this->cancelar, "clicked", G_CALLBACK (itransformacao_cancelar), NULL);
}
void InterfaceTransformacao::load_button_radio(GtkBuilder *builder) {
	//g_print("InterfaceTransformacao: load_button_radio\n");
	
	this->radioRotOrigem = GTK_RADIO_BUTTON( gtk_builder_get_object(GTK_BUILDER(builder), "radiobutton_rotacao_opcoes_origem") );
	this->radioRotPonto = GTK_RADIO_BUTTON( gtk_builder_get_object(GTK_BUILDER(builder), "radiobutton_rotacao_opcoes_ponto") );
	this->radioRotCentro = GTK_RADIO_BUTTON( gtk_builder_get_object(GTK_BUILDER(builder), "radiobutton_rotacao_opcoes_centro") );
	
	g_signal_connect (this->radioRotOrigem, "toggled", G_CALLBACK (itransformacao_changed_rotacao_radio), NULL); // Equivale ao grupo (radioRotPonto)
	//g_signal_connect (this->radioRotPonto, "toggled", G_CALLBACK (itransformacao_changed_rotacao_radio), NULL);
	g_signal_connect (this->radioRotCentro, "toggled", G_CALLBACK (itransformacao_changed_rotacao_radio), NULL); // Equivale ao grupo (radioRotPonto)




	gtk_widget_set_sensitive( (GtkWidget*) GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton33")) , false );
	gtk_widget_set_sensitive( (GtkWidget*) GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton34")) , false );
	gtk_widget_set_sensitive( (GtkWidget*) GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton37")) , false );

	gtk_widget_set_sensitive( (GtkWidget*) GTK_RADIO_BUTTON( gtk_builder_get_object(GTK_BUILDER(builder), "radiobutton3") ) , false );
	gtk_widget_set_sensitive( (GtkWidget*) GTK_RADIO_BUTTON( gtk_builder_get_object(GTK_BUILDER(builder), "radiobutton4") ) , false );
	gtk_widget_set_sensitive( (GtkWidget*) GTK_RADIO_BUTTON( gtk_builder_get_object(GTK_BUILDER(builder), "radiobutton5") ) , false );
}
void InterfaceTransformacao::load_entry(GtkBuilder *builder) {
	//g_print("InterfaceTransformacao: load_entry\n");
	
	this->entryTraHorizontal = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton_translacao_horizontal_valor"));
  	this->entryTraVertical = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton_translacao_vertical_valor"));
	this->entryTraProfundidade = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton34"));
	
	this->entryRotAngulo = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton_rotacao_angulo"));
  	this->entryRotX = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton_rotacao_x"));
	this->entryRotY = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton_rotacao_y"));
  	this->entryRotZ = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton33"));

	this->entryEscX = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton35"));
	this->entryEscY = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton36"));
	this->entryEscZ = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton37"));
}

void InterfaceTransformacao::open(Objeto *objeto) {
	//g_print("InterfaceTransformacao: open\n");
	this->objeto = objeto;
	this->limpar_campos();
	
	gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON(this->radioRotCentro), TRUE );
	this->changed_rotacao_radio();
	
	gtk_widget_show(window);
}
void InterfaceTransformacao::hide() {
	//g_print("InterfaceTransformacao: hide\n");
	gtk_widget_hide(window);
}

void InterfaceTransformacao::changed_rotacao_radio() {
	//g_print("InterfaceTransformacao: changed_rotacao_radio\n");
	
	// Se opcao rotacionar sobre ponto estiver ativa -> abre campos para digitar o ponto
	if (gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(this->radioRotPonto) )) {
		this->activate_sobre_ponto(true);
	} else {
		this->activate_sobre_ponto(false);
	}
}

int InterfaceTransformacao::get_translacao_entry_horizontal() {
	return atoi(gtk_entry_get_text(this->entryTraHorizontal));
}
int InterfaceTransformacao::get_translacao_entry_vertical() {
	return atoi(gtk_entry_get_text(this->entryTraVertical));
}
int InterfaceTransformacao::get_translacao_entry_profundidade() {
	return atoi(gtk_entry_get_text(this->entryTraProfundidade));
}
int InterfaceTransformacao::get_rotacao_entry_angulo() {
	return atoi(gtk_entry_get_text(this->entryRotAngulo));
}
int InterfaceTransformacao::get_rotacao_entry_x() {
	return atoi(gtk_entry_get_text(this->entryRotX));
}
int InterfaceTransformacao::get_rotacao_entry_y() {
	return atoi(gtk_entry_get_text(this->entryRotY));
}
int InterfaceTransformacao::get_rotacao_entry_z() {
	return atoi(gtk_entry_get_text(this->entryRotZ));
}
int InterfaceTransformacao::get_escalonamento_entry_x() {
	return atoi(gtk_entry_get_text(this->entryEscX));
}
int InterfaceTransformacao::get_escalonamento_entry_y() {
	return atoi(gtk_entry_get_text(this->entryEscY));
}
int InterfaceTransformacao::get_escalonamento_entry_z() {
	return atoi(gtk_entry_get_text(this->entryEscZ));
}

void InterfaceTransformacao::limpar_campos() {
	//g_print("InterfaceTransformacao: limpar_campos\n");
	
	gtk_entry_set_text(this->entryTraHorizontal, "0");
	gtk_entry_set_text(this->entryTraVertical, "0");
	gtk_entry_set_text(this->entryTraProfundidade, "0");
	
	gtk_entry_set_text(this->entryRotAngulo, "0");
	gtk_entry_set_text(this->entryRotX, "0");
	gtk_entry_set_text(this->entryRotY, "0");
	gtk_entry_set_text(this->entryRotZ, "0");
	
	gtk_entry_set_text(this->entryEscX, "1");
	gtk_entry_set_text(this->entryEscY, "1");
	gtk_entry_set_text(this->entryEscZ, "1");
}
void InterfaceTransformacao::transformar_objeto() {
	double traHorizontal, traVertical, traProfundidade;
	traHorizontal = this->get_translacao_entry_horizontal();
	traVertical = this->get_translacao_entry_vertical();
	traProfundidade = this->get_translacao_entry_profundidade();
	
	double rotX, rotY, rotZ, rotAngulo;
	rotX = this->get_rotacao_entry_x();
	rotY = this->get_rotacao_entry_y();
	rotZ = this->get_rotacao_entry_z();
	rotAngulo = this->get_rotacao_entry_angulo();
	
	double escX, escY, escZ;
	escX = this->get_escalonamento_entry_x();
	escY = this->get_escalonamento_entry_y();
	escZ = this->get_escalonamento_entry_z();
	
	g_print("InterfaceTransformacao::transformar_objeto(Transladar) - Horizontal: %g | Vertical: %g\n", traHorizontal, traVertical);
	this->objeto->transladar(traHorizontal, traVertical, traProfundidade);
	
	g_print("InterfaceTransformacao::transformar_objeto(Escalonar) - X: %g | Y: %g\n", escX, escY);
	this->objeto->escalonar(escX, escY, escZ);
	
	if (gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(this->radioRotOrigem) )) {
		g_print("InterfaceTransformacao::transformar_objeto(Rotacionar - Origem) - Angulo: %g\n", rotAngulo);
		this->objeto->rotacionar_origem(rotAngulo);
	}
	if (gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(this->radioRotPonto) )) {
		g_print("InterfaceTransformacao::transformar_objeto(Rotacionar - Sobre Ponto) - Angulo: %g | P(%g, %g, %g)\n", rotAngulo, rotX, rotY, rotZ);
		this->objeto->rotacionar_sobre_ponto(rotAngulo, rotX, rotY, rotZ);
	}
	if (gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(this->radioRotCentro) )) {
		g_print("InterfaceTransformacao::transformar_objeto(Rotacionar - Centro do objeto) - Angulo: %g\n", rotAngulo);
		this->objeto->rotacionar_centro_objeto(rotAngulo);
	}
}

void InterfaceTransformacao::activate_sobre_ponto(bool activate) {
	gtk_widget_set_sensitive( (GtkWidget*)this->entryRotX, activate );
	gtk_widget_set_sensitive( (GtkWidget*)this->entryRotY, activate );
	gtk_widget_set_sensitive( (GtkWidget*)this->entryRotZ, activate );
}

#endif
