/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef WINDOW_HPP
#define WINDOW_HPP

#include<string>

#include "ListaEnc.hpp"
#include "Objeto.hpp"
#include "Ponto.hpp"
#include "Poligono.hpp"
#include "Reta.hpp"

class Window {
private:
	ListaEnc<Objeto*> *lista;
	
	Ponto *pontoMin, *pontoMax;
	Ponto *pontoCentro;

	double anguloX, anguloY, anguloZ;
	double width, height;
	double transladarX, transladarY, transladarZ;

	/*
	 * Translada a window de acordo com a taxa de deslocamento
	 * da coordenada x e y
	 * \param dX Taxa de deslocamento da coordenada x
	 * \param dY Taxa de deslocamento da coordenada y
	 * \return void
	 */
	void transladar(double dX, double dY, double dZ);

public:
	/*
	 * Cria um objeto da classe Window
	 * \param width Largura da ViewPort
	 * \param height Altura da ViewPort
	 */
	Window(double width, double height);
	/*
	 * Adiciona um objeto no mundo para ser desenhado
	 * na tela
	 * \param objeto Um objeto do mundo
	 * \return void
	 */
	void add_objeto(Objeto* objeto);
	/*
	 * Recalcula as coordenadas normalizadas de todos
	 * os objetos do mundo
	 * \return void
	 */
	void recalcular_scn();
	/*
	 * Translada a window para a esquerda
	 * \param passo Numero de pixels a serem transladados
	 * \return void
	 */
	void deslocar_left(double passo);
	/*
	 * Translada a window para a direita
	 * \param passo Numero de pixels a serem transladados
	 * \return void
	 */
	void deslocar_right(double passo);
	/*
	 * Translada a window para a cima
	 * \param passo Numero de pixels a serem transladados
	 * \return void
	 */
	void deslocar_up(double passo);
	/*
	 * Translada a window para baixo
	 * \param passo Numero de pixels a serem transladados
	 * \return void
	 */
	void deslocar_down(double passo);
	/*
	 * Escalona os objetos do mundo para serem melhor visualizados,
	 * adicionando um efeito de aproximação
	 * \param passo Taxa de aproximação. Ex: passo = 2 => 2x de aproximação
	 * \return void
	 */
	void zoom_in(double passo);
	/*
	 * Escalona os objetos do mundo para serem melhor visualizados,
	 * adicionando um efeito de afastamento
	 * \param passo Taxa de afastamento. Ex: passo = 2 => 2x de afastamento
	 * \return void
	 */
	void zoom_out(double passo);
	/*
	 * Rotaciona os objetos do mundo (utilizando as coordenadas scn de cada um)
	 * para dar um efeito de rotação da window. O angulo de rotação informado
	 * é multiplicado por (-1), pois o efeito somente funciona caso os objetos
	 * rotacionem na direção contrária da window
	 * \param angulo Angulo de rotação da window
	 * \return void
	 */
	void rotacionar(double angulo);
	/*
	 * Retorna o ponto mais a esquerda da window
	 * \return Ponto*
	 */
	Ponto* get_ponto_min();
	/*
	 * Retorna o ponto mais a direita da window
	 * \return Ponto*
	 */
	Ponto* get_ponto_max();
	/*
	 * Retorna o ponto central da window
	 * \return Ponto*
	 */
	Ponto* get_ponto_centro();
	/*
	 * Retorna a lista de objetos do mundo
	 * \return ListaEnc<Objeto*>*
	 */
	ListaEnc<Objeto*>* get_objetos();
	/*
	 * Retorna um objeto da lista de acordo com seu nome (único)
	 * \param nome Nome do objeto procurado
	 * \return Objeto*
	 */
	Objeto* lista_objeto_search(std::string nome);

};

#endif