/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef OBJETO_HPP
#define OBJETO_HPP

#include <gtk/gtk.h>
#include <string>

enum Object {
	PONTO,
	RETA,
	POLIGONO,
	CURVA,
	OBJETO
};

class Objeto {
protected:
	GtkEntry *inputNome, *inputX, *inputY;
	std::string nome;
	Object tipo;

public:
	/*
	 * Cria um objeto da classe Objeto
	 */
	Objeto();
	void set_nome(std::string nome);
	/*
	 * Retorna o nome do objeto
	 * \return std::string
	 */
	std::string get_nome();
	/*
	 * Retorna o tipo do objeto
	 * \return Object
	 */
	Object get_tipo();
	/*
	 * Retorna o tipo do objeto
	 * \return std::string
	 */
	std::string get_tipo_to_string();
	/*
	 * Translada o objeto dentro da window
	 * \param dX Taxa de deslocamento da coordenada x
	 * \param dY Taxa de deslocamento da coordenada y
	 * \return void
	 */
	virtual void transladar(double dX, double dY, double dZ);
	/*
	 * Escalona o objeto dentro da window
	 * \param sX Taxa de escalonamento da coordenada x
	 * \param sY Taxa de escalonamento da coordenada y
	 * \return void
	 */
	virtual void escalonar(double sX, double sY, double sZ);
	/*
	 * Rotaciona o objeto em cima do objeto P(0,0)
	 * \param angulo Angulo de rotação
	 * \return void
	 */
	virtual void rotacionar_origem(double angulo);
	/*
	 * Rotaciona o objeto em cima de si mesmo
	 * \param angulo Angulo de rotação
	 * \return void
	 */
	virtual void rotacionar_centro_objeto(double angulo);
	/*
	 * Rotaciona o objeto em cima de um ponto qualquer
	 * \param angulo Angulo de rotação
	 * \param x Coordenada x do objeto
	 * \param y Coordenada y do objeto
	 * \return void
	 */
	virtual void rotacionar_sobre_ponto(double angulo, double x, double y, double z);
	/*
	 * Inicia o processo de renormalização dos pontos do objeto
	 * \return void
	 */
	virtual void recalcular_scn();
	/*
	 * Translada as coordenadas normalizadas do objeto
	 * \param dX Taxa de translação da coordenada x
	 * \param dY Taxa de translação da coordenada y
	 * \return void
	 */
	virtual void transladar_scn(double dX, double dY, double dZ);
	/*
	 * Escalona as coordenadas normalizadas do objeto
	 * \param sX Taxa de escalonamento da coordenada x
	 * \param sY Taxa de escalonamento da coordenada y
	 * \return void
	 */
	virtual void escalonar_scn(double sX, double sY, double sZ);
	/*
	 * Rotaciona as coordenadas normalizadas do objeto
	 * \param angulo Angulo de rotacionamento
	 * \param xC Coordenada x do ponto central da window
	 * \param yC Coordenada y do ponto central da window
	 * \return void
	 */
	virtual void rotacionar_scn(double angulo, double xC, double yC, double zC);




	/* ARRUMAR DEPOIS */

	virtual void transladar_3D(double dX, double dY, double dZ);
	virtual void escalonar_3D(double sX, double sY, double sZ);
};

#endif