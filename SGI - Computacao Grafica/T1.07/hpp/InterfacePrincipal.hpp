/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef INTERFACE_PRINCIPAL_HPP
#define INTERFACE_PRINCIPAL_HPP

#include<gtk/gtk.h>
#include<string.h>

#include "Ponto.hpp"

class InterfacePrincipal {
private:
	GtkWidget *pontoAdicionar, *retaAdicionar, *poligonoAdicionar;
	GtkWidget *poligonoNovo;
	
	GtkEntry *inputPasso, *inputGraus;
	
	GtkWidget *buttonIn, *buttonOut;
	GtkWidget *buttonRight, *buttonLeft, *buttonUp, *buttonDown;
	GtkWidget *buttonObjetoNovo;
	GtkWidget *buttonRotacionar;

	GtkRadioButton *radioCohen, *radioLB;
	GtkRadioButton *radioEixoX, *radioEixoY, *radioEixoZ;
	
	void load_button(GtkBuilder *builder);
	void load_entry(GtkBuilder *builder);

public:
	InterfacePrincipal(GtkBuilder *builder);
	
	int get_window_passo();
	Eixo get_window_rotacao_tipo();
	int get_window_rotacao_graus();

	std::string get_clipping_radio();

};

#endif