/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef PONTO_HPP
#define PONTO_HPP

#include <gtk/gtk.h>
#include "Objeto.hpp"
#include "Matriz.hpp"

enum Eixo {
	X,
	Y,
	Z
};

class Ponto : public Objeto {
protected:
	double x, y, z;
	double xSCN, ySCN, zSCN;

	/*
	 * Translada o ponto de acordo com a coordenada que é passada,
	 * podendo ser as coordenadas do mundo ou normalizadas
	 * \param x Coordenada x a ser transladada
	 * \param y Coordenada y a ser transladada
	 * \param z Coordenada z a ser transladada
	 * \param dX Taxa de translação da coordenada x
	 * \param dY Taxa de translação da coordenada y
	 * \return void
	 */
	virtual void translacao(double &x, double &y, double &z, double dX, double dY, double dZ);
	/*
	 * Escalona o ponto de acordo com a coordenada que é passada,
	 * podendo ser as coordenadas do mundo ou normalizadas
	 * \param x Coordenada x a ser escalonada
	 * \param y Coordenada y a ser escalonada
	 * \param z Coordenada z a ser escalonada
	 * \param sX Taxa de escalonamento da coordenada x
	 * \param sY Taxa de escalonamento da coordenada y
	 * \return void
	 */
	virtual void escalonamento(double &x, double &y, double &z, double sX, double sY, double sZ);
	/*
	 * Rotaciona o ponto levando em consideração um ponto de
	 * referência, podendo ser coordenadas do mundo ou 
	 * normalizadas (depende do que for passado por parametro)
	 * \param x Coordenada x a ser rotacionada
	 * \param y Coordenada y a ser rotacionada
	 * \param z Coordenada z a ser rotacionada
	 * \param angulo Angulo de rotação
	 * \param pX Coordenada x do ponto de referência
	 * \param pY Coordenada y do ponto de referência
	 * \return void
	 */
	virtual void rotacao(double &x, double &y, double &z, double angulo, double pX, double pY, double pZ);
	//virtual void rotacao_eixo(Eixo eixo, double &x, double &y, double &z, double angulo, double pX, double pY, double pZ);

public:
	Ponto(Ponto* ponto);
	/*
	 * Cria um objeto da classe Ponto
	 * \param nome Nome do ponto
	 * \param x Coordenada x
	 * \param y Coordenada y
	 */
	Ponto(std::string nome, double x, double y, double z);
	/*
	 * Cria um objeto da classe Ponto
	 * \param x Coordenada x
	 * \param y Coordenada y
	 */
	Ponto(double x, double y, double z);
	/*
	 * Retorna a coordenada x do ponto
	 * \return double
	 */
	double get_x();
	/*
	 * Retorna a coordenada y do ponto
	 * \return double
	 */
	double get_y();
	/*
	 * Retorna a coordenada z do ponto
	 * \return double
	 */
	double get_z();
	/*
	 * Retorna a coordenada x normalizada do ponto
	 * \return double
	 */
	double get_scn_x();
	/*
	 * Retorna a coordenada y normalizada do ponto
	 * \return double
	 */
	double get_scn_y();
	/*
	 * Retorna a coordenada z normalizada do ponto
	 * \return double
	 */
	double get_scn_z();

	void set_scn_x(double x);
	void set_scn_y(double y);
	void set_scn_z(double z);
	/*
	 * Translada o ponto dentro da window
	 * \param dX Taxa de deslocamento da coordenada x
	 * \param dY Taxa de deslocamento da coordenada y
	 * \return void
	 */
	void transladar(double dX, double dY, double dZ);
	/*
	 * Escalona o ponto dentro da window
	 * \param sX Taxa de escalonamento da coordenada x
	 * \param sY Taxa de escalonamento da coordenada y
	 * \return void
	 */
	void escalonar(double sX, double sY, double sZ);
	/*
	 * Rotaciona o ponto em cima do ponto P(0,0)
	 * \param angulo Angulo de rotação
	 * \return void
	 */
	void rotacionar_origem(double angulo);
	/*
	 * Rotaciona o ponto em cima de si mesmo
	 * \param angulo Angulo de rotação
	 * \return void
	 */
	void rotacionar_centro_objeto(double angulo);
	/*
	 * Rotaciona o ponto em cima de um ponto qualquer
	 * \param angulo Angulo de rotação
	 * \param x Coordenada x do ponto de referência
	 * \param y Coordenada y do ponto de referência
	 * \return void
	 */
	void rotacionar_sobre_ponto(double angulo, double x, double y, double z);
	/*
	 * Inicia o processo de renormalização das coordenadas do ponto
	 * \return void
	 */
	void recalcular_scn();
	/*
	 * Translada as coordenadas normalizadas do ponto
	 * \param dX Taxa de translação da coordenada x
	 * \param dY Taxa de translação da coordenada y
	 * \return void
	 */
	void transladar_scn(double dX, double dY, double dZ);
	/*
	 * Escalona as coordenadas normalizadas do ponto
	 * \param sX Taxa de escalonamento da coordenada x
	 * \param sY Taxa de escalonamento da coordenada y
	 * \return void
	 */
	void escalonar_scn(double sX, double sY, double sZ);
	/*
	 * Rotaciona as coordenadas normalizadas do ponto
	 * \param angulo Angulo de rotacionamento
	 * \param xC Coordenada x do ponto central da window
	 * \param yC Coordenada y do ponto central da window
	 * \return void
	 */
	void rotacionar_scn(double angulo, double xC, double yC, double zC);
	void get_matriz_rotacao(Eixo e, double angulo, double matriz2[4][4]);

	bool clipping(double x0, double y0, double x, double y);
	bool scn_equal(Ponto* p);

	void calcular_vetor_rc(int rc[4], double xE, double yF, double xD, double yT);
};

#endif