/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef INTERFACE_OBJETO_HPP
#define INTERFACE_OBJETO_HPP

#include <gtk/gtk.h>
#include<string>

#include "Objeto.hpp"
#include "Ponto.hpp"
#include "Reta.hpp"
#include "Poligono.hpp"
#include "Curva2.hpp"

class InterfaceObjeto {
private:
	Poligono *poligono;
	Curva2* curva;

	GtkWidget *window;
	// [0]: Ponto | [1]: Reta | [2]: Poligono | [3]: Curva
	GtkEntry *entryNome[4];

	// [0][]: Ponto | [1][]: Reta | [2][]: Poligono | [3][]: Curva
	// [][0]: X | [][1]: Y | [][2]: Z
	GtkEntry* entryCurva[4][3];
	GtkEntry* entryReta[2][3];
	
	// [0]: X | [1]: Y | [2]: Z
	GtkEntry* entryPonto[3];
	GtkEntry* entryPoligono[3];

	// [0]: Ponto | [1]: Reta | [2]: Poligono | [3]: Curva
	GtkWidget *adicionar[4];
	GtkWidget *resetar[4];

	GtkWidget *fechar;
	GtkWidget *poligonoNovoPonto, *curvaNovoPonto;
	
	GtkRadioButton *radioBezier, *radioSpline;

	// [0]: Ponto | [1]: Reta | [2]: Poligono | [3]: Curva
	int contadorNome[4];

	void set_nome();
	std::string get_entry_nome_ponto();
	std::string get_entry_nome_reta();
	std::string get_entry_nome_poligono();
	std::string get_entry_nome_curva();

	void load_button(GtkBuilder *builder);
	void load_entry(GtkBuilder *builder);
	
	int get_ponto_entry_x();
	int get_ponto_entry_y();
	int get_ponto_entry_z();

	int get_poligono_entry_x();
	int get_poligono_entry_y();
	int get_poligono_entry_z();
	
	int get_reta_entry_x(int i);
	int get_reta_entry_y(int i);
	int get_reta_entry_z(int i);

	int get_curva_entry_x(int i);
	int get_curva_entry_y(int i);
	int get_curva_entry_z(int i);

public:
	InterfaceObjeto(GtkBuilder *builder);
	
	void open();
	void hide();
	
	void limpar_campos();
	void poligono_novo_ponto();
	void curva_novo_ponto();
	
	Ponto* get_ponto();
	Reta* get_reta();
	Poligono* get_poligono();
	Curva2* get_curva();

};

#endif
