/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef INTERFACE_OBJETO_CPP
#define INTERFACE_OBJETO_CPP

#include "hpp/InterfaceObjeto.hpp"


void iobjeto_adicionar_ponto();
void iobjeto_adicionar_reta();
void iobjeto_adicionar_poligono();
void iobjeto_adicionar_curva();

void iobjeto_resetar();

void iobjeto_fechar();

void iobjeto_poligono_novo_ponto();
void iobjeto_curva_novo_ponto();

InterfaceObjeto::InterfaceObjeto(GtkBuilder *builder) {
	//g_print("InterfaceObjeto: InterfaceObjeto\n");
	this->load_button(builder);
	this->load_entry(builder);
	
	this->window = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "window1") );
	//g_signal_connect (this->window, "destroy", G_CALLBACK (iobjeto_cancelar), NULL);
	
	this->radioBezier = GTK_RADIO_BUTTON( gtk_builder_get_object(GTK_BUILDER(builder), "radiobutton15") );
	this->radioSpline = GTK_RADIO_BUTTON( gtk_builder_get_object(GTK_BUILDER(builder), "radiobutton16") );
	


	gtk_widget_set_sensitive( (GtkWidget*) GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton29")) , false );
	gtk_widget_set_sensitive( (GtkWidget*) GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton31")) , false );
	gtk_widget_set_sensitive( (GtkWidget*) GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton30")) , false );
	gtk_widget_set_sensitive( (GtkWidget*) GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton32")) , false );
	gtk_widget_set_sensitive( (GtkWidget*) GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton28")) , false );
	gtk_widget_set_sensitive( (GtkWidget*) GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton26")) , false );
	gtk_widget_set_sensitive( (GtkWidget*) GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton27")) , false );
	gtk_widget_set_sensitive( (GtkWidget*) GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton25")) , false );

	this->contadorNome[0] = 0;
	this->contadorNome[1] = 0;
	this->contadorNome[2] = 0;
	this->contadorNome[3] = 0;
}

void InterfaceObjeto::load_button(GtkBuilder *builder) {
	//g_print("InterfaceObjeto: load_button\n");
	
	this->adicionar[0] = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button23") );
	this->adicionar[1] = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button21") );
	this->adicionar[2] = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button19") );
	this->adicionar[3] = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button17") );
	
	this->resetar[0] = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button22") );
	this->resetar[1] = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button20") );
	this->resetar[2] = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button18") );
	this->resetar[3] = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button16") );

	this->fechar = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button5") );

	this->poligonoNovoPonto = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button10") );
	this->curvaNovoPonto = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button2") );
	
	g_signal_connect (this->adicionar[0], "clicked", G_CALLBACK (iobjeto_adicionar_ponto), NULL);
	g_signal_connect (this->adicionar[1], "clicked", G_CALLBACK (iobjeto_adicionar_reta), NULL);
	g_signal_connect (this->adicionar[2], "clicked", G_CALLBACK (iobjeto_adicionar_poligono), NULL);
	g_signal_connect (this->adicionar[3], "clicked", G_CALLBACK (iobjeto_adicionar_curva), NULL);

	g_signal_connect (this->resetar[0], "clicked", G_CALLBACK (iobjeto_resetar), NULL);
	g_signal_connect (this->resetar[1], "clicked", G_CALLBACK (iobjeto_resetar), NULL);
	g_signal_connect (this->resetar[2], "clicked", G_CALLBACK (iobjeto_resetar), NULL);
	g_signal_connect (this->resetar[3], "clicked", G_CALLBACK (iobjeto_resetar), NULL);

	g_signal_connect (this->fechar, "clicked", G_CALLBACK (iobjeto_fechar), NULL);

	g_signal_connect (this->poligonoNovoPonto, "clicked", G_CALLBACK (iobjeto_poligono_novo_ponto), NULL);
	g_signal_connect (this->curvaNovoPonto, "clicked", G_CALLBACK (iobjeto_curva_novo_ponto), NULL);
}
void InterfaceObjeto::load_entry(GtkBuilder *builder) {
	//g_print("InterfaceObjeto: load_entry\n");

	// Nome
	this->entryNome[0] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "entry1"));
	this->entryNome[1] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "entry3"));
	this->entryNome[2] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "entry4"));
	this->entryNome[3] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "entry5"));

  	// Ponto
  	this->entryPonto[0] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton9"));
  	this->entryPonto[1] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton10"));
  	this->entryPonto[2] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton25"));

  	// Poligono
	this->entryPoligono[0] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton15"));
  	this->entryPoligono[1] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton16"));
  	this->entryPoligono[2] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton28"));

  	// Reta
  	this->entryReta[0][0] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton11"));
  	this->entryReta[0][1] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton12"));
  	this->entryReta[0][2] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton26"));
	
	this->entryReta[1][0] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton13"));
  	this->entryReta[1][1] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton14"));
  	this->entryReta[1][2] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton27"));

  	// Curva
  	this->entryCurva[0][0] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton17"));
  	this->entryCurva[0][1] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton18"));
  	this->entryCurva[0][2] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton29"));

  	this->entryCurva[1][0] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton19"));
  	this->entryCurva[1][1] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton20"));
  	this->entryCurva[1][2] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton31"));

  	this->entryCurva[2][0] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton22"));
  	this->entryCurva[2][1] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton21"));
  	this->entryCurva[2][2] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton30"));

  	this->entryCurva[3][0] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton24"));
  	this->entryCurva[3][1] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton23"));
  	this->entryCurva[3][2] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton32"));
}

void InterfaceObjeto::open() {
	//g_print("InterfaceObjeto: open\n");
	this->set_nome();
	this->limpar_campos();
	gtk_widget_show(window);
}
void InterfaceObjeto::hide() {
	//g_print("InterfaceObjeto: hide\n");
	gtk_widget_hide(window);
}

Ponto* InterfaceObjeto::get_ponto() {
	std::string nome;
	nome = this->get_entry_nome_ponto();
	
	this->contadorNome[0]++;
	this->set_nome();

	int x, y;
	x = get_ponto_entry_x();
	y = get_ponto_entry_y();
	
	this->limpar_campos();

	return new Ponto(nome, x, y, 0);
}
Reta* InterfaceObjeto::get_reta() {
	std::string nome;
	nome = this->get_entry_nome_reta();
	
	this->contadorNome[1]++;
	this->set_nome();
	
	// Ponto P0
	int x, y;
	x = get_reta_entry_x(0);
	y = get_reta_entry_y(0);
	
	Ponto *p0;
	p0 = new Ponto(x, y, 0);
	
	// Ponto P1
	x = get_reta_entry_x(1);
	y = get_reta_entry_y(1);
	
	Ponto *p1;
	p1 = new Ponto(x, y, 0);
	
	this->limpar_campos();

	return new Reta(nome, p0, p1);
}
Poligono* InterfaceObjeto::get_poligono() {
	this->poligono_novo_ponto(); // Resgatar o ultimo ponto (nao foi adicionado com botao adicionar)
	this->contadorNome[2]++;
	this->set_nome();

	Poligono *p;
	p = this->poligono;

	this->limpar_campos();
	return p;
}
Curva2* InterfaceObjeto::get_curva() {
	g_print("InterfaceObjeto::get_curva\n");

	this->curva_novo_ponto();
	

	if (gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(this->radioBezier) )) {
		this->curva->construir_curva_bezier();
	} else {
		this->curva->construir_curva_spline();
	}

	this->contadorNome[3]++;
	this->set_nome();

	Curva2 *c;
	c = this->curva;
	this->limpar_campos();

	return c;
}
void InterfaceObjeto::curva_novo_ponto() {
	std::string nome;
	nome = this->get_entry_nome_curva();

	int n_pontos;

	if (this->curva == NULL) {
		this->curva = new Curva2(nome);
		n_pontos = 4;

		gtk_widget_set_sensitive( (GtkWidget*)this->entryCurva[3][0], false );
		gtk_widget_set_sensitive( (GtkWidget*)this->entryCurva[3][1], false );
		//gtk_widget_set_sensitive( (GtkWidget*)this->entryCurva[3][2], false );
	} else {
		n_pontos = 3;
	}

	double x, y, z;

	int c;
	for (c = 0; c < n_pontos; c++) {
		x = get_curva_entry_x(c);
		y = get_curva_entry_y(c);
		z = get_curva_entry_z(c);
		
		curva->add_ponto( new Ponto(x, y, z) );
	}
}
void InterfaceObjeto::poligono_novo_ponto() {
	int x, y;
	x = get_poligono_entry_x();
	y = get_poligono_entry_y();
	
	this->poligono->add_ponto( new Ponto(x, y, 0) );
}

void InterfaceObjeto::set_nome() {
	std::string nome;
	gchar* nome_c;

	// Ponto
	nome = "ponto" + std::to_string(this->contadorNome[0]);
	nome_c = (gchar*) nome.c_str();
	gtk_entry_set_text(this->entryNome[0], nome_c);

	// Reta
	nome = "reta" + std::to_string(this->contadorNome[1]);
	nome_c = (gchar*) nome.c_str();
	gtk_entry_set_text(this->entryNome[1], nome_c);

	// Poligono
	nome = "poligono" + std::to_string(this->contadorNome[2]);
	nome_c = (gchar*) nome.c_str();
	gtk_entry_set_text(this->entryNome[2], nome_c);

	// Curva
	nome = "curva" + std::to_string(this->contadorNome[3]);
	nome_c = (gchar*) nome.c_str();
	gtk_entry_set_text(this->entryNome[3], nome_c);
}

std::string InterfaceObjeto::get_entry_nome_ponto() {
	return gtk_entry_get_text(this->entryNome[0]);
}
std::string InterfaceObjeto::get_entry_nome_reta() {
	return gtk_entry_get_text(this->entryNome[1]);
}
std::string InterfaceObjeto::get_entry_nome_poligono() {
	return gtk_entry_get_text(this->entryNome[2]);
}
std::string InterfaceObjeto::get_entry_nome_curva() {
	return gtk_entry_get_text(this->entryNome[3]);
}

int InterfaceObjeto::get_ponto_entry_x() {
	return atoi(gtk_entry_get_text(this->entryPonto[0]));
}
int InterfaceObjeto::get_ponto_entry_y() {
	return atoi(gtk_entry_get_text(this->entryPonto[1]));
}
int InterfaceObjeto::get_ponto_entry_z() {
	return atoi(gtk_entry_get_text(this->entryPonto[2]));
}


int InterfaceObjeto::get_poligono_entry_x() {
	return atoi(gtk_entry_get_text(this->entryPoligono[0]));
}
int InterfaceObjeto::get_poligono_entry_y() {
	return atoi(gtk_entry_get_text(this->entryPoligono[1]));
}
int InterfaceObjeto::get_poligono_entry_z() {
	return atoi(gtk_entry_get_text(this->entryPoligono[2]));
}


int InterfaceObjeto::get_reta_entry_x(int i) {
	return atoi(gtk_entry_get_text(this->entryReta[i][0]));
}
int InterfaceObjeto::get_reta_entry_y(int i) {
	return atoi(gtk_entry_get_text(this->entryReta[i][1]));
}
int InterfaceObjeto::get_reta_entry_z(int i) {
	return atoi(gtk_entry_get_text(this->entryReta[i][2]));
}

int InterfaceObjeto::get_curva_entry_x(int i) {
	return atoi(gtk_entry_get_text(this->entryCurva[i][0]));
}
int InterfaceObjeto::get_curva_entry_y(int i) {
	return atoi(gtk_entry_get_text(this->entryCurva[i][1]));
}
int InterfaceObjeto::get_curva_entry_z(int i) {
	return atoi(gtk_entry_get_text(this->entryCurva[i][2]));
}

void InterfaceObjeto::limpar_campos() {
	//g_print("InterfaceObjeto: limpar_campos\n");

	gtk_entry_set_text(this->entryPonto[0], "0");
	gtk_entry_set_text(this->entryPonto[1], "0");
	//gtk_entry_set_text(this->entryPonto[2], "0");
	
	gtk_entry_set_text(this->entryPoligono[0], "0");
	gtk_entry_set_text(this->entryPoligono[1], "0");
	//gtk_entry_set_text(this->entryPoligono[2], "0");

	int c;
	for (c = 0; c < 2; c++) {
		gtk_entry_set_text(this->entryReta[c][0], "0");
		gtk_entry_set_text(this->entryReta[c][1], "0");
		//gtk_entry_set_text(this->entryReta[c][2], "0");
	}

	for (c = 0; c < 4; c++) {
		gtk_entry_set_text(this->entryCurva[c][0], "0");
		gtk_entry_set_text(this->entryCurva[c][1], "0");
		//gtk_entry_set_text(this->entryCurva[c][2], "0");
	}

	gtk_widget_set_sensitive( (GtkWidget*)this->entryCurva[3][0], true );
	gtk_widget_set_sensitive( (GtkWidget*)this->entryCurva[3][1], true );
	//gtk_widget_set_sensitive( (GtkWidget*)this->entryCurva[3][2], true );

	this->curva = NULL;

	std::string nome;
	nome = this->get_entry_nome_poligono();
	this->poligono = new Poligono(nome);
}

#endif
