/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef PONTO_CPP
#define PONTO_CPP

#include <cmath>
#include "hpp/Ponto.hpp"

#define PI 3.14

Ponto::Ponto(Ponto* ponto) {
	this->x = ponto->x;
	this->y = ponto->y;
	this->z = ponto->z;

	this->xSCN = ponto->xSCN;
	this->ySCN = ponto->ySCN;
	this->zSCN = ponto->zSCN;
}
Ponto::Ponto(double x, double y, double z) : Objeto() {
	this->tipo = PONTO;
	
	this->x = x;
	this->y = y;
	this->z = z;

	this->recalcular_scn();
}
Ponto::Ponto(std::string nome, double x, double y, double z) {
	this->nome = nome;
	this->tipo = PONTO;
	
	this->x = x;
	this->y = y;
	this->z = z;

	this->recalcular_scn();
}

double Ponto::get_x() {
	//g_print("Ponto::get_x: %g\n", this->x);
	return this->x;
}
double Ponto::get_y() {
	//g_print("Ponto::get_y: %g\n", this->y);
	return this->y;
}
double Ponto::get_z() {
	//g_print("Ponto::get_z: %g\n", this->z);
	return this->z;
}

double Ponto::get_scn_x() {
	return this->xSCN;
}
double Ponto::get_scn_y() {
	return this->ySCN;
}
double Ponto::get_scn_z() {
	return this->zSCN;
}

void Ponto::set_scn_x(double x) {
	this->xSCN = x;
}
void Ponto::set_scn_y(double y) {
	this->ySCN = y;
}
void Ponto::set_scn_z(double z) {
	this->zSCN = z;
}

void Ponto::translacao(double &x, double &y, double &z, double dX, double dY, double dZ) {
	//g_print("Ponto::transladar - P(%g,%g)", x, y);

	double matriz1[4] = {
		x, y, z, 1
	};
	double matriz2[4][4] = {
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		dX, dY, dZ, 1
	};

	double matriz3[3];
	Matriz::multiplicar_4x4(matriz1, matriz2, matriz3);

	x = matriz3[0];
	y = matriz3[1];
	z = matriz3[2];

	//g_print(" para P(%g; %g; %g)\n", x, y, z);
}
void Ponto::escalonamento(double &x, double &y, double &z, double sX, double sY, double sZ) {
	//g_print("Ponto::escalonamento  - P(%g,%g)", x, y);
	
	double matriz1[4] = {
		x, y, z, 1
	};
	double matriz2[4][4] = {
		sX, 0, 0, 0,
		0, sY, 0, 0,
		0,  0, sZ, 0,
		0,  0, 0, 1,
	};

	double matriz3[4];
	Matriz::multiplicar_4x4(matriz1, matriz2, matriz3);

	x = matriz3[0];
	y = matriz3[1];
	z = matriz3[2];
	
	//g_print(" para P(%g, %g, %g)\n", x, y, z);
}

void Ponto::rotacao(double &x, double &y, double &z, double angulo, double pX, double pY, double pZ) {
	//g_print("Ponto::rotacao    - P(%g,%g)", x, y);

	double cosTeta, senTeta;
	cosTeta = cos ((-angulo) * PI / 180);
	senTeta = sin ((-angulo) * PI / 180);

	x -= pX;
	y -= pY;
	//z -= pZ;

	double matriz1[3] = {
		x, y, 1
	};
	double matriz2[3][3] = {
		cosTeta, -senTeta, 0,
		senTeta, cosTeta, 0,
		0, 0, 1
	};

	double matriz3[3];
	Matriz::multiplicar_3x3(matriz1, matriz2, matriz3);

	x = matriz3[0];
	y = matriz3[1];
	z = matriz3[2];

	x += pX;
	y += pY;
	//z += pZ;
}

/*
void Ponto::rotacao(Eixo e, double &x, double &y, double &z, double anguloX, double anguloY, double anguloZ, double pX, double pY, double pZ) {
	//g_print("Ponto::rotacao - P(%g; %g; %g)", x, y, z);

	// Move para a origem
	this->translacao(x, y, z, -pX, -pY, -pZ);

	// Move o eixo do objeto sobre o plano xy
	rotacao_eixo(X, x, y, z, -anguloX, pX, pY, pZ);
	// Alinha o eixo do objeto com eixo Y
	rotacao_eixo(Z, x, y, z, -anguloZ, pX, pY, pZ);
	// Rotaciona o objeto
	rotacao_eixo(Y, x, y, z, anguloY, pX, pY, pZ);
	// Desfaz as duas primeiras rotacoes
	rotacao_eixo(Z, x, y, z, anguloZ, pX, pY, pZ);
	rotacao_eixo(X, x, y, z, anguloX, pX, pY, pZ);
	
	// Desfaz primeira translacao
	this->translacao(x, y, z, pX, pY, pZ);

	//g_print(" para P(%g, %g, %g)\n", x, y, z);
	g_print("Ponto::rotacao - anguloX: %g | anguloY: %g | anguloZ: %g)\n", anguloX, anguloY, anguloZ);
}
void Ponto::rotacao_eixo(Eixo eixo, double &x, double &y, double &z, double angulo, double pX, double pY, double pZ) {
	double matriz1[4] = {
		x, y, z, 1
	};

	double matriz2[4][4];
	this->get_matriz_rotacao(eixo, -angulo, matriz2);

	double matriz3[4];
	Matriz::multiplicar_4x4(matriz1, matriz2, matriz3);

	x = matriz3[0];
	y = matriz3[1];
	z = matriz3[2];
}
*/

void Ponto::transladar(double dX, double dY, double dZ) {
	this->translacao(this->x, this->y, this->z, dX, dY, dZ);
}
void Ponto::escalonar(double sX, double sY, double sZ) {
	this->escalonamento(this->x, this->y, this->z, sX, sY, sZ);
}
void Ponto::rotacionar_sobre_ponto(double angulo, double x, double y, double z) {
	this->rotacao(this->x, this->y, this->z, angulo, x, y, z);
}
void Ponto::rotacionar_centro_objeto(double angulo) {
	this->rotacionar_sobre_ponto(angulo, this->x, this->y, this->z);
}
void Ponto::rotacionar_origem(double angulo) {
	this->rotacionar_sobre_ponto(angulo, 0, 0, 0);
}

void Ponto::recalcular_scn() {
	this->xSCN = this->x;
	this->ySCN = this->y;
	this->zSCN = this->z;
}
void Ponto::transladar_scn(double dX, double dY, double dZ) {
	this->translacao(this->xSCN, this->ySCN, this->zSCN, dX, dY, dZ);
}
void Ponto::escalonar_scn(double sX, double sY, double sZ) {
	this->escalonamento(this->xSCN, this->ySCN, this->zSCN, sX, sY, sZ);
}
void Ponto::rotacionar_scn(double angulo, double xC, double yC, double zC) {
	this->rotacao(this->xSCN, this->ySCN, this->zSCN, angulo, xC, yC, zC);
}

bool Ponto::clipping(double x0, double y0, double x, double y) {
	if (this->xSCN < x0 || this->xSCN > x) {
		return false;
	}
	if (this->ySCN < y0 || this->ySCN > y) {
		return false;
	}

	return true;
}

bool Ponto::scn_equal(Ponto* p) {
	if (p == NULL) {
		return false;
	}

	if ( (p->xSCN == this->xSCN) && (p->ySCN == this->ySCN) ) {//&& (p->zSCN == this->zSCN) ) {
		//g_print("Ponto::equal_scn - igual\n");
		return true;
	}
	
	return false;
}
void Ponto::get_matriz_rotacao(Eixo e, double angulo, double matriz[4][4]) {
	double cosTeta, senTeta;
	cosTeta = cos ((-angulo) * PI / 180);
	senTeta = sin ((-angulo) * PI / 180);

	switch (e) {
		// Eixo X
		case X: 
			{
				double matrix[4][4] = {
					1, 0, 0, 0,
					0, cosTeta, senTeta, 0,
					0, -senTeta, cosTeta, 0,
					0, 0, 0, 1
				};

				matriz = matrix;
			}

			break;
		// Eixo Y
		case Y:
			{
				double matrix[4][4] = {
					cosTeta, 0, -senTeta, 0,
					0, 1, 0, 0,
					senTeta, 0, cosTeta, 0,
					0, 0, 0, 1
				};

				matriz = matrix;
			}

			break;
		// Eixo Z
		default:
			{
				double matrix[4][4] = {
					cosTeta, senTeta, 0, 0,
					-senTeta, cosTeta, 0, 0,
					0, 0, 1, 0,
					0, 0, 0, 1
				};

				matriz = matrix;
			}

			break;
	}
}

/* ARRUMAR DEPOIS */

// Depois descartar o mesmo metodo na classe Reta
void Ponto::calcular_vetor_rc(int rc[4], double xE, double yF, double xD, double yT) {
	double x, y;
	x = this->get_scn_x();
	y = this->get_scn_y();

	rc[0] = 0;
	rc[1] = 0;
	rc[2] = 0;
	rc[3] = 0;

	if (x < xE) {
		rc[3] = 1;
	} else if (xD < x) {
		rc[2] = 1;
	}

	if (y < yF) {
		rc[1] = 1;
	} else if (yT < y) {
		rc[0] = 1;
	}
}

#endif