/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef INTERFACE_PRINCIPAL_CPP
#define INTERFACE_PRINCIPAL_CPP

#include<string>

#include "hpp/InterfacePrincipal.hpp"

void iprincipal_button_right();
void iprincipal_button_left();
void iprincipal_button_up();
void iprincipal_button_down();

void iprincipal_button_in();
void iprincipal_button_out();

void iprincipal_button_rotacionar();
void iprincipal_objeto_novo();

InterfacePrincipal::InterfacePrincipal(GtkBuilder *builder) {
	this->load_button(builder);
	this->load_entry(builder);
}

void InterfacePrincipal::load_button(GtkBuilder *builder) {
	this->buttonRight = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button_window_right") );
	this->buttonLeft = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button_window_left") );
	this->buttonUp = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button_window_up") );
	this->buttonDown = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button_window_down") );
	this->buttonObjetoNovo = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button9") );
	
	g_signal_connect (this->buttonRight, "clicked", G_CALLBACK(iprincipal_button_right), NULL);
	g_signal_connect (this->buttonLeft, "clicked", G_CALLBACK(iprincipal_button_left), NULL);
	g_signal_connect (this->buttonUp, "clicked", G_CALLBACK(iprincipal_button_up), NULL);
	g_signal_connect (this->buttonDown, "clicked", G_CALLBACK(iprincipal_button_down), NULL);
	g_signal_connect (this->buttonObjetoNovo, "clicked", G_CALLBACK(iprincipal_objeto_novo), NULL);
	
	this->buttonIn = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button_window_in") );
	this->buttonOut = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button_window_out") );
	
	g_signal_connect (this->buttonIn, "clicked", G_CALLBACK(iprincipal_button_in), NULL);
	g_signal_connect (this->buttonOut, "clicked", G_CALLBACK(iprincipal_button_out), NULL);

	this->buttonRotacionar = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button11") );

	g_signal_connect (this->buttonRotacionar, "clicked", G_CALLBACK(iprincipal_button_rotacionar), NULL);

	this->radioCohen = GTK_RADIO_BUTTON( gtk_builder_get_object(GTK_BUILDER(builder), "radiobutton1") );
	this->radioLB = GTK_RADIO_BUTTON( gtk_builder_get_object(GTK_BUILDER(builder), "radiobutton2") );

	this->radioEixoX = GTK_RADIO_BUTTON( gtk_builder_get_object(GTK_BUILDER(builder), "radiobutton12") );
	this->radioEixoY = GTK_RADIO_BUTTON( gtk_builder_get_object(GTK_BUILDER(builder), "radiobutton13") );
	this->radioEixoZ = GTK_RADIO_BUTTON( gtk_builder_get_object(GTK_BUILDER(builder), "radiobutton14") );
}
void InterfacePrincipal::load_entry(GtkBuilder *builder) {
	this->inputPasso = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton_passo"));
	this->inputGraus = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "rotacao_spinbutton"));
}

int InterfacePrincipal::get_window_passo() {
	return atoi( gtk_entry_get_text(this->inputPasso) );
}
int InterfacePrincipal::get_window_rotacao_graus() {
	return atoi( gtk_entry_get_text(this->inputGraus) );
}

Eixo InterfacePrincipal::get_window_rotacao_tipo() {
	if (gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(this->radioEixoX) )) {
		return X;
	}
	if (gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(this->radioEixoY) )) {
		return Y;
	}
	if (gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(this->radioEixoZ) )) {
		return Z;
	}
}
std::string InterfacePrincipal::get_clipping_radio() {
	if (gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(this->radioCohen) )) {
		return "cohen";
	}
	if (gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(this->radioLB) )) {
		return "lb";
	}
}

#endif
