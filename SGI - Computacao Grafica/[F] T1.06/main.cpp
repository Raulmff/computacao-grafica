/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>

#include "hpp/ViewPort.hpp"
#include "hpp/Window.hpp"
#include "hpp/DrawingArea.hpp"
#include "hpp/InterfaceMenuObjeto.hpp"
#include "hpp/InterfaceTransformacao.hpp"
#include "hpp/InterfacePrincipal.hpp"
#include "hpp/InterfaceObjeto.hpp"
#include "hpp/Poligono.hpp"
#include "hpp/Ponto.hpp"
#include "hpp/Reta.hpp"
#include "hpp/Objeto.hpp"
#include "hpp/Curva2.hpp"

InterfaceMenuObjeto *interfaceMenuObjeto;
InterfacePrincipal *interfacePrincipal;
InterfaceObjeto *interfaceObjeto;
InterfaceTransformacao* iTransformacao;
DrawingArea *drawingArea;

Window *window;
ViewPort *viewport;

GtkWidget *windowPrincipal;
GtkBuilder *builder;
cairo_surface_t *surface;

int array_to_int(int array[], int size) {
	std::string str;
	str = "";
	
	int c;
	for (c = 0; c < size; c++) {
		str += std::to_string( array[c] );
	}

	return atoi( str.c_str() );
}

void console_configure() {
	g_print("\n\n");
	g_print("-------------------------------------------------------------");
	g_print("\n\n");
}

void configure(GtkWidget *widget, GdkEventConfigure *event, gpointer data) {
	//g_print("Main::configure\n");

	if (surface)
   	cairo_surface_destroy (surface);

  	surface = gdk_window_create_similar_surface (
		gtk_widget_get_window (widget),
		CAIRO_CONTENT_COLOR,
		gtk_widget_get_allocated_width (widget),
		gtk_widget_get_allocated_height (widget)
	);
	
	cairo_t *cairo;
	cairo = cairo_create (surface);
	
	DrawingArea::set_rgb(cairo, 255, 255, 255);
	
	cairo_paint (cairo);
	cairo_destroy (cairo);
}
void draw_configure(GtkWidget *widget, cairo_t *cairo,  gpointer data) {
	//g_print("draw_configure\n");
	
	cairo_set_source_surface (cairo, surface, 0, 0);
	cairo_paint (cairo);
}
void draw() {
	//g_print("Main::draw()\n");
	viewport->draw(drawingArea, surface, window, interfacePrincipal->get_clipping_radio());

	console_configure();
}
void fechar_programa() {
	gtk_main_quit();
}


void iprincipal_objeto_novo() {
	interfaceObjeto->open();
}
void iprincipal_objeto_remover() {}
void iprincipal_button_right() {
	g_print("Main::iprincipal_button_right()\n");
	int passo;
	passo = interfacePrincipal->get_window_passo();
	
	window->deslocar_right(passo);
	draw();
}
void iprincipal_button_left() {
	g_print("Main::iprincipal_button_left()\n");
	int passo;
	passo = interfacePrincipal->get_window_passo();
	
	window->deslocar_left(passo);
	draw();
}
void iprincipal_button_up() {
	g_print("Main::iprincipal_button_up()\n");
	int passo;
	passo = interfacePrincipal->get_window_passo();
	
	window->deslocar_up(passo);
	draw();
}
void iprincipal_button_down() {
	g_print("Main::iprincipal_button_down()\n");
	int passo;
	passo = interfacePrincipal->get_window_passo();
	
	window->deslocar_down(passo);
	draw();
}
void iprincipal_button_in() {
	g_print("Main::iprincipal_button_in()\n");
	int passo;
	passo = interfacePrincipal->get_window_passo();
	
	window->zoom_in(passo);
	draw();
}
void iprincipal_button_out() {
	g_print("Main::iprincipal_button_out()\n");
	int passo;
	passo = interfacePrincipal->get_window_passo();
	
	window->zoom_out(passo);
	draw();
}
void iprincipal_button_rotacionar() {
	g_print("Main::iprincipal_button_rotacionar()\n");
	int graus;
	graus = interfacePrincipal->get_window_graus();
	
	window->rotacionar(graus);
	draw();
}

void iobjeto_fechar() {
	interfaceObjeto->hide();
	interfaceObjeto->limpar_campos();
}
void iobjeto_poligono_novo_ponto() {
	interfaceObjeto->poligono_novo_ponto();
}
void iobjeto_curva_novo_ponto() {
	interfaceObjeto->curva_novo_ponto();
}
void iobjeto_changed_tipo_radio() {
	interfaceObjeto->changed_tipo_radio();
}
void iobjeto_adicionar() {
	g_print("Main::iobjeto_adicionar()\n");

	Objeto *objeto;
	objeto = interfaceObjeto->get_objeto();
	
	interfaceMenuObjeto->add_objeto( objeto->get_nome(), objeto->get_tipo_to_string() );
	window->add_objeto(objeto);
	draw();
}
void iobjeto_resetar() {
	g_print("Main::iobjeto_resetar()\n");
	interfaceObjeto->limpar_campos();
}
void iobjeto_remover() {}


void itransformacao_aplicar() {
	iTransformacao->transformar_objeto();
	iTransformacao->hide();

	window->recalcular_scn();
	
	draw();
}
void itransformacao_cancelar() {
	iTransformacao->hide();
}
void itransformacao_adicionar() {}
void itransformacao_remover() {}
void itransformacao_changed_rotacao_radio() {
	iTransformacao->changed_rotacao_radio();
}

void load() {
	windowPrincipal = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "window_principal") );
	g_signal_connect (windowPrincipal, "destroy", G_CALLBACK (fechar_programa), NULL);
}


void imenu_objeto_selecionar (GtkTreeView *treeview, GtkTreePath *path, GtkTreeViewColumn *col, gpointer userdata) {
	GtkTreeModel *model;
	GtkTreeIter iter;
	
	model = gtk_tree_view_get_model(treeview);
	
	if ( gtk_tree_model_get_iter(model, &iter, path) ) {
		gchar *name;
		
		gtk_tree_model_get(model, &iter, 1, &name, -1);
		
		Objeto *objeto;
		objeto = window->lista_objeto_search(name);
		
		iTransformacao->open(objeto);
		
		g_free(name);
	}
}

int main(int argc, char *argv[]) {
	int width, height;
	width = 800;
	height = 600;

	gtk_init(&argc, &argv);
	
	builder = gtk_builder_new();
  	gtk_builder_add_from_file(builder, "interface.glade", NULL);
	
	window = new Window(width, height);
	viewport = new ViewPort(new Ponto(0,0,0), new Ponto(width, height, 0));
	
	interfacePrincipal = new InterfacePrincipal(builder);
	interfaceObjeto = new InterfaceObjeto(builder);
	interfaceMenuObjeto = new InterfaceMenuObjeto(builder);
	iTransformacao = new InterfaceTransformacao(builder);
	
	drawingArea = new DrawingArea(builder);
	
	

	/* AREA DE TESTES (adicionar objetos manualmente) */

	
	
	Curva2* curva;
	curva = new Curva2("adsdada");
	curva->add_ponto(new Ponto(10,10,0));
	curva->add_ponto(new Ponto(40,40,0));
	curva->add_ponto(new Ponto(70,40,0));
	curva->add_ponto(new Ponto(100,10,0));
	curva->add_ponto(new Ponto(100,10,0));
	curva->add_ponto(new Ponto(150,0,0));
	curva->add_ponto(new Ponto(170,50,0));
	curva->add_ponto(new Ponto(200,70,0));
	curva->curva_spline();

	window->add_objeto(curva);
	
	

	/* FIM AREA DE TESTES */



	load();
	
	console_configure();

	gtk_builder_connect_signals(builder, NULL);
  	gtk_widget_show_all(windowPrincipal);
  	gtk_main ();
	
	return 0;
}
