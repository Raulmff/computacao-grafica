/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef INTERFACE_OBJETO_CPP
#define INTERFACE_OBJETO_CPP

#include "hpp/InterfaceObjeto.hpp"

void iobjeto_fechar();
void iobjeto_adicionar();
void iobjeto_resetar();
void iobjeto_remover();

void iobjeto_poligono_novo_ponto();
void iobjeto_curva_novo_ponto();

void iobjeto_changed_tipo_radio();

InterfaceObjeto::InterfaceObjeto(GtkBuilder *builder) {
	//g_print("InterfaceObjeto: InterfaceObjeto\n");
	this->load_button(builder);
	this->load_button_radio(builder);
	this->load_entry(builder);
	
	this->window = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "window1") );
	//g_signal_connect (this->window, "destroy", G_CALLBACK (iobjeto_cancelar), NULL);
	
	this->contadorNome[0] = 0;
	this->contadorNome[1] = 0;
	this->contadorNome[2] = 0;
	this->contadorNome[3] = 0;
}

void InterfaceObjeto::load_button(GtkBuilder *builder) {
	//g_print("InterfaceObjeto: load_button\n");
	
	this->fechar = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button5") );
	this->adicionar = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button6") );
	this->resetar = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button12") );

	this->poligonoNovoPonto = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button10") );
	this->curvaNovoPonto = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button2") );
	
	g_signal_connect (this->fechar, "clicked", G_CALLBACK (iobjeto_fechar), NULL);
	g_signal_connect (this->adicionar, "clicked", G_CALLBACK (iobjeto_adicionar), NULL);
	g_signal_connect (this->resetar, "clicked", G_CALLBACK (iobjeto_resetar), NULL);

	g_signal_connect (this->poligonoNovoPonto, "clicked", G_CALLBACK (iobjeto_poligono_novo_ponto), NULL);
	g_signal_connect (this->curvaNovoPonto, "clicked", G_CALLBACK (iobjeto_curva_novo_ponto), NULL);
}
void InterfaceObjeto::load_button_radio(GtkBuilder *builder) {
	//g_print("InterfaceObjeto: load_button_radio\n");
	
	this->radioPonto = GTK_RADIO_BUTTON( gtk_builder_get_object(GTK_BUILDER(builder), "radiobutton6") );
	this->radioReta = GTK_RADIO_BUTTON( gtk_builder_get_object(GTK_BUILDER(builder), "radiobutton7") );
	this->radioPoligono = GTK_RADIO_BUTTON( gtk_builder_get_object(GTK_BUILDER(builder), "radiobutton8") );
	this->radioCurva = GTK_RADIO_BUTTON( gtk_builder_get_object(GTK_BUILDER(builder), "radiobutton9") );
	
	g_signal_connect (this->radioPonto, "toggled", G_CALLBACK (iobjeto_changed_tipo_radio), NULL);
	g_signal_connect (this->radioReta, "toggled", G_CALLBACK (iobjeto_changed_tipo_radio), NULL);
	g_signal_connect (this->radioPoligono, "toggled", G_CALLBACK (iobjeto_changed_tipo_radio), NULL);
	g_signal_connect (this->radioCurva, "toggled", G_CALLBACK (iobjeto_changed_tipo_radio), NULL);
}
void InterfaceObjeto::load_entry(GtkBuilder *builder) {
	//g_print("InterfaceObjeto: load_entry\n");

	this->entryNome = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "entry2"));

  	// Ponto
  	this->entryPonto[0] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton9"));
  	this->entryPonto[1] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton10"));
  	this->entryPonto[2] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton25"));

  	// Poligono
	this->entryPoligono[0] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton15"));
  	this->entryPoligono[1] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton16"));
  	this->entryPoligono[2] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton28"));

  	// Reta
  	this->entryReta[0][0] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton11"));
  	this->entryReta[0][1] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton12"));
  	this->entryReta[0][2] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton26"));
	
	this->entryReta[1][0] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton13"));
  	this->entryReta[1][1] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton14"));
  	this->entryReta[1][2] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton27"));

  	// Curva
  	this->entryCurva[0][0] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton17"));
  	this->entryCurva[0][1] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton18"));
  	this->entryCurva[0][2] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton29"));

  	this->entryCurva[1][0] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton19"));
  	this->entryCurva[1][1] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton20"));
  	this->entryCurva[1][2] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton31"));

  	this->entryCurva[2][0] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton22"));
  	this->entryCurva[2][1] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton21"));
  	this->entryCurva[2][2] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton30"));

  	this->entryCurva[3][0] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton24"));
  	this->entryCurva[3][1] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton23"));
  	this->entryCurva[3][2] = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton32"));
}

void InterfaceObjeto::open() {
	//g_print("InterfaceObjeto: open\n");
	this->limpar_campos();
	
	gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON(this->radioPonto), TRUE );
	this->changed_tipo_radio();
	
	gtk_widget_show(window);
}
void InterfaceObjeto::hide() {
	//g_print("InterfaceObjeto: hide\n");
	gtk_widget_hide(window);
}

void InterfaceObjeto::activate_ponto(bool activate) {
	gtk_widget_set_sensitive( (GtkWidget*)this->entryPonto[0], activate );
	gtk_widget_set_sensitive( (GtkWidget*)this->entryPonto[1], activate );
	gtk_widget_set_sensitive( (GtkWidget*)this->entryPonto[2], activate );
}
void InterfaceObjeto::activate_poligono(bool activate) {
	gtk_widget_set_sensitive( (GtkWidget*)this->entryPoligono[0], activate );
	gtk_widget_set_sensitive( (GtkWidget*)this->entryPoligono[1], activate );
	gtk_widget_set_sensitive( (GtkWidget*)this->entryPoligono[2], activate );
}
void InterfaceObjeto::activate_reta(bool activate) {
	int c;
	for (c = 0; c < 2; c++) {
		gtk_widget_set_sensitive( (GtkWidget*)this->entryReta[c][0], activate );
		gtk_widget_set_sensitive( (GtkWidget*)this->entryReta[c][1], activate );
		gtk_widget_set_sensitive( (GtkWidget*)this->entryReta[c][2], activate );
	}
}
void InterfaceObjeto::activate_curva(bool activate) {
	int c;
	for (c = 0; c < 4; c++) {
		gtk_widget_set_sensitive( (GtkWidget*)this->entryCurva[c][0], activate );
		gtk_widget_set_sensitive( (GtkWidget*)this->entryCurva[c][1], activate );
		gtk_widget_set_sensitive( (GtkWidget*)this->entryCurva[c][2], activate );
	}
}

Ponto* InterfaceObjeto::get_ponto() {
	std::string nome;
	nome = get_entry_nome();
	
	this->contadorNome[0]++;
	this->set_nome();

	int x, y;
	x = get_ponto_entry_x();
	y = get_ponto_entry_y();
	
	return new Ponto(nome, x, y, 0);
}
Reta* InterfaceObjeto::get_reta() {
	std::string nome;
	nome = get_entry_nome();
	
	this->contadorNome[1]++;
	this->set_nome();
	
	// Ponto P0
	int x, y;
	x = get_reta_entry_x(0);
	y = get_reta_entry_y(0);
	
	Ponto *p0;
	p0 = new Ponto(x, y, 0);
	
	// Ponto P1
	x = get_reta_entry_x(1);
	y = get_reta_entry_y(1);
	
	Ponto *p1;
	p1 = new Ponto(x, y, 0);
	
	return new Reta(nome, p0, p1);
}
Poligono* InterfaceObjeto::get_poligono() {
	this->poligono_novo_ponto(); // Resgatar o ultimo ponto (nao foi adicionado com botao adicionar)
	this->contadorNome[2]++;
	this->set_nome();
	
	return this->poligono;
}
Curva2* InterfaceObjeto::get_curva() {
	g_print("InterfaceObjeto::get_curva\n");

	this->curva_novo_ponto();
	this->curva->construir_curva();

	this->contadorNome[3]++;
	this->set_nome();

	return this->curva;
}
void InterfaceObjeto::curva_novo_ponto() {
	std::string nome;
	nome = get_entry_nome();

	int n_pontos;

	if (this->curva == NULL) {
		this->curva = new Curva2(nome);
		n_pontos = 4;

		gtk_widget_set_sensitive( (GtkWidget*)this->entryCurva[3][0], false );
		gtk_widget_set_sensitive( (GtkWidget*)this->entryCurva[3][1], false );
		gtk_widget_set_sensitive( (GtkWidget*)this->entryCurva[3][2], false );
	} else {
		n_pontos = 3;
	}

	double x, y, z;

	int c;
	for (c = 0; c < n_pontos; c++) {
		x = get_curva_entry_x(c);
		y = get_curva_entry_y(c);
		z = get_curva_entry_z(c);
		
		curva->add_ponto( new Ponto(x, y, z) );
	}
}
void InterfaceObjeto::poligono_novo_ponto() {
	int x, y;
	x = get_poligono_entry_x();
	y = get_poligono_entry_y();
	
	this->poligono->add_ponto( new Ponto(x, y, 0) );
}

void InterfaceObjeto::changed_tipo_radio() {
	//g_print("InterfaceObjeto: changed_tipo_radio\n");
	this->curva = NULL;
	this->poligono = NULL;

	if (gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(this->radioPonto) )) {
		this->set_nome();
		
		this->activate_ponto(true);
		this->activate_reta(false);
		this->activate_poligono(false);
		this->activate_curva(false);
	}
	if (gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(this->radioReta) )) {
		this->set_nome();
		
		this->activate_ponto(false);
		this->activate_reta(true);
		this->activate_poligono(false);
		this->activate_curva(false);
	}
	if (gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(this->radioPoligono) )) {
		std::string nome;
		nome = this->set_nome();
		this->poligono = new Poligono(nome);
		
		this->activate_ponto(false);
		this->activate_reta(false);
		this->activate_poligono(true);
		this->activate_curva(false);
	}
	if (gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(this->radioCurva) )) {
		this->set_nome();
		
		this->activate_ponto(false);
		this->activate_reta(false);
		this->activate_poligono(false);
		this->activate_curva(true);
	}
}

std::string InterfaceObjeto::set_nome() {
	std::string nome;

	if (gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(this->radioPonto) )) {
		nome = "ponto" + std::to_string(this->contadorNome[0]);
	}
	if (gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(this->radioReta) )) {
		nome = "reta" + std::to_string(this->contadorNome[1]);
	}
	if (gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(this->radioPoligono) )) {
		nome = "poligono" + std::to_string(this->contadorNome[2]);
	}
	if (gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(this->radioCurva) )) {
		nome = "curva" + std::to_string(this->contadorNome[3]);
	}
	
	gchar* n;
	n = (gchar*) nome.c_str();

	gtk_entry_set_text(this->entryNome, n);

	return nome;
}
std::string InterfaceObjeto::get_entry_nome() {
	return gtk_entry_get_text(this->entryNome);
}
int InterfaceObjeto::get_ponto_entry_x() {
	return atoi(gtk_entry_get_text(this->entryPonto[0]));
}
int InterfaceObjeto::get_ponto_entry_y() {
	return atoi(gtk_entry_get_text(this->entryPonto[1]));
}
int InterfaceObjeto::get_ponto_entry_z() {
	return atoi(gtk_entry_get_text(this->entryPonto[2]));
}


int InterfaceObjeto::get_poligono_entry_x() {
	return atoi(gtk_entry_get_text(this->entryPoligono[0]));
}
int InterfaceObjeto::get_poligono_entry_y() {
	return atoi(gtk_entry_get_text(this->entryPoligono[1]));
}
int InterfaceObjeto::get_poligono_entry_z() {
	return atoi(gtk_entry_get_text(this->entryPoligono[2]));
}


int InterfaceObjeto::get_reta_entry_x(int i) {
	return atoi(gtk_entry_get_text(this->entryReta[i][0]));
}
int InterfaceObjeto::get_reta_entry_y(int i) {
	return atoi(gtk_entry_get_text(this->entryReta[i][1]));
}
int InterfaceObjeto::get_reta_entry_z(int i) {
	return atoi(gtk_entry_get_text(this->entryReta[i][2]));
}

int InterfaceObjeto::get_curva_entry_x(int i) {
	return atoi(gtk_entry_get_text(this->entryCurva[i][0]));
}
int InterfaceObjeto::get_curva_entry_y(int i) {
	return atoi(gtk_entry_get_text(this->entryCurva[i][1]));
}
int InterfaceObjeto::get_curva_entry_z(int i) {
	return atoi(gtk_entry_get_text(this->entryCurva[i][2]));
}

Objeto* InterfaceObjeto::get_objeto() {
	if (gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(this->radioPonto) )) {
		return this->get_ponto();
	}
	if (gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(this->radioReta) )) {
		return this->get_reta();
	}
	if (gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(this->radioPoligono) )) {
		return this->get_poligono();
	}
	if (gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(this->radioCurva) )) {
		return this->get_curva();
	}
}

void InterfaceObjeto::limpar_campos() {
	//g_print("InterfaceObjeto: limpar_campos\n");

	gtk_entry_set_text(this->entryPonto[0], "0");
	gtk_entry_set_text(this->entryPonto[1], "0");
	gtk_entry_set_text(this->entryPonto[2], "0");
	
	gtk_entry_set_text(this->entryPoligono[0], "0");
	gtk_entry_set_text(this->entryPoligono[1], "0");
	gtk_entry_set_text(this->entryPoligono[2], "0");

	int c;
	for (c = 0; c < 2; c++) {
		gtk_entry_set_text(this->entryReta[c][0], "0");
		gtk_entry_set_text(this->entryReta[c][1], "0");
		gtk_entry_set_text(this->entryReta[c][2], "0");
	}

	for (c = 0; c < 4; c++) {
		gtk_entry_set_text(this->entryCurva[c][0], "0");
		gtk_entry_set_text(this->entryCurva[c][1], "0");
		gtk_entry_set_text(this->entryCurva[c][2], "0");
	}
}

#endif
