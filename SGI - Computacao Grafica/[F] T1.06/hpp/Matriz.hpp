/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef MATRIZ_HPP
#define MATRIZ_HPP

class Matriz {
private:

public:
	/*
	 * Cria um objeto da classe Matriz
	 */
	Matriz();
	/*
	 * Multiplica um array e uma matriz, armazenando os
	 * valores num segundo array
	 * \param a Matriz esquerda na multiplicação
	 * \param b Matriz a direita na multiplicação
	 * \param c Matriz de armazenamento dos resultados
	 * \return void
	 */
	static void multiplicar_3x3(double a[3], double b[3][3], double c[3]);
	static void multiplicar_4x4(double a[4], double b[4][4], double c[4]);

};

#endif