/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef CURVA2_HPP
#define CURVA2_HPP

#include <gtk/gtk.h>
#include "Poligono.hpp"
#include "Ponto.hpp"
#include "ListaEnc.hpp"

class Curva2 : public Poligono {
public:
	Curva2(std::string nome);

	void construir_curva();
	bool clipping(double x0, double y0, double x, double y);
	void curva_spline();

};

#endif