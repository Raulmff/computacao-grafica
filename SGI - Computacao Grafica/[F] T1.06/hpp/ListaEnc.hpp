/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef LISTAENC_HPP
#define LISTAENC_HPP

#include "Elemento.hpp"

template<typename T>
class ListaEnc {

public:
	ListaEnc();
	~ListaEnc();
	
	void adicionaNoInicio(const T& dado);
	T retiraDoInicio();
	void eliminaDoInicio();
	
	void adicionaNaPosicao(const T& dado, int pos);
	int posicao(const T& dado) const;
	T* posicaoMem(const T& dado) const;
	bool contem(const T& dado);
	T retiraDaPosicao(int pos);
	
	void adiciona(const T& dado);
	T retira();
	
	T retiraEspecifico(const T& dado);
	void adicionaEmOrdem(const T& data);
	bool listaVazia() const;
	bool igual(T dado1, T dado2);
	bool maior(T dado1, T dado2);
	bool menor(T dado1, T dado2);
	void destroiLista();
	
	T get(int c);
	int get_size();
	ListaEnc<T>* copy();

private:
	Elemento<T>* head;
	int size;

};

#endif