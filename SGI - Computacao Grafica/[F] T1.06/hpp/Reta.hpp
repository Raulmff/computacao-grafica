/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef RETA_HPP
#define RETA_HPP

#include <gtk/gtk.h>

#include "Ponto.hpp"
#include "Objeto.hpp"

class Reta : public Objeto {
private:
	Ponto *pontoMin, *pontoMax;
	Ponto *pontoSCNMin, *pontoSCNMax;

	void calcular_vetor_rc(Ponto* ponto, int rc[4], bool& dentro, double xE, double yF, double xD, double yT);
	bool clipping_cohen_ponto(Ponto* ponto, int rc[4], double anguloReta, double xE, double yF, double xD, double yT);
	bool igual(double v1, double v2);

public:
	/*
	 * Cria um objeto da classe Reta
	 * \param nome Nome da reta
	 * \param min Ponto mais a esquerda da Reta
	 * \param max Ponto mais a direita da Reta
	 */
	Reta(std::string nome, Ponto *min, Ponto *max);
	/*
	 * Cria um objeto da classe Reta
	 * \param min Ponto mais a esquerda da Reta
	 * \param max Ponto mais a direita da Reta
	 */
	Reta(Ponto *min, Ponto *max);
	/*
	 * Retorna o ponto mais a esquerda da Reta
	 * \return Ponto*
	 */
	Ponto* get_ponto_min();
	/*
	 * Retorna o ponto mais a direita da Reta
	 * \return Ponto*
	 */
	Ponto* get_ponto_max();
	/*
	 * Translada a reta dentro da window
	 * \param dX Taxa de deslocamento da coordenada x
	 * \param dY Taxa de deslocamento da coordenada y
	 * \return void
	 */
	void transladar(double dX, double dY);
	/*
	 * Escalona a reta dentro da window
	 * \param sX Taxa de escalonamento da coordenada x
	 * \param sY Taxa de escalonamento da coordenada y
	 * \return void
	 */
	void escalonar(double sX, double sY);
	/*
	 * Rotaciona a reta em cima do ponto P(0,0)
	 * \param angulo Angulo de rotação
	 * \return void
	 */
	void rotacionar_origem(double angulo);
	/*
	 * Rotaciona a reta em cima do ponto central da reta
	 * \param angulo Angulo de rotação
	 * \return void
	 */
	void rotacionar_centro_objeto(double angulo);
	/*
	 * Rotaciona a reta em cima de um ponto qualquer
	 * \param angulo Angulo de rotação
	 * \param x Coordenada x do ponto
	 * \param y Coordenada y do ponto
	 * \return void
	 */
	void rotacionar_sobre_ponto(double angulo, double x, double y);
	/*
	 * Inicia o processo de renormalização dos pontos da reta
	 * \return void
	 */
	void recalcular_scn();
	/*
	 * Translada as coordenadas normalizadas da reta
	 * \param dX Taxa de translação da coordenada x
	 * \param dY Taxa de translação da coordenada y
	 * \return void
	 */
	void transladar_scn(double dX, double dY);
	/*
	 * Escalona as coordenadas normalizadas da reta
	 * \param sX Taxa de escalonamento da coordenada x
	 * \param sY Taxa de escalonamento da coordenada y
	 * \return void
	 */
	void escalonar_scn(double sX, double sY);
	/*
	 * Rotaciona as coordenadas normalizadas da reta
	 * \param angulo Angulo de rotacionamento
	 * \param xC Coordenada x do ponto central da window
	 * \param yC Coordenada y do ponto central da window
	 * \return void
	 */
	void rotacionar_scn(double angulo, double xC, double yC);

	bool clipping_cohen(double x0, double y0, double x, double y);

	bool clipping_liang_barsky(double xe, double xd, double yb, double yt);

	/*
	 * Obtem o angulo de acordo com duas das coordenadas da reta
	 * \param delta1 A diferenca absoluta da primeira coordenada (ex: x1 - x0)
	 * \param delta2 A diferenca absoluta da segunda coordenada (ex: z1 - z0)
	 * \return double
	 */
	double get_angulo(double delta1, double delta2);

};

#endif