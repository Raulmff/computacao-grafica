/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef CURVA2_CPP
#define CURVA2_CPP

#include <cmath>
#include "hpp/Curva2.hpp"

Curva2::Curva2(std::string nome) : Poligono(nome) {
	this->tipo = CURVA;
}

void Curva2::construir_curva() {
	ListaEnc<Ponto*> *lista;
	lista = new ListaEnc<Ponto*>();

	int c;
	for (c = 0; c < this->lista->get_size() -3; c += 3) {
		Ponto *p1, *p2, *p3, *p4;
		p1 = this->lista->get(c);
		p2 = this->lista->get(c+1);
		p3 = this->lista->get(c+2);
		p4 = this->lista->get(c+3);

		double t, tt;
		for (t = 0; t <= 1; t += 0.1f) {
			/*
			 * Pontos que nao podem ser adicionados a curva:
			 * O primeiro ponto de todos os conjuntos de 4 pontos (com a excessao do primeiro conjunto)
			 * O ultimo ponto de todos os conjuntos de 4 pontos (com a excessao do ultimo conjunto)
			 */
			if ( (t == 1 || t == 0) && (c != 0) && c != (this->lista->get_size()-3) ) {
				continue;
			}

			tt = 1-t;

			double x;
			x =  p1->get_x() * pow(tt, 3.0);
			x += p2->get_x() * pow(tt, 2.0) * (3*t);
			x += p3->get_x() * pow(t, 2.0) * (3*tt);
			x += p4->get_x() * pow(t, 3.0);
			
			double y;
			y =  p1->get_y() * pow(tt, 3.0);
			y += p2->get_y() * pow(tt, 2.0) * (3*t);
			y += p3->get_y() * pow(t, 2.0) * (3*tt);
			y += p4->get_y() * pow(t, 3.0);
			
			double z;
			z = 0;

			Ponto* p;
			p = new Ponto(x, y, z);

			lista->adiciona(p);
		}
	}

	this->lista = lista;
}

void Curva2::curva_spline() {
	ListaEnc<Ponto*> *lista;
	lista = new ListaEnc<Ponto*>();

	if (this->lista == NULL) {
		return;
	}
	if (this->lista->get_size() == 0) {
		return;
	}

	double t[4][4] = {
		0.000, 0.00, 0.0, 1,
		0.008, 0.04, 0.2, 0,
		0.048, 0.08, 0.0, 0,
		0.048, 0.00, 0.0, 0
	};

	double c[4][4] = {
		-0.167,	+0.50, -0.500, 0.167,
		+0.500, -1.00, +0.500, 0.000,
		-0.500, +0.00, +0.500, 0.000,
		+0.167, +0.67, +0.167, 0.000
	};	 

	int j;
	for(j = 0; j < this->lista->get_size() - 3; j++) {
		Ponto *p0, *p1, *p2, *p3;
		p0 = this->lista->get(j);
		p1 = this->lista->get(j+1);
		p2 = this->lista->get(j+2);
		p3 = this->lista->get(j+3);

		double gX[4];
		gX[0] = p0->get_x();
		gX[1] = p1->get_x();
		gX[2] = p2->get_x();
		gX[3] = p3->get_x();

		double gY[4];
		gY[0] = p0->get_y();
		gY[1] = p1->get_y();
		gY[2] = p2->get_y();
		gY[3] = p3->get_y();

		int i;
		double cX[4], cY[4];
		for(i = 0; i < 4; i++) {
			cX[i] = (gX[0] * c[i][0]) + (gX[1] * c[i][1]) + (gX[2] * c[i][2]) + (gX[3] * c[i][3]);
			cY[i] = (gY[0] * c[i][0]) + (gY[1] * c[i][1]) + (gY[2] * c[i][2]) + (gY[3] * c[i][3]);
		}

		double rX[4], rY[4];
		for(i = 0; i < 4; i++) {
			rX[i] = (cX[0] * t[i][0]) + (cX[1] * t[i][1]) + (cX[2] * t[i][2]) + (cX[3] * t[i][3]);
			rY[i] = (cY[0] * t[i][0]) + (cY[1] * t[i][1]) + (cY[2] * t[i][2]) + (cY[3] * t[i][3]);
		}

		double x, dx, d2x, d3x;
		x = rX[0];
		dx = rX[1];
		d2x = rX[2];
		d3x = rX[3];

		double y, dy, d2y, d3y;
		y = rY[0];
		dy = rY[1];
		d2y = rY[2];
		d3y = rY[3];

		double xV, yV;
		xV = x;
		yV = y;

		for(i = 0; i < 5; i++) {
			x += dx;
			dx += d2x;
			d2x += d3x;
			
			y += dy;
			dy += d2y;
			d2y += d3y;

			Ponto *a1, *a2;
			a1 = new Ponto(xV,yV,0);
			a2 = new Ponto(x,y,0);

			lista->adiciona(a1);
			lista->adiciona(a2);
			
			//printf("xV: %f yV: %f\n", a1->get_scn_x(), a1->get_y());
			//printf("x: %f y: %f\n", a2->get_scn_x(), a2->get_y());
			//printf("\n");

			xV = x;
			yV = y;
		}
	}

	this->lista = lista;
}

bool Curva2::clipping(double x0, double y0, double x, double y) {
	ListaEnc<Ponto*> *lista;
	lista = new ListaEnc<Ponto*>();
	
	int size;
	size = this->listaSCN->get_size();

	int c;
	for(c = 0; c < size-1; c++) {
		Ponto *atual, *proximo;
		atual = this->listaSCN->get(c);
		proximo = this->listaSCN->get(c+1);
		
		Reta *reta;
		reta = new Reta(atual, proximo);

		if( reta->clipping_cohen(x0,y0,x,y) ) {
			lista->adiciona(reta->get_ponto_min());
			
			if( !proximo->scn_equal(reta->get_ponto_max()) || c == size-2) {
				lista->adiciona(reta->get_ponto_max());
			}
		}

	}

	this->listaSCN = lista;

	return true;
}

#endif