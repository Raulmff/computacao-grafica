/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef POLIGONO_CPP
#define POLIGONO_CPP

#include <math.h>

#include "hpp/Poligono.hpp"
#include "ListaEnc.cpp"

#define PI 3.14159265

int array_to_int(int array[], int size);

Poligono::Poligono() {
	this->nome = "";
	this->tipo = POLIGONO;
	this->lista = new ListaEnc<Ponto*>();
	this->listaSCN = NULL;
}
Poligono::Poligono(std::string nome) : Objeto() {
	this->nome = nome;
	this->tipo = POLIGONO;
	this->lista = new ListaEnc<Ponto*>();
	this->listaSCN = NULL;
}

void Poligono::add_ponto(Ponto *ponto) {
	this->lista->adiciona(ponto);
}

void Poligono::transladar(double Dx, double Dy) {
	int c;
	for (c = 0; c < this->lista->get_size(); c++) {
		Ponto *p;
		p = this->lista->get(c);
		
		p->transladar(Dx, Dy);
	}
}

void Poligono::escalonar(double Sx, double Sy) {
	Ponto* centro;
	centro = this->get_centro();

	double centroX, centroY;
	centroX = centro->get_x();
	centroY = centro->get_y();

	int i;
	for (i = 0; i < this->lista->get_size(); i++) {
		Ponto *p;
		p = this->lista->get(i);
		
		p->transladar(-centroX, -centroY);
		p->escalonar(Sx, Sy);
		p->transladar(centroX, centroY);
	}
}

void Poligono::rotacionar_sobre_ponto(double angulo, double x, double y) {
	int i;
	for (i = 0; i < this->lista->get_size(); i++) {
		Ponto *p;
		p = this->lista->get(i);
		
		p->rotacionar_sobre_ponto(angulo, x, y);
	}
}

Ponto* Poligono::get_centro() {
	double centroX, centroY;
	
	centroX = 0;
	centroY = 0;
	
	int c;
	for (c = 0; c < this->lista->get_size(); c++) {
		Ponto *p;
		p = this->lista->get(c);
		
		centroX += p->get_x();
		centroY += p->get_y();
	}

	centroX = centroX/c;
	centroY = centroY/c;

	return new Ponto(centroX, centroY, 0);
}
Ponto* Poligono::get_centro_scn() {
	double centroX, centroY;
	
	centroX = 0;
	centroY = 0;
	
	int c;
	for (c = 0; c < this->listaSCN->get_size(); c++) {
		Ponto *p;
		p = this->listaSCN->get(c);
		
		centroX += p->get_scn_x();
		centroY += p->get_scn_y();
	}

	centroX = centroX/c;
	centroY = centroY/c;

	return new Ponto(centroX, centroY, 0);
}
void Poligono::rotacionar_centro_objeto(double angulo) {
	Ponto* centro;
	centro = this->get_centro();

	double centroX, centroY;
	centroX = centro->get_x();
	centroY = centro->get_y();

	int i;
	for (i = 0; i < this->lista->get_size(); i++) {
		Ponto *p;
		p = this->lista->get(i);
		
		p->rotacionar_sobre_ponto(angulo, centroX, centroY);
	}
}
void Poligono::rotacionar_origem(double angulo) {
	int i;
	for (i = 0; i < this->lista->get_size(); i++) {
		Ponto *p;
		p = this->lista->get(i);
		
		p->rotacionar_origem(angulo);
	}
}
ListaEnc<Ponto*>* Poligono::get_lista() {
	return this->lista;
}
ListaEnc<Ponto*>* Poligono::get_lista_scn() {
	return this->listaSCN;
}

void Poligono::recalcular_scn() {
	int c;
	for (c = 0; c < this->lista->get_size(); c++) {
		Ponto *p;
		p = this->lista->get(c);
		p->recalcular_scn();
	}

	this->listaSCN = this->lista;
}
void Poligono::transladar_scn(double dX, double dY) {
	int c;
	for (c = 0; c < this->listaSCN->get_size(); c++) {
		Ponto *p;
		p = this->listaSCN->get(c);
		p->transladar_scn(dX, dY);
	}
}
void Poligono::escalonar_scn(double sX, double sY) {
	double centroX, centroY;
	centroX = this->get_centro_scn()->get_x();
	centroY = this->get_centro_scn()->get_y();

	int c;
	for (c = 0; c < this->listaSCN->get_size(); c++) {
		Ponto *p;
		p = this->listaSCN->get(c);
		
		p->transladar_scn(-centroX, -centroY);
		p->escalonar_scn(sX, sY);
		p->transladar_scn(centroX * sX, centroY * sY);
	}
}
void Poligono::rotacionar_scn(double angulo, double xC, double yC) {
	ListaEnc<Ponto*> *lista;
	int size;
	
	lista = this->listaSCN;
	size = this->listaSCN->get_size();

	int c;
	for (c = 0; c < size; c++) {
		Ponto *p;
		p = lista->get(c);
		p->rotacionar_scn(angulo, xC, yC);
	}
}
bool Poligono::clipping(double x0, double y0, double x, double y) {
	ListaEnc<Ponto*> *lista;
	lista = new ListaEnc<Ponto*>();
	
	Ponto *p1, *p2;
	p2 = this->listaSCN->get( this->listaSCN->get_size()-1 );

	bool clipping;
	clipping = false;

	int c;
	for (c = 0; c < this->listaSCN->get_size(); c++) {
		p1 = p2;
		p2 = this->listaSCN->get(c);

		Reta *reta;
		reta = new Reta(p1, p2);

		Ponto* p;
		p = NULL;

		// Os dois pontos estao fora da window
		if ( !reta->clipping_cohen(x0, y0, x, y) ) {
			int rcP1[4], rcP2[4];
			p1->calcular_vetor_rc(rcP1, x0, y0, x, y);
			p2->calcular_vetor_rc(rcP2, x0, y0, x, y);
			
			int rc1, rc2;
			rc1 = array_to_int(rcP1, 4);
			rc2 = array_to_int(rcP2, 4);
			
			// Os dois pontos estao no mesmo quadrante
			if (rc1 == rc2) {
				continue;
			}

			double xNew, yNew;
			switch (rc2) {
				// Topo/Esquerda (1001)
				case 1001:
					xNew = x0;
					yNew = y;
					break;

				// Topo/Meio (1000)
				case 1000:
					xNew = p2->get_scn_x();
					yNew = y;
					break;

				// Topo/Direita (1010)
				case 1010:
					xNew = x;
					yNew = y;
					break;

				// Meio/Esquerda (0001)
				case 1:
					xNew = x0;
					yNew = p2->get_scn_y();
					break;

				// Meio/Direita (0010)
				case 10:
					xNew = x;
					yNew = p2->get_scn_y();
					break;

				// Baixo/Esquerda (0101)
				case 101:
					xNew = x0;
					yNew = y0;
					break;

				// Baixo/Meio (0100)
				case 100:
					xNew = p2->get_scn_x();
					yNew = y0;
					break;

				// Baixo/Direita (0110)
				case 110:
					xNew = x;
					yNew = y0;
					break;

				// Dentro da window (0000)
				default:
					xNew = p2->get_scn_x();
					yNew = p2->get_scn_y();
					break;
			}

			p = new Ponto(xNew, yNew, 0);
			p->set_scn_x(xNew);
			p->set_scn_y(yNew);
			p->set_scn_z(0);

			g_print("c %d | P(%g; %g) | rcP2: %i%i%i%i\n", c, p2->get_x(), p2->get_y(), rcP2[0], rcP2[1], rcP2[2], rcP2[3]);
		// Ao menos uma reta/aresta pode ser criada pelos pontos e passar dentro da window
		} else {
			Ponto *min, *max;
			min = reta->get_ponto_min();
			max = reta->get_ponto_max();

			// Caso os dois pontos estejam fora
			if ( !min->scn_equal(p1) ) {
				lista->adiciona(min);
			}

			p = max;

			// Ao menos uma parte do poligono esta dentro da window
			clipping = true;
		}

		lista->adiciona(p);
	}

	this->listaSCN = lista;
	return clipping;
}

#endif