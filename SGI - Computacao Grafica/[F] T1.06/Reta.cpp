/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef RETA_CPP
#define RETA_CPP

#include <math.h>
#include <stdlib.h>

#include "hpp/Reta.hpp"

#define PI 3.14159265

Reta::Reta(Ponto *min, Ponto *max) : Objeto() {
	this->tipo = RETA;
	
	this->pontoMin = new Ponto(min);
	this->pontoMax = new Ponto(max);
}
Reta::Reta(std::string nome, Ponto *min, Ponto *max) {
	this->nome = nome;
	this->tipo = RETA;
	
	this->pontoMin = new Ponto(min);
	this->pontoMax = new Ponto(max);
}

Ponto* Reta::get_ponto_min() {
	return this->pontoMin;
}
Ponto* Reta::get_ponto_max() {
	return this->pontoMax;
}

void Reta::transladar(double dX, double dY) {
	this->pontoMin->transladar(dX, dY);
	this->pontoMax->transladar(dX, dY);
}
void Reta::escalonar(double sX, double sY) {
	int centroX = ((this->pontoMin->get_x() + this->pontoMax->get_x()) / 2);
	int centroY = ((this->pontoMin->get_y() + this->pontoMax->get_y()) / 2);
	
	this->pontoMin->transladar(-centroX, -centroY);
	this->pontoMax->transladar(-centroX, -centroY);

	this->pontoMin->escalonar(sX, sY);
	this->pontoMax->escalonar(sX, sY);

	this->pontoMin->transladar(centroX, centroY);
	this->pontoMax->transladar(centroX, centroY);
}
void Reta::rotacionar_sobre_ponto(double angulo, double x, double y) {
	this->pontoMin->rotacionar_sobre_ponto(angulo, x, y);
	this->pontoMax->rotacionar_sobre_ponto(angulo, x, y);
}
void Reta::rotacionar_centro_objeto(double angulo) {
	int centroX = ((this->pontoMin->get_x() + this->pontoMax->get_x()) / 2);
	int centroY = ((this->pontoMin->get_y() + this->pontoMax->get_y()) / 2);
	
	this->pontoMin->rotacionar_sobre_ponto(angulo, centroX, centroY);
	this->pontoMax->rotacionar_sobre_ponto(angulo, centroX, centroY);
}
void Reta::rotacionar_origem(double angulo) {
	this->pontoMin->rotacionar_sobre_ponto(angulo, 0, 0);
	this->pontoMax->rotacionar_sobre_ponto(angulo, 0, 0);
}


void Reta::recalcular_scn() {
	this->pontoMin->recalcular_scn();
	this->pontoMax->recalcular_scn();
}
void Reta::transladar_scn(double dX, double dY) {
	this->pontoMin->transladar_scn(dX, dY);
	this->pontoMax->transladar_scn(dX, dY);
}
void Reta::escalonar_scn(double sX, double sY) {
	double centroX, centroY;
	centroX = (this->pontoMin->get_scn_x() + this->pontoMax->get_scn_x()) / 2;
	centroY = (this->pontoMin->get_scn_y() + this->pontoMax->get_scn_y()) / 2;

	this->pontoMin->transladar_scn(-centroX, -centroY);
	this->pontoMax->transladar_scn(-centroX, -centroY);

	this->pontoMin->escalonar_scn(sX, sY);
	this->pontoMax->escalonar_scn(sX, sY);

	centroX *= sX;
	centroY *= sY;
	
	this->pontoMin->transladar_scn(centroX, centroY);
	this->pontoMax->transladar_scn(centroX, centroY);
}
void Reta::rotacionar_scn(double angulo, double xC, double yC) {
	this->pontoMin->rotacionar_scn(angulo, xC, yC);
	this->pontoMax->rotacionar_scn(angulo, xC, yC);
}
void Reta::calcular_vetor_rc(Ponto* ponto, int rc[4], bool& dentro, double xE, double yF, double xD, double yT) {
	double x, y;
	x = ponto->get_scn_x();
	y = ponto->get_scn_y();

	rc[0] = 0;
	rc[1] = 0;
	rc[2] = 0;
	rc[3] = 0;

	if (x < xE) {
		rc[3] = 1;
	} else if (xD < x) {
		rc[2] = 1;
	}

	if (y < yF) {
		rc[1] = 1;
	} else if (yT < y) {
		rc[0] = 1;
	}

	dentro = (rc[0] == 0) && (rc[1] == 0) && (rc[2] == 0) && (rc[3] == 0);
}

bool Reta::clipping_cohen_ponto(Ponto* ponto, int rc[4], double anguloReta, double xE, double yF, double xD, double yT) {
	double xP, yP;
	xP = ponto->get_scn_x();
	yP = ponto->get_scn_y();

	double x, y;
	x = ponto->get_scn_x();
	y = ponto->get_scn_y();

	if (rc[0] == 1) { // xT (coord. x quando y esta no topo) - 1***
		x = (1 / anguloReta) * (yT - yP) + xP;
	}
	if (rc[1] == 1) { // xF (coord. x quando y esta no fundo) - *1**
		x = (1 / anguloReta) * (yF - yP) + xP;
	}
	if (rc[2] == 1) { // yD (coord. y quando x esta na direita) - **1*
		y = anguloReta * (xD - xP) + yP;
	}
	if (rc[3] == 1) { // yE (coord. y quando x esta na esquerda) - ***1
		y = anguloReta * (xE - xP) + yP;
	}

	if (x > xD) {
		x = xD;
	}
	if (x < xE) {
		x = xE;
	}
	if (y < yF) {
		y = yF;
	}
	if (y > yT) {
		y = yT;
	}

	ponto->set_scn_x(x);
	ponto->set_scn_y(y);
}

bool Reta::clipping_cohen(double xE, double yF, double xD, double yT) {
	g_print("Reta::clipping_cohen\n");

	bool minDentro, maxDentro;
	int rcMin[4], rcMax[4];

	// Obtem os vetores rc para o ponto min e max
	this->calcular_vetor_rc(this->pontoMin, rcMin, minDentro, xE, yF, xD, yT);
	this->calcular_vetor_rc(this->pontoMax, rcMax, maxDentro, xE, yF, xD, yT);

	 // Completamente dentro (ponto min e ponto max)
	if (minDentro && maxDentro) {
		return true;
	}

	bool AND;
	AND = ((rcMin[0] == 1) && (rcMax[0] == 1)) ||
		  ((rcMin[1] == 1) && (rcMax[1] == 1)) ||
		  ((rcMin[2] == 1) && (rcMax[2] == 1)) ||
		  ((rcMin[3] == 1) && (rcMax[3] == 1));

	// Completamente fora (nao existe a chance de passarem dentro da window)
	if (AND) {
		return false;
	}

	double anguloReta;
	anguloReta = (this->pontoMax->get_scn_y() - this->pontoMin->get_scn_y()) / (this->pontoMax->get_scn_x() - this->pontoMin->get_scn_x());

	this->clipping_cohen_ponto(this->pontoMin, rcMin, anguloReta, xE, yF, xD, yT);
	
	// Pode estar dentro, mas caso um deles continue fora apos o clipping entao esta fora
	if (!minDentro && !maxDentro) {
		this->calcular_vetor_rc(this->pontoMin, rcMin, minDentro, xE, yF, xD, yT);
		
		// Completamente fora. Descarta o calculo do segundo ponto
		if (!minDentro) {
			return false;
		}
	}
	
	this->clipping_cohen_ponto(this->pontoMax, rcMax, anguloReta, xE, yF, xD, yT);

	return true;
}

bool Reta::clipping_liang_barsky(double xe, double yb, double xd, double yt) {
	g_print("Reta::clipping_liang_barsky\n");

	double xMin, yMin;
	xMin = this->pontoMin->get_scn_x();
	yMin = this->pontoMin->get_scn_y();

	double dx, dy;
	dx = this->pontoMax->get_scn_x() - xMin;
	dy = this->pontoMax->get_scn_y() - yMin;

	double p0, p1, p2, p3;
	p0 = -dx;
	p1 = dx;
	p2 = -dy;
	p3 = dy;

	double q0, q1, q2, q3;
	q0 = xMin - xe;
	q1 = xd - xMin;
	q2 = yMin - yb;
	q3 = yt - yMin;

	double r0, r1, r2, r3;
	r0 = q0/p0;
	r1 = q1/p1;
	r2 = q2/p2;
	r3 = q3/p3;

	double s1;
	s1 = 0.0;

	if (p0 < 0) {
		s1 = std::max(s1, r0);
	}
	if (p1 < 0) {
		s1 = std::max(s1, r1);
	}
	if (p2 < 0) {
		s1 = std::max(s1, r2);
	}
	if (p3 < 0) {
		s1 = std::max(s1, r3);
	}

	double s2;
	s2 = 1;

	if (p0 > 0) {
		s2 = std::min(s2, r0);
	}
	if (p1 > 0) {
		s2 = std::min(s2, r1);
	}
	if (p2 > 0) {
		s2 = std::min(s2, r2);
	}
	if (p3 > 0) {
		s2 = std::min(s2, r3);
	}

	// Reta fora da window
	if ( s1 > s2 ) {
		return false;
	}

	double x, y;
	if(s1 > 0) {
		x = xMin + s1 * dx;
		y = yMin + s1 * dy;

		this->pontoMin->set_scn_x(x);
		this->pontoMin->set_scn_y(y);
	}

	if(s2 < 1) {
		x = xMin + s2 * dx;
		y = yMin + s2 * dy;

		this->pontoMax->set_scn_x(x);
		this->pontoMax->set_scn_y(y);
	}

	return true;
}
double Reta::get_angulo(double delta1, double delta2) {
	double hipotenusa;
	hipotenusa = sqrt( (delta1 * delta1) + (delta2 * delta2) );

	double seno;
	seno = delta2 / hipotenusa;

	double angulo;
	angulo = asin(seno) * 180.0 / PI;

	// Caso o ponto max e min estiverem trocados
	/*
	if ( (delta1 < 0) && (delta2 >= 0) || (delta2 < 0) && (delta1 >= 0) ) {
		angulo *= -1;
	}*/

	return angulo;
}

#endif