/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef MATRIZ_CPP
#define MATRIZ_CPP

#include "hpp/Matriz.hpp"

Matriz::Matriz() {}
void Matriz::multiplicar_3x3(double a[3], double b[3][3], double c[3]) {
	int i;
	for(i = 0; i < 3; i++) {
		c[i] = (a[0] * b[0][i]) + (a[1] * b[1][i]) + (a[2] * b[2][i]);
	}
}
void Matriz::multiplicar_4x4(double a[4], double b[4][4], double c[4]) {
	int i;
	for(i = 0; i < 4; i++) {
		c[i] = (a[0] * b[0][i]) + (a[1] * b[1][i]) + (a[2] * b[2][i]) + (a[3] * b[3][i]);
	}
}

#endif