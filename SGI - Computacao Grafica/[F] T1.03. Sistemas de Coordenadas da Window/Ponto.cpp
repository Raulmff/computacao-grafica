/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef PONTO_CPP
#define PONTO_CPP

#include <cmath>
#include "hpp/Ponto.hpp"

#define PI 3.14

Ponto::Ponto(double x, double y) : Objeto() {
	this->tipo = "PONTO";
	
	this->x = x;
	this->y = y;
	this->z = 1;

	this->recalcular_scn();
}
Ponto::Ponto(std::string nome, double x, double y) {
	this->nome = nome;
	this->tipo = "PONTO";
	
	this->x = x;
	this->y = y;
	this->z = 1;

	this->recalcular_scn();
}

double Ponto::get_x() {
	return this->x;
}
double Ponto::get_y() {
	return this->y;
}

double Ponto::get_scn_x() {
	return this->xSCN;
}
double Ponto::get_scn_y() {
	return this->ySCN;
}

void Ponto::translacao(double &x, double &y, double &z, double dX, double dY) {
	g_print("Ponto::transladar - P(%g,%g)", x, y);

	double matriz1[3] = {
		x, y, z
	};
	double matriz2[3][3] = {
		1, 0, 0,
		0, 1, 0,
		dX, dY, 1
	};

	double matriz3[3];
	Matriz::multiplicar(matriz1, matriz2, matriz3);

	x = matriz3[0];
	y = matriz3[1];
	z = matriz3[2];

	g_print(" para P(%g,%g)\n", x, y);
}
void Ponto::escalonamento(double &x, double &y, double &z, double sX, double sY) {
	g_print("Ponto::escalonar  - P(%g,%g)", x, y);
	
	double matriz1[3] = {
		x, y, z
	};
	double matriz2[3][3] = {
		sX, 0, 0,
		0, sY, 0,
		0,  0, 1
	};

	double matriz3[3];
	Matriz::multiplicar(matriz1, matriz2, matriz3);

	x = matriz3[0];
	y = matriz3[1];
	z = matriz3[2];
	
	g_print(" para P(%g,%g)\n", x, y);
}
void Ponto::rotacao(double &x, double &y, double &z, double angulo, double pX, double pY) {
	g_print("Ponto::rotacao    - P(%g,%g)", x, y);

	double cosTeta, senTeta;
	cosTeta = cos ((-angulo) * PI / 180);
	senTeta = sin ((-angulo) * PI / 180);

	x -= pX;
	y -= pY;

	double matriz1[3] = {
		x, y, z
	};
	double matriz2[3][3] = {
		cosTeta, -senTeta, 0,
		senTeta, cosTeta, 0,
		0, 0, 1
	};

	double matriz3[3];
	Matriz::multiplicar(matriz1, matriz2, matriz3);

	x = matriz3[0];
	y = matriz3[1];
	z = matriz3[2];

	x += pX;
	y += pY;
	g_print(" para P(%g,%g)\n", x, y);
}

void Ponto::transladar(double dX, double dY) {
	this->translacao(this->x, this->y, this->z, dX, dY);
}
void Ponto::escalonar(double sX, double sY) {
	this->escalonamento(this->x, this->y, this->z, sX, sY);
}
void Ponto::rotacionar_sobre_ponto(double angulo, double x, double y) {
	this->rotacao(this->x, this->y, this->z, angulo, x, y);
}
void Ponto::rotacionar_centro_objeto(double angulo) {
	this->rotacionar_sobre_ponto(angulo, this->x, this->y);
}
void Ponto::rotacionar_origem(double angulo) {
	this->rotacionar_sobre_ponto(angulo, 0,0);
}



void Ponto::recalcular_scn() {
	this->xSCN = this->x;
	this->ySCN = this->y;
	this->zSCN = this->z;
}
void Ponto::transladar_scn(double dX, double dY) {
	this->translacao(this->xSCN, this->ySCN, this->zSCN, dX, dY);
}
void Ponto::normalizar_scn(double sX, double sY) {
	this->escalonar_scn(sX, sY);
}
void Ponto::escalonar_scn(double sX, double sY) {
	this->escalonamento(this->xSCN, this->ySCN, this->zSCN, sX, sY);
}
void Ponto::rotacionar_scn(double angulo, double xC, double yC) {
	this->rotacao(this->xSCN, this->ySCN, this->zSCN, angulo, xC, yC);
}

#endif