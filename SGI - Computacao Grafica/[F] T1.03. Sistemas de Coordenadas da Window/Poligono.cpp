/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef POLIGONO_CPP
#define POLIGONO_CPP

#include <math.h>

#include "hpp/Poligono.hpp"
#include "ListaEnc.cpp"

#define PI 3.14159265

Poligono::Poligono(std::string nome) : Objeto() {
	this->nome = nome;
	this->tipo = "POLIGONO";
	this->lista = new ListaEnc<Ponto*>();
}

void Poligono::add_ponto(Ponto *ponto) {
	this->lista->adiciona(ponto);
}

void Poligono::transladar(double Dx, double Dy) {
	int c;
	for (c = 0; c < this->lista->get_size(); c++) {
		Ponto *p;
		p = this->lista->get(c);
		
		p->transladar(Dx, Dy);
	}
}

void Poligono::escalonar(double Sx, double Sy) {
	Ponto* centro;
	centro = this->get_centro();

	double centroX, centroY;
	centroX = centro->get_x();
	centroY = centro->get_y();

	int i;
	for (i = 0; i < this->lista->get_size(); i++) {
		Ponto *p;
		p = this->lista->get(i);
		
		p->transladar(-centroX, -centroY);
		p->escalonar(Sx, Sy);
		p->transladar(centroX, centroY);
	}
}

void Poligono::rotacionar_sobre_ponto(double angulo, double x, double y) {
	int i;
	for (i = 0; i < this->lista->get_size(); i++) {
		Ponto *p;
		p = this->lista->get(i);
		
		p->rotacionar_sobre_ponto(angulo, x, y);
	}
}

Ponto* Poligono::get_centro() {
	double centroX, centroY;
	
	centroX = 0;
	centroY = 0;
	
	int c;
	for (c = 0; c < this->lista->get_size(); c++) {
		Ponto *p;
		p = this->lista->get(c);
		
		centroX += p->get_x();
		centroY += p->get_y();
	}

	centroX = centroX/c;
	centroY = centroY/c;

	return new Ponto(centroX, centroY);
}
Ponto* Poligono::get_centro_scn() {
	double centroX, centroY;
	
	centroX = 0;
	centroY = 0;
	
	int c;
	for (c = 0; c < this->lista->get_size(); c++) {
		Ponto *p;
		p = this->lista->get(c);
		
		centroX += p->get_scn_x();
		centroY += p->get_scn_y();
	}

	centroX = centroX/c;
	centroY = centroY/c;

	return new Ponto(centroX, centroY);
}
void Poligono::rotacionar_centro_objeto(double angulo) {
	Ponto* centro;
	centro = this->get_centro();

	double centroX, centroY;
	centroX = centro->get_x();
	centroY = centro->get_y();

	int i;
	for (i = 0; i < this->lista->get_size(); i++) {
		Ponto *p;
		p = this->lista->get(i);
		
		p->rotacionar_sobre_ponto(angulo, centroX, centroY);
	}
}
void Poligono::rotacionar_origem(double angulo) {
	int i;
	for (i = 0; i < this->lista->get_size(); i++) {
		Ponto *p;
		p = this->lista->get(i);
		
		p->rotacionar_origem(angulo);
	}
}
ListaEnc<Ponto*>* Poligono::get_lista() {
	return this->lista;
}






void Poligono::recalcular_scn() {
	int c;
	for (c = 0; c < this->lista->get_size(); c++) {
		Ponto *p;
		p = this->lista->get(c);
		p->recalcular_scn();
	}
}
void Poligono::transladar_scn(double dX, double dY) {
	int c;
	for (c = 0; c < this->lista->get_size(); c++) {
		Ponto *p;
		p = this->lista->get(c);
		p->transladar_scn(dX, dY);
	}
}
void Poligono::escalonar_scn(double sX, double sY, Ponto *centro, Ponto* centroSCN) {
	int i;
	for (i = 0; i < this->lista->get_size(); i++) {
		Ponto *p;
		p = this->lista->get(i);
		
		p->transladar_scn(-centro->get_x(), -centro->get_y());
		p->escalonar_scn(sX, sY);
		p->transladar_scn(centroSCN->get_x(), centroSCN->get_y());
	}
}
void Poligono::escalonar_scn(double sX, double sY) {
	Ponto* centro;
	centro = this->get_centro_scn();

	double centroX, centroY;
	centroX = centro->get_x();
	centroY = centro->get_y();

	this->escalonar_scn(sX, sY, centro, centro);
}
void Poligono::normalizar_scn(double sX, double sY) {
	Ponto *centro1, *centro2;
	centro1 = this->get_centro_scn();

	double centroX, centroY;
	centroX = centro1->get_x();
	centroY = centro1->get_y();

	centro2 = new Ponto( centroX * sX, centroY * sY );

	this->escalonar_scn(sX, sY, centro1, centro2);
}
void Poligono::rotacionar_scn(double angulo, double xC, double yC) {
	int c;
	for (c = 0; c < this->lista->get_size(); c++) {
		Ponto *p;
		p = this->lista->get(c);

		p->rotacionar_scn(angulo, xC, yC);
	}
}

#endif