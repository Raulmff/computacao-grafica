/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef DRAWING_AREA_HPP
#define DRAWING_AREA_HPP

#include<gtk/gtk.h>
#include <cmath>

#include "ViewPort.hpp"
#include "Poligono.hpp"
#include "Ponto.hpp"
#include "Reta.hpp"

class DrawingArea {
private:
	GtkWidget *drawing;

	void draw_ponto(Ponto* ponto, cairo_surface_t *surface, Ponto *windowMin, Ponto *windowMax, Ponto* viewMin, Ponto* viewMax, int r, int g, int b);
	void draw_reta(Reta *reta, cairo_surface_t *surface, Ponto *windowMin, Ponto *windowMax, Ponto* viewMin, Ponto* viewMax, int r, int g, int b);

public:
	DrawingArea(GtkBuilder *builder);
	
	void draw();
	void draw_window(Ponto* centro, cairo_surface_t *surface, Ponto *windowMin, Ponto *windowMax, Ponto* viewMin, Ponto* viewMax);
	void draw_ponto(Ponto* ponto, cairo_surface_t *surface, Ponto *windowMin, Ponto *windowMax, Ponto* viewMin, Ponto* viewMax);
	void draw_reta(Reta *reta, cairo_surface_t *surface, Ponto *windowMin, Ponto *windowMax, Ponto* viewMin, Ponto* viewMax);
	void draw_poligono(Poligono *poligono, cairo_surface_t *surface, Ponto *windowMin, Ponto *windowMax, Ponto* viewMin, Ponto* viewMax);
	
	/*
	 * Defini a cor rgb que será usada para os próximos desenhos
	 * \param cairo ...
	 * \param red Proporção red
	 * \param green Proporção green
	 * \param blue Proporção blue
	 * \return void
	 */
	static void set_rgb(cairo_t* cairo, int red, int green, int blue);

};

#endif