/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef VIEWPORT_HPP
#define VIEWPORT_HPP

#include<gtk/gtk.h>

class DrawingArea;

#include "Window.hpp"
#include "Ponto.hpp"
#include "Reta.hpp"
#include "Poligono.hpp"
#include "DrawingArea.hpp"

class ViewPort {
private:
	Ponto *pontoMin, *pontoMax;

public:
	/*
	 * Cria um objeto da classe ViewPort
	 * \param min Ponto mais a esquerda da ViewPort
	 * \param max Ponto mais a direita da ViewPort
	 */
	ViewPort(Ponto *min, Ponto *max);
	/*
	 * Desenha todos os objetos do mundo
	 * \param drawing Objeto responsavel por desenhar
	 * \param surface ...
	 * \param window Objeto representando a janela do usuario
	 * \return void
	 */
	void draw(DrawingArea *drawing, cairo_surface_t *surface, Window *window);
	/*
	 * Redesenha a janela de modo a deixa-la toda branca
	 * \param surface ...
	 * \return void
	 */
	void limpar_tela(cairo_surface_t *surface);
	/*
	 * Realiza o calculo para transformada da ViewPort
	 * na coordenada x
	 * \param xW Coordenada x que sera transformada
	 * \param xWMin Coordenada x mais a esquerda da Window
	 * \param xWMax Coordenada x mais a direita da Window
	 * \param xVMin Coordenada x mais a esquerda da ViewPort
	 * \param xVMax Coordenada x mais a direita da ViewPort
	 * \return void
	 */
	static double transformada_x(double xW, double xWMin, double xWMax, double xVMin, double xVMax);
	/*
	 * Realiza o calculo para transformada da ViewPort
	 * na coordenada y
	 * \param yW Coordenada x que sera transformada
	 * \param yWMin Coordenada y mais a esquerda da Window
	 * \param yWMax Coordenada y mais a direita da Window
	 * \param yVMin Coordenada y mais a esquerda da ViewPort
	 * \param yVMax Coordenada y mais a direita da ViewPort
	 * \return void
	 */
	static double transformada_y(double yW, double yWMin, double yWMax, double yVMin, double yVMax);
	/*
	 * Retorna o ponto mais a esquerda da ViewPort
	 * \return Ponto*
	 */
	Ponto* get_ponto_min();
	/*
	 * Retorna o ponto mais a direita da ViewPort
	 * \return Ponto*
	 */
	Ponto* get_ponto_max();

};

#endif
