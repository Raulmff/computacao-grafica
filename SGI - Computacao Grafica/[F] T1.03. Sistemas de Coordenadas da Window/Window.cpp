/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef WINDOW_CPP
#define WINDOW_CPP

#include<typeinfo>

#include "hpp/Window.hpp"
#include "ListaEnc.cpp"

Window::Window(int width, int height) {
	this->lista = new ListaEnc<Objeto*>();
	
	this->pontoMin = new Ponto(-1,-1);
	this->pontoMax = new Ponto(1,1);
	this->pontoCentro = new Ponto(0, 0);

	this->width = width;
	this->height = height;

	this->zoom = 1;
	this->angulo = 0;

	this->transladarX = 0;
	this->transladarY = 0;
}

void Window::add_objeto(Objeto *objeto) {
	this->lista->adiciona(objeto);
	this->recalcular_scn();
}

void Window::deslocar_left(int passo) {
	this->transladar(-passo, 0);
}
void Window::deslocar_right(int passo) {
	this->transladar(passo, 0);
}
void Window::deslocar_up(int passo) {
	this->transladar(0, passo);
}
void Window::deslocar_down(int passo) {
	this->transladar(0, -passo);
}
void Window::zoom_in(int passo) {
	this->zoom += passo;
	this->recalcular_scn();
}
void Window::zoom_out(int passo) {
	this->zoom -= passo;
	this->recalcular_scn();
}
void Window::transladar(double dX, double dY) {
	this->transladarX += dX;
	this->transladarY += dY;

	this->recalcular_scn();	
}
void Window::recalcular_scn() {
	double transladarX, transladarY;
	// Translada o mundo para o centro da window e soma com o deslocamento do usuario
	transladarX = this->transladarX + ( 2 / (this->width) );
	transladarY = this->transladarY + ( 2 / (this->height) );

	double escalaX, escalaY;
	// Escalona o mundo para caber dentro da window
	escalaX = 2 / (this->width);
	escalaY = 2 / (this->height);

	double zoom;
	zoom = 1;

	if (this->zoom < 0) {
		zoom = (-1) / this->zoom;
	} else if (this->zoom > 0) {
		zoom = this->zoom;
	}

	g_print("Window::recalcular_scn() - transladX: %g | transladY: %g\n", transladarX, transladarY);
	g_print("Window::recalcular_scn() - Zoom: %g\n", escalaX);
	g_print("Window::recalcular_scn() - EscalaX: %g | EscalaY: %g\n", escalaX, escalaY);

	Objeto *objeto;
	int c;
	for (c = 0; c < this->lista->get_size(); c++) {
		objeto = this->lista->get(c);

		objeto->recalcular_scn();
		objeto->transladar_scn(-transladarX, -transladarY);
		objeto->rotacionar_scn(-this->angulo, this->pontoCentro->get_x(), this->pontoCentro->get_y());
		objeto->escalonar_scn(zoom, zoom);
		objeto->normalizar_scn(escalaX, escalaY);
	}
}
void Window::rotacionar(int angulo) {
	this->angulo += angulo;
	if (this->angulo >= 360) {
		this->angulo -= 360;
	}
	g_print("Window::rotacionar(%g)\n", this->angulo);
	
	this->recalcular_scn();
}

Ponto* Window::get_ponto_min() {
	return this->pontoMin;
}
Ponto* Window::get_ponto_max() {
	return this->pontoMax;
}
Ponto* Window::get_ponto_centro() {
	return this->pontoCentro;
}

ListaEnc<Objeto*>* Window::get_objetos() {
	return this->lista;
}

Objeto* Window::lista_objeto_search(std::string nome) {
	Objeto *objeto;
	
	int c;
	for (c = 0; c < this->lista->get_size(); c++) {
		objeto = this->lista->get(c);
		
		if (objeto->get_nome() == nome) {
			return objeto;
		}
	}
	
	return NULL;
}

#endif
