/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef DRAWING_AREA_CPP
#define DRAWING_AREA_CPP

#include "hpp/DrawingArea.hpp"
#include "ListaEnc.cpp"

void configure(GtkWidget *widget, GdkEventConfigure *event, gpointer data);
void draw_configure(GtkWidget *widget, cairo_t *cairo,  gpointer data);

DrawingArea::DrawingArea(GtkBuilder *builder) {
	this->drawing = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "drawingarea") );
	
	g_signal_connect (this->drawing, "configure-event", G_CALLBACK (configure), NULL);
	g_signal_connect (this->drawing, "draw", G_CALLBACK (draw_configure), NULL);
}
void DrawingArea::draw() {
	gtk_widget_queue_draw (this->drawing);
}

void DrawingArea::set_rgb(cairo_t* cairo, int red, int green, int blue) {
	double r, g, b;
	
	r = red/255;
	g = green/255;
	b = blue/255;
	
	cairo_set_source_rgb (cairo, r, g, b);
}
void DrawingArea::draw_window(Ponto *centro, cairo_surface_t *surface, Ponto *windowMin, Ponto *windowMax, Ponto* viewMin, Ponto* viewMax) {
	Reta* reta;
	reta = new Reta( new Ponto(-1, 0), new Ponto(1, 0) );
	this->draw_reta(reta, surface, windowMin, windowMax, viewMin, viewMax, 255, 0, 0);

	reta = new Reta( new Ponto(0, -1) , new Ponto(0, 1) );
	this->draw_reta(reta, surface, windowMin, windowMax, viewMin, viewMax, 255, 0, 0);

	this->draw_ponto(centro, surface, windowMin, windowMax, viewMin, viewMax, 200, 0, 0);

}
void DrawingArea::draw_ponto(Ponto *ponto, cairo_surface_t *surface, Ponto *windowMin, Ponto *windowMax, Ponto* viewMin, Ponto* viewMax, int r, int g, int b) {
	double x, y;
	x = ViewPort::transformada_x(ponto->get_scn_x(), windowMin->get_x(), windowMax->get_x(), viewMin->get_x(), viewMax->get_x() );
	y = ViewPort::transformada_y(ponto->get_scn_y(), windowMin->get_y(), windowMax->get_y(), viewMin->get_y(), viewMax->get_y() );
	
	g_print("DrawingArea::draw_ponto - P(%g;%g)\n", x, y);

	cairo_t *cairo;
	cairo = cairo_create (surface);
	
	DrawingArea::set_rgb(cairo, r, g, b);
	
	cairo_set_line_width(cairo, 1);
	cairo_arc(cairo, x, y, 1, 0, 2 * M_PI);
	cairo_fill_preserve(cairo);
	cairo_stroke(cairo);
}
void DrawingArea::draw_reta(Reta *reta, cairo_surface_t *surface, Ponto *windowMin, Ponto *windowMax, Ponto* viewMin, Ponto* viewMax, int r, int g, int b) {
	double x, y;
	x = ViewPort::transformada_x(reta->get_ponto_max()->get_scn_x(), windowMin->get_x(), windowMax->get_x(), viewMin->get_x(), viewMax->get_x() );
	y = ViewPort::transformada_y(reta->get_ponto_max()->get_scn_y(), windowMin->get_y(), windowMax->get_y(), viewMin->get_y(), viewMax->get_y() );
	
	double x0, y0;
	x0 = ViewPort::transformada_x(reta->get_ponto_min()->get_scn_x(), windowMin->get_x(), windowMax->get_x(), viewMin->get_x(), viewMax->get_x() );
	y0 = ViewPort::transformada_y(reta->get_ponto_min()->get_scn_y(), windowMin->get_y(), windowMax->get_y(), viewMin->get_y(), viewMax->get_y() );
	
	g_print("DrawingArea::draw_reta - P1(%g;%g) | P2 (%g;%g)\n", x0, y0, x, y);
	
	cairo_t *cairo;
  	cairo = cairo_create (surface);
	
	DrawingArea::set_rgb(cairo, r, g, b);
	
	cairo_set_line_width(cairo, 1);
	cairo_move_to(cairo, x0, y0);
	cairo_line_to(cairo, x, y);
	cairo_stroke(cairo);
}

void DrawingArea::draw_ponto(Ponto *ponto, cairo_surface_t *surface, Ponto *windowMin, Ponto *windowMax, Ponto* viewMin, Ponto* viewMax) {
	this->draw_ponto(ponto, surface, windowMin, windowMax, viewMin, viewMax, 0, 0, 0);
}
void DrawingArea::draw_reta(Reta *reta, cairo_surface_t *surface, Ponto *windowMin, Ponto *windowMax, Ponto* viewMin, Ponto* viewMax) {
	this->draw_reta(reta, surface, windowMin, windowMax, viewMin, viewMax, 0, 0, 0);
}

void DrawingArea::draw_poligono(Poligono *poligono, cairo_surface_t *surface, Ponto *windowMin, Ponto *windowMax, Ponto* viewMin, Ponto* viewMax) {
	ListaEnc<Ponto*> *lista;
	lista = poligono->get_lista();
	
	Ponto *anterior, *atual;
	Reta *reta;
	atual = lista->get(0);
	
	int c;
	for (c = 1; c < lista->get_size(); c++) {
		anterior = atual;
		atual = lista->get(c);
		
		reta = new Reta(anterior, atual);
		draw_reta(reta, surface, windowMin, windowMax, viewMin, viewMax);
	}
	
	anterior = lista->get(0);
	
	reta = new Reta(atual, anterior);
	draw_reta(reta, surface, windowMin, windowMax, viewMin, viewMax);
}


#endif