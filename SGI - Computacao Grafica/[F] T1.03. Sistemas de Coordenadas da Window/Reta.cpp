/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef RETA_CPP
#define RETA_CPP

#include <math.h>
#include <stdlib.h>

#include "hpp/Reta.hpp"

#define PI 3.14159265

Reta::Reta(Ponto *min, Ponto *max) : Objeto() {
	this->tipo = "RETA";
	
	this->pontoMin = min;
	this->pontoMax = max;
}
Reta::Reta(std::string nome, Ponto *min, Ponto *max) {
	this->nome = nome;
	this->tipo = "RETA";
	
	this->pontoMin = min;
	this->pontoMax = max;
}

Ponto* Reta::get_ponto_min() {
	return this->pontoMin;
}
Ponto* Reta::get_ponto_max() {
	return this->pontoMax;
}

void Reta::transladar(double dX, double dY) {
	this->pontoMin->transladar(dX, dY);
	this->pontoMax->transladar(dX, dY);
}
void Reta::escalonar(double sX, double sY) {
	int centroX = ((this->pontoMin->get_x() + this->pontoMax->get_x()) / 2);
	int centroY = ((this->pontoMin->get_y() + this->pontoMax->get_y()) / 2);
	
	this->pontoMin->transladar(-centroX, -centroY);
	this->pontoMax->transladar(-centroX, -centroY);

	this->pontoMin->escalonar(sX, sY);
	this->pontoMax->escalonar(sX, sY);

	this->pontoMin->transladar(centroX, centroY);
	this->pontoMax->transladar(centroX, centroY);
}
void Reta::rotacionar_sobre_ponto(double angulo, double x, double y) {
	this->pontoMin->rotacionar_sobre_ponto(angulo, x, y);
	this->pontoMax->rotacionar_sobre_ponto(angulo, x, y);
}
void Reta::rotacionar_centro_objeto(double angulo) {
	int centroX = ((this->pontoMin->get_x() + this->pontoMax->get_x()) / 2);
	int centroY = ((this->pontoMin->get_y() + this->pontoMax->get_y()) / 2);
	
	this->pontoMin->rotacionar_sobre_ponto(angulo, centroX, centroY);
	this->pontoMax->rotacionar_sobre_ponto(angulo, centroX, centroY);
}
void Reta::rotacionar_origem(double angulo) {
	this->pontoMin->rotacionar_sobre_ponto(angulo, 0, 0);
	this->pontoMax->rotacionar_sobre_ponto(angulo, 0, 0);
}


void Reta::recalcular_scn() {
	this->pontoMin->recalcular_scn();
	this->pontoMax->recalcular_scn();
}
void Reta::transladar_scn(double dX, double dY) {
	this->pontoMin->transladar_scn(dX, dY);
	this->pontoMax->transladar_scn(dX, dY);
}
void Reta::escalonar_scn(double sX, double sY, Ponto *centro1, Ponto* centro2) {
	double centroX, centroY;
	centroX = centro1->get_x();
	centroY = centro1->get_y();
	this->pontoMin->transladar_scn(-centroX, -centroY);
	this->pontoMax->transladar_scn(-centroX, -centroY);

	this->pontoMin->escalonar_scn(sX, sY);
	this->pontoMax->escalonar_scn(sX, sY);

	centroX = centro2->get_x();
	centroY = centro2->get_y();
	this->pontoMin->transladar_scn(centroX, centroY);
	this->pontoMax->transladar_scn(centroX, centroY);
}
void Reta::normalizar_scn(double sX, double sY) {
	double centroX, centroY;
	centroX = (this->pontoMin->get_scn_x() + this->pontoMax->get_scn_x()) / 2;
	centroY = (this->pontoMin->get_scn_y() + this->pontoMax->get_scn_y()) / 2;
	
	Ponto *centro1, *centro2;
	centro1 = new Ponto(centroX, centroY);
	centro2 = new Ponto(centroX * sX, centroY * sY);

	this->escalonar_scn(sX, sY, centro1, centro2);
}
void Reta::escalonar_scn(double sX, double sY) {
	double centroX, centroY;
	centroX = (this->pontoMin->get_scn_x() + this->pontoMax->get_scn_x()) / 2;
	centroY = (this->pontoMin->get_scn_y() + this->pontoMax->get_scn_y()) / 2;
	
	Ponto *centro;
	centro = new Ponto(centroX, centroY);

	this->escalonar_scn(sX, sY, centro, centro);
}
void Reta::rotacionar_scn(double angulo, double xC, double yC) {
	this->pontoMin->rotacionar_scn(angulo, xC, yC);
	this->pontoMax->rotacionar_scn(angulo, xC, yC);
}

#endif