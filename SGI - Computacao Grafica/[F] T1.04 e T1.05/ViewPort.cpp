/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef VIEWPORT_CPP
#define VIEWPORT_CPP

#include "hpp/ViewPort.hpp"
#include "ListaEnc.cpp"

ViewPort::ViewPort(Ponto *min, Ponto *max) {
	this->pontoMin = new Ponto(min->get_x() + 10, min->get_y() + 10, 0);
	this->pontoMax = new Ponto(max->get_x() - 10, max->get_y() - 10, 0);
}

void ViewPort::limpar_tela(cairo_surface_t *surface) {
	cairo_t *cairo;
  	cairo = cairo_create (surface);

  	DrawingArea::set_rgb(cairo, 255, 255, 255);
  	
	cairo_paint (cairo);
  	cairo_destroy (cairo);
}
void ViewPort::draw(DrawingArea *drawing, cairo_surface_t *surface, Window *window, std::string clip) {
	this->limpar_tela(surface);
	
	ListaEnc<Objeto*> *lista;
	lista = window->get_objetos();
	
	drawing->draw_window( window->get_ponto_centro() , surface, window->get_ponto_min(), window->get_ponto_max(), this->get_ponto_min(), this->get_ponto_max());
	
	double xE, yF;
	xE = window->get_ponto_min()->get_x();
	yF = window->get_ponto_min()->get_y();

	double xD, yT;
	xD = window->get_ponto_max()->get_x();
	yT = window->get_ponto_max()->get_y();

	int c;
	for (c = 0; c < lista->get_size(); c++) {
		Objeto *objeto;
		objeto = lista->get(c);
		
		switch ( objeto->get_tipo() ) {
			case PONTO:
				Ponto* ponto;
				ponto = (Ponto*) objeto;

				if ( ponto->clipping(xE, yF, xD, yT) ) {
					drawing->draw_ponto(ponto, surface, window->get_ponto_min(), window->get_ponto_max(), this->get_ponto_min(), this->get_ponto_max());
				}

				break;
			case RETA:
				Reta *reta;
				reta = (Reta*) objeto;

				bool clipping;
				if (clip == "cohen") {
					clipping = reta->clipping_cohen(xE, yF, xD, yT);
				} else {
					clipping = reta->clipping_liang_barsky(xE, yF, xD, yT);
				}

				if ( clipping ) {
					drawing->draw_reta(reta, surface, window->get_ponto_min(), window->get_ponto_max(), this->get_ponto_min(), this->get_ponto_max());
				}

				break;
			case POLIGONO:
				Poligono *poligono;
				poligono = (Poligono*) objeto;

				if ( poligono->clipping(xE, yF, xD, yT) ) {
					drawing->draw_poligono(poligono, surface, window->get_ponto_min(), window->get_ponto_max(), this->get_ponto_min(), this->get_ponto_max());
				}

				break;
			case CURVA:
				Curva2 *curva;
				curva = (Curva2*) objeto;
				
				if ( curva->clipping(xE, yF, xD, yT) ) {
					drawing->draw_curva(curva, surface, window->get_ponto_min(), window->get_ponto_max(), this->get_ponto_min(), this->get_ponto_max());
				}

				break;
			default:
				break;
		}
		
		drawing->draw();
	}
}

Ponto* ViewPort::get_ponto_min() {
	return this->pontoMin;
}
Ponto* ViewPort::get_ponto_max() {
	return this->pontoMax;
}

double ViewPort::transformada_x(double xW, double xWMin, double xWMax, double xVMin, double xVMax) {
	return ( (xW - xWMin) / (xWMax - xWMin) ) * (xVMax - xVMin) +10;
}
double ViewPort::transformada_y(double yW, double yWMin, double yWMax, double yVMin, double yVMax) {
	return ( 1 - ( (yW - yWMin) / (yWMax - yWMin) ) ) * (yVMax - yVMin) +10;
}

#endif
