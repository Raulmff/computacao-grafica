/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef CURVA2_CPP
#define CURVA2_CPP

#include <cmath>
#include "hpp/Curva2.hpp"

Curva2::Curva2(std::string nome) : Poligono(nome) {
	this->tipo = CURVA;
}

void Curva2::construir_curva() {
	ListaEnc<Ponto*> *lista;
	lista = new ListaEnc<Ponto*>();

	int c;
	for (c = 0; c < this->lista->get_size() -3; c += 3) {
		Ponto *p1, *p2, *p3, *p4;
		p1 = this->lista->get(c);
		p2 = this->lista->get(c+1);
		p3 = this->lista->get(c+2);
		p4 = this->lista->get(c+3);

		double t, tt;
		for (t = 0; t <= 1; t += 0.1f) {
			/*
			 * Pontos que nao podem ser adicionados a curva:
			 * O primeiro ponto de todos os conjuntos de 4 pontos (com a excessao do primeiro conjunto)
			 * O ultimo ponto de todos os conjuntos de 4 pontos (com a excessao do ultimo conjunto)
			 */
			if ( (t == 1 || t == 0) && (c != 0) && c != (this->lista->get_size()-3) ) {
				continue;
			}

			tt = 1-t;

			double x;
			x =  p1->get_x() * pow(tt, 3.0);
			x += p2->get_x() * pow(tt, 2.0) * (3*t);
			x += p3->get_x() * pow(t, 2.0) * (3*tt);
			x += p4->get_x() * pow(t, 3.0);
			
			double y;
			y =  p1->get_y() * pow(tt, 3.0);
			y += p2->get_y() * pow(tt, 2.0) * (3*t);
			y += p3->get_y() * pow(t, 2.0) * (3*tt);
			y += p4->get_y() * pow(t, 3.0);
			
			double z;
			z = 0;

			Ponto* p;
			p = new Ponto(x, y, z);

			lista->adiciona(p);
		}
	}

	this->lista = lista;
}
bool Curva2::clipping(double x0, double y0, double x, double y) {
	this->listaSCN = this->lista;	
}

#endif