/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef POLIGONO_HPP
#define POLIGONO_HPP

#include "ListaEnc.hpp"
#include "Reta.hpp"
#include "Ponto.hpp"
#include "Objeto.hpp"

class Poligono : public Objeto {
protected:
	ListaEnc<Ponto*> *lista;
	ListaEnc<Ponto*> *listaSCN;
	
	/*
	 * Retorna o ponto central do poligono
	 * \return Ponto*
	 */
	Ponto* get_centro();
	/*
	 * Retorna o ponto central normalizado do poligono
	 * \return Ponto*
	 */
	Ponto* get_centro_scn();

public:
	/*
	 * Cria um objeto da classe Poligono
	 */
	Poligono();
	/*
	 * Cria um objeto da classe Poligono
	 * \param nome Nome do poligono
	 */
	Poligono(std::string nome);
	/*
	 * Adiciona um ponto ao poligono
	 * \param ponto Um ponto a ser adicionado no poligono
	 * \return void
	 */
	void add_ponto(Ponto *ponto);
	/*
	 * Retorna a lista de pontos do Poligono
	 * \return ListaEnc<Ponto*>*
	 */
	ListaEnc<Ponto*>* get_lista();

	ListaEnc<Ponto*>* get_lista_scn();
	/*
	 * Translada o poligono dentro da window
	 * \param dX Taxa de deslocamento da coordenada x
	 * \param dY Taxa de deslocamento da coordenada y
	 * \return void
	 */
	void transladar(double dX, double dY);
	/*
	 * Escalona o poligono dentro da window
	 * \param sX Taxa de escalonamento da coordenada x
	 * \param sY Taxa de escalonamento da coordenada y
	 * \return void
	 */
	void escalonar(double sX, double sY);
	/*
	 * Rotaciona o poligono em cima do ponto P(0,0)
	 * \param angulo Angulo de rotação
	 * \return void
	 */
	void rotacionar_origem(double angulo);
	/*
	 * Rotaciona o poligono em cima de si mesmo
	 * \param angulo Angulo de rotação
	 * \return void
	 */
	void rotacionar_centro_objeto(double angulo);
	/*
	 * Rotaciona o poligono em cima de um ponto qualquer
	 * \param angulo Angulo de rotação
	 * \param x Coordenada x do ponto de referência
	 * \param y Coordenada y do ponto de referência
	 * \return void
	 */
	void rotacionar_sobre_ponto(double angulo, double x, double y);
	/*
	 * Inicia o processo de renormalização das coordenadas do poligono
	 * \return void
	 */
	void recalcular_scn();
	/*
	 * Translada as coordenadas normalizadas do poligono
	 * \param dX Taxa de translação da coordenada x
	 * \param dY Taxa de translação da coordenada y
	 * \return void
	 */
	void transladar_scn(double dX, double dY);
	/*
	 * Escalona as coordenadas normalizadas do poligono
	 * \param sX Taxa de escalonamento da coordenada x
	 * \param sY Taxa de escalonamento da coordenada y
	 * \return void
	 */
	void escalonar_scn(double sX, double sY);
	/*
	 * Rotaciona as coordenadas normalizadas do poligono
	 * \param angulo Angulo de rotacionamento
	 * \param xC Coordenada x do ponto central da window
	 * \param yC Coordenada y do ponto central da window
	 * \return void
	 */
	void rotacionar_scn(double angulo, double xC, double yC);

	bool clipping(double x0, double y0, double x, double y);

};

#endif
