/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef INTERFACE_OBJETO_HPP
#define INTERFACE_OBJETO_HPP

#include <gtk/gtk.h>
#include<string>

#include "Objeto.hpp"
#include "Ponto.hpp"
#include "Reta.hpp"
#include "Poligono.hpp"
#include "Curva2.hpp"

class InterfaceObjeto {
private:
	Poligono *poligono;
	Curva2* curva;

	GtkWidget *window;
	GtkEntry *entryNome;

	// [][0]: X | [][1]: Y | [][2]: Z
	GtkEntry* entryCurva[4][3];
	GtkEntry* entryReta[2][3];
	
	// [0]: X | [1]: Y | [2]: Z
	GtkEntry* entryPonto[3];
	GtkEntry* entryPoligono[3];

	GtkWidget *fechar;
	GtkWidget *adicionar, *resetar;
	GtkWidget *poligonoNovoPonto, *curvaNovoPonto;
	
	GtkRadioButton *radioPonto, *radioReta;
	GtkRadioButton *radioCurva, *radioPoligono;
	
	// [0]: Ponto | [1]: Reta | [2]: Poligono | [3]: Curva
	int contadorNome[4];

	std::string set_nome();
	std::string get_entry_nome();

	void activate_ponto(bool active);
	void activate_reta(bool active);
	void activate_poligono(bool active);
	void activate_curva(bool active);
	
	void load_button(GtkBuilder *builder);
	void load_button_radio(GtkBuilder *builder);
	void load_entry(GtkBuilder *builder);
	
	int get_ponto_entry_x();
	int get_ponto_entry_y();
	int get_ponto_entry_z();

	int get_poligono_entry_x();
	int get_poligono_entry_y();
	int get_poligono_entry_z();
	
	int get_reta_entry_x(int i);
	int get_reta_entry_y(int i);
	int get_reta_entry_z(int i);

	int get_curva_entry_x(int i);
	int get_curva_entry_y(int i);
	int get_curva_entry_z(int i);
	
	Ponto* get_ponto();
	Reta* get_reta();
	Poligono* get_poligono();
	Curva2* get_curva();

public:
	InterfaceObjeto(GtkBuilder *builder);
	
	void open();
	void hide();
	
	void limpar_campos();
	void changed_tipo_radio();
	void poligono_novo_ponto();
	void curva_novo_ponto();
	
	Objeto* get_objeto();

};

#endif
