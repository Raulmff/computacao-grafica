/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef POLIGONO_CPP
#define POLIGONO_CPP

#include <gtk/gtk.h>

#include "hpp/ViewPort.hpp"
#include "hpp/Window.hpp"
#include "hpp/Ponto.hpp"
#include "hpp/Reta.hpp"
#include "hpp/Poligono.hpp"
#include "ListaEnc.cpp"

Poligono::Poligono(GtkBuilder *builder) : Ponto() {
	this->objeto = POLIGONO;
	this->lista = NULL;
	
	this->inputNome = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "entry_novo_poligono_nome"));
	
	this->inputX = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton_poligono_x"));
  	this->inputY = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton_poligono_y"));
}

void Poligono::add_ponto() {
	int x, y;
	
	this->nome = gtk_entry_get_text(this->inputNome);
	
	x = atoi(gtk_entry_get_text(this->inputX));
  	y = atoi(gtk_entry_get_text(this->inputY));
	
	if (this->lista == NULL) {
		this->x = x;
  		this->y = y;

  		this->lista = new ListaEnc<Ponto*>();
		return;
	}
	
	Ponto *ponto;
	ponto = new Ponto(x, y);
	
	this->lista->adiciona(ponto);
}

void Poligono::draw(cairo_surface_t *surface, Window *window, ViewPort *viewport) {
	if ( this->lista->listaVazia() ) {
		Ponto::draw(surface, window, viewport);
		return;
	}
	
	Ponto *anterior, *atual;
	Reta *reta;
	atual = this;
	
	int c;
	for (c = 0; c < lista->get_size(); c++) {
		anterior = atual;
		atual = lista->get(c);
		
		reta = new Reta(anterior->get_x(), anterior->get_y(), atual->get_x(), atual->get_y());
		reta->draw(surface, window, viewport);
	}
	
	reta = new Reta(atual->get_x(), atual->get_y(), this->get_x(), this->get_y());
	reta->draw(surface, window, viewport);
}

void Poligono::limpar_campos() {
	gtk_entry_set_text(this->inputX, "0");
	gtk_entry_set_text(this->inputY, "0");
}

#endif