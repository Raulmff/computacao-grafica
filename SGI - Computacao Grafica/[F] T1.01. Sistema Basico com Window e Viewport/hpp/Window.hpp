/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef WINDOW_HPP
#define WINDOW_HPP

class Poligono;
class Ponto;

#include<gtk/gtk.h>

#include "Poligono.hpp"
#include "ListaEnc.hpp"
#include "Ponto.hpp"

class Window {
private:
	GtkBuilder *builder;
	ListaEnc<Ponto*> *lista;
	
	int xMin, yMin;
	int xMax, yMax;
	
	Poligono *poligono;
	
	GtkWidget *pontoAdicionar, *retaAdicionar, *poligonoAdicionar;
	GtkWidget *poligonoNovo;
	
	GtkEntry *inputPasso;
	
	GtkWidget *buttonIn, *buttonOut;
	GtkWidget *buttonRight, *buttonLeft, *buttonUp, *buttonDown;
	

public:
	Window(GtkBuilder *builder, int minX, int minY, int maxX, int maxY);
	
	void add(Ponto* ponto);
	void add_reta();
	void add_ponto();
	void add_poligono();
	void add_ponto_poligono();
	
	void deslocar_left();
	void deslocar_right();
	void deslocar_up();
	void deslocar_down();
	
	void zoom_in();
	void zoom_out();
	
	int get_min_x();
	int get_min_y();
	int get_max_x();
	int get_max_y();
	
	ListaEnc<Ponto*>* get_objetos();
	
	void principal_load();
	void secundaria_load();
};

#endif