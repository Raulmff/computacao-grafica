/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef POLIGONO_HPP
#define POLIGONO_HPP

#include <gtk/gtk.h>

#include "ViewPort.hpp"
#include "Window.hpp"
#include "ListaEnc.hpp"
#include "Ponto.hpp"

class Poligono : public Ponto {
private:
	ListaEnc<Ponto*> *lista;

public:
	Poligono(GtkBuilder *builder);
	
	void add_ponto();
	void draw(cairo_surface_t *surface, Window *window, ViewPort *viewport);
	void limpar_campos();

};

#endif
