/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef PONTO_HPP
#define PONTO_HPP

#include <gtk/gtk.h>
#include <string>

class Window;
class ViewPort;

#include "ViewPort.hpp"
#include "Window.hpp"

enum Objeto {
	PONTO,
	RETA,
	POLIGONO
};

class Ponto {
protected:
	GtkEntry *inputX, *inputY;
	GtkEntry *inputNome;
	
	Objeto objeto;
	cairo_t *cairo;
	
	std::string nome;
	int x, y;
	
	Ponto();

public:
	Ponto(int x, int y);
	Ponto(GtkBuilder *builder);
	
	int get_x();
	int get_y();
	
	virtual void draw(cairo_surface_t *surface, Window *window, ViewPort *viewport);
	
	void limpar_campos();

};

#endif