/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef RETA_HPP
#define RETA_HPP

#include <gtk/gtk.h>

#include "ViewPort.hpp"
#include "Window.hpp"
#include "Ponto.hpp"

class Reta : public Ponto {
private:
	GtkEntry *inputX0, *inputY0;
	int x0, y0;

public:
	Reta(GtkBuilder *builder);
	Reta(int x0, int y0, int x, int y);
	
	void draw(cairo_surface_t *surface, Window *window, ViewPort *viewport);
	void limpar_campos();

};

#endif