/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef VIEWPORT_HPP
#define VIEWPORT_HPP

#include<gtk/gtk.h>

#include "Window.hpp"
#include "Ponto.hpp"

class ViewPort {
private:
	GtkEntry *inputPasso;
	
	int xMin, yMin;
	int xMax, yMax;

public:
	ViewPort(GtkBuilder *builder, int minX, int minY, int maxX, int maxY);
	
	void draw(cairo_surface_t *surface, Window *window);
	void limpar_tela(cairo_surface_t *surface);
	
	static void set_rgb(cairo_t* cairo, int red, int green, int blue);
	static double transformada_x(double xW, double xWMin, double xWMax, double xVMin, double xVMax);
	static double transformada_y(double yW, double yWMin, double yWMax, double yVMin, double yVMax);
	
	int get_min_x();
	int get_min_y();
	int get_max_x();
	int get_max_y();

};

#endif
