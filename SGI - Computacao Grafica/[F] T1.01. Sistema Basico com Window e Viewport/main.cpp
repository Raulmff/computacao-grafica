/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#include <gtk/gtk.h>

#include "hpp/ViewPort.hpp"
#include "hpp/Window.hpp"
#include "hpp/Poligono.hpp"
#include "hpp/Ponto.hpp"
#include "hpp/Reta.hpp"

Window *window;
ViewPort *viewport;

GtkWidget *windowPrincipal, *windowPonto, *windowReta, *windowPoligono;
GtkWidget *drawing;

GtkComboBox *objetoComboBox;
GtkBuilder *builder;

cairo_surface_t *surface;

static void fechar_programa() {
	gtk_main_quit();
}
static void configure(GtkWidget *widget, GdkEventConfigure *event, gpointer data) {
	g_print("configure\n");

	if (surface)
   	cairo_surface_destroy (surface);

  	surface = gdk_window_create_similar_surface (
		gtk_widget_get_window (widget),
		CAIRO_CONTENT_COLOR,
		gtk_widget_get_allocated_width (widget),
		gtk_widget_get_allocated_height (widget)
	);
	
	cairo_t *cairo;
	cairo = cairo_create (surface);
	
	ViewPort::set_rgb(cairo, 255, 255, 255);
	
	cairo_paint (cairo);
	cairo_destroy (cairo);
}
static void draw_configure(GtkWidget *widget, cairo_t *cairo,  gpointer data) {
	g_print("draw_configure\n");
	
	cairo_set_source_surface (cairo, surface, 0, 0);
	cairo_paint (cairo);
}
static void open_window_poligono() {
	g_print("open_window_poligono\n");
  	gtk_widget_show(windowPoligono);
}
static void open_window_ponto() {
	g_print("open_window_ponto\n");
  	gtk_widget_show(windowPonto);
}
static void open_window_reta() {
	g_print("open_window_reta\n");
  	gtk_widget_show(windowReta);
}
static void selecionar_objeto() {
	int i;
	i = gtk_combo_box_get_active(objetoComboBox);
	
	switch (i) {
		case 0:
			open_window_ponto();
			break;
		case 1:
			open_window_reta();
			break;
		case 2:
			open_window_poligono();
			break;
		default:
			break;
	}
	
	gtk_combo_box_set_active(objetoComboBox, -1);
}

void draw() {
	g_print("draw\n");
	
	viewport->draw(surface, window);
	gtk_widget_queue_draw (drawing);
}

void button_window_right() {
	g_print("button_window_right\n");
	
	window->deslocar_right();
	draw();
}
void button_window_left() {
	g_print("button_window_left\n");
	
	window->deslocar_left();
	draw();
}
void button_window_up() {
	g_print("button_window_up\n");
	
	window->deslocar_up();
	draw();
}
void button_window_down() {
	g_print("button_window_down\n");
	
	window->deslocar_down();
	draw();
}

void button_window_in() {
	g_print("button_window_in\n");
	
	window->zoom_in();
	draw();
}
void button_window_out() {
	g_print("button_window_out\n");
	
	window->zoom_out();
	draw();
}

void ponto_adicionar() {
	window->add_ponto();
	draw();
	
	gtk_widget_hide(windowPonto);
}
void reta_adicionar() {
	window->add_reta();
	draw();
	
	gtk_widget_hide(windowReta);
}
void poligono_adicionar() {
	window->add_poligono();
	draw();
	
	gtk_widget_hide(windowPoligono);
}
void poligono_novo_ponto() {
	window->add_ponto_poligono();
}

void load() {
	windowPrincipal = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "window_principal") );
	windowPoligono = GTK_WIDGET(gtk_builder_get_object(GTK_BUILDER(builder), "window_poligono"));
	windowReta = GTK_WIDGET(gtk_builder_get_object(GTK_BUILDER(builder), "window_reta"));
	windowPonto = GTK_WIDGET(gtk_builder_get_object(GTK_BUILDER(builder), "window_ponto"));
  	
	objetoComboBox = GTK_COMBO_BOX(gtk_builder_get_object(GTK_BUILDER(builder), "novo_objeto_combobox") );
	drawing = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "drawingarea") );
	
	g_signal_connect (windowPrincipal, "destroy", G_CALLBACK (fechar_programa), NULL);
	g_signal_connect (objetoComboBox, "changed", G_CALLBACK(selecionar_objeto), NULL);
	g_signal_connect (drawing, "configure-event", G_CALLBACK (configure), NULL);
	g_signal_connect (drawing, "draw", G_CALLBACK (draw_configure), NULL);
}

int main(int argc, char *argv[]) {
	gtk_init(&argc, &argv);
	
	builder = gtk_builder_new();
  	gtk_builder_add_from_file(builder, "interface.glade", NULL);
	
	window = new Window(builder, 0,0,500,500);
	viewport = new ViewPort(builder, 0,0,250,250);
	
	load();
	
	gtk_builder_connect_signals(builder, NULL);
  	gtk_widget_show_all(windowPrincipal);
  	gtk_main ();
	
	return 0;
}
