/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef WINDOW_CPP
#define WINDOW_CPP

#include<gtk/gtk.h>
#include <typeinfo>

class Poligono;
class Ponto;

void button_window_right();
void button_window_left();
void button_window_up();
void button_window_down();
void button_window_in();
void button_window_out();

void ponto_adicionar();
void reta_adicionar();
void poligono_novo_ponto();
void poligono_adicionar();

#include "hpp/Poligono.hpp"
#include "hpp/Window.hpp"
#include "hpp/Reta.hpp"
#include "ListaEnc.cpp"

Window::Window(GtkBuilder *builder, int minX, int minY, int maxX, int maxY) {
	this->poligono = new Poligono(builder);
	this->lista = new ListaEnc<Ponto*>();
	
	this->xMin = minX;
	this->yMin = minY;
	
	this->xMax = maxX;
	this->yMax = maxY;
	
	this->builder = builder;
	this->inputPasso = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton_passo"));
	
	this->principal_load();
	this->secundaria_load();
}

void Window::add(Ponto *ponto) {
	this->lista->adiciona(ponto);
}
void Window::add_reta() {
	g_print("reta_adicionar\n");
	
	Reta *reta = new Reta(this->builder);
	reta->limpar_campos();
	
	this->lista->adiciona(reta);
}
void Window::add_ponto() {
	g_print("ponto_adicionar\n");
	
	Ponto *ponto = new Ponto(this->builder);
	ponto->limpar_campos();
	
	this->lista->adiciona(ponto);
}
void Window::add_poligono() {
	g_print("poligono_adicionar\n");
	
	this->add_ponto_poligono();
	this->lista->adiciona(poligono);
	
	poligono = new Poligono(this->builder);
}
void Window::add_ponto_poligono() {
	g_print("poligono_novo_ponto\n");
	
	this->poligono->add_ponto();
	this->poligono->limpar_campos();
}

void Window::deslocar_left() {
	int d;
	d = (-1) * atoi( gtk_entry_get_text(this->inputPasso) );
	
	this->xMin += d;
	this->xMax += d;
}
void Window::deslocar_right() {
	int d;
	d = atoi( gtk_entry_get_text(this->inputPasso) );
	
	this->xMin += d;
	this->xMax += d;
}
void Window::deslocar_up() {
	int d;
	d = atoi( gtk_entry_get_text(this->inputPasso) );
	
	this->yMin += d;
	this->yMax += d;
}
void Window::deslocar_down() {
	int d;
	d = (-1) * atoi( gtk_entry_get_text(this->inputPasso) );
	
	this->yMin += d;
	this->yMax += d;
}
void Window::zoom_in() {
	int d;
	d = atoi( gtk_entry_get_text(this->inputPasso) );
	
	this->xMin += d;
	this->yMin += d;
	
	this->xMax -= d;
	this->yMax -= d;
}
void Window::zoom_out() {
	int d;
	d = atoi( gtk_entry_get_text(this->inputPasso) );
	
	this->xMin -= d;
	this->yMin -= d;
	
	this->yMax += d;
	this->xMax += d;
}

int Window::get_min_x() {
	return this->xMin;
}
int Window::get_min_y() {
	return this->yMin;
}
int Window::get_max_x() {
	return this->xMax;
}
int Window::get_max_y() {
	return this->yMax;
}

ListaEnc<Ponto*>* Window::get_objetos() {
	return this->lista;
}

void Window::principal_load() {
	buttonRight = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button_window_right") );
	buttonLeft = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button_window_left") );
	buttonUp = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button_window_up") );
	buttonDown = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button_window_down") );
	
	buttonIn = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button_window_in") );
	buttonOut = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button_window_out") );
	
	g_signal_connect (buttonRight, "clicked", G_CALLBACK(button_window_right), NULL);
	g_signal_connect (buttonLeft, "clicked", G_CALLBACK(button_window_left), NULL);
	g_signal_connect (buttonUp, "clicked", G_CALLBACK(button_window_up), NULL);
	g_signal_connect (buttonDown, "clicked", G_CALLBACK(button_window_down), NULL);
	g_signal_connect (buttonIn, "clicked", G_CALLBACK(button_window_in), NULL);
	g_signal_connect (buttonOut, "clicked", G_CALLBACK(button_window_out), NULL);
}
void Window::secundaria_load() {
	poligonoAdicionar = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button_poligono_adicionar") );
	pontoAdicionar = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button_ponto_adicionar") );
	retaAdicionar = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button_reta_adicionar") );
	
	poligonoNovo = GTK_WIDGET( gtk_builder_get_object( GTK_BUILDER(builder), "button_poligono_novo") );
	
	g_signal_connect (poligonoAdicionar, "clicked", G_CALLBACK (poligono_adicionar), NULL);
	g_signal_connect (pontoAdicionar, "clicked", G_CALLBACK (ponto_adicionar), NULL);
	g_signal_connect (retaAdicionar, "clicked", G_CALLBACK (reta_adicionar), NULL);
	g_signal_connect (poligonoNovo, "clicked", G_CALLBACK (poligono_novo_ponto), NULL);
}
#endif
