/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef LISTAENC_CPP
#define LISTAENC_CPP

#include "hpp/ListaEnc.hpp"
#include "hpp/Elemento.hpp"
#include <cstdlib>

template<typename T>
ListaEnc<T>::ListaEnc() {
	this->head = NULL;
	this->size = 0;
}

template<typename T>
ListaEnc<T>::~ListaEnc() {
	this->destroiLista();
}

template<typename T>
void ListaEnc<T>::adicionaNoInicio(const T& dado) {
	Elemento<T> *novo, *prox;
	prox = NULL;
	novo = new Elemento<T>(dado, NULL);
	
	if (this->head != NULL) {
		prox = this->head;
	}
	
	this->head = novo;
	this->head->setProximo(prox);
	this->size++;
}
template<typename T>
T ListaEnc<T>::retiraDoInicio() {
	if (this->head == NULL) {
		throw "Erro! Não existe elemento na lista";
	}
	
	Elemento<T> *prox, *primeiro;
	primeiro = this->head;
	this->head = this->head->getProximo();
	
	this->size--;
	return primeiro->getInfo();
}
template<typename T>
void ListaEnc<T>::eliminaDoInicio() {
	Elemento<T> *removido;
	removido = this->retiraDoInicio();
	
	delete removido;
}


template<typename T>
void ListaEnc<T>::adicionaNaPosicao(const T& dado, int pos) {
	if (pos == 0) {
		this->adicionaNoInicio(dado);
		return;
	}
	
	// Se ultrapassar o limite, adiciona no fim
	if (pos > this->size) {
		pos = this->size;
	}
	
	Elemento<T> *novo, *anterior, *aux;
	anterior = NULL;
	aux = this->head;
	novo = new Elemento<T>(dado, NULL);
	
	int c;
	for (c = 0; c < pos; c++) {
		anterior = aux;
		aux = aux->getProximo();
		
		if (c == pos-1) {
			anterior->setProximo(novo);
			novo->setProximo(aux);
			this->size++;
			return;
		}
	}
	
	throw "Erro!";
}
template<typename T>
int ListaEnc<T>::posicao(const T& dado) const {
	Elemento<T> *aux;
	aux = this->head;
	
	int c;
	for (c = 0; c < this->size; c++) {
		if (aux->getInfo() == dado) {
			return c;
		}
		
		aux = this->aux->getProximo();
	}
	
	return -1;
}
template<typename T>
T* ListaEnc<T>::posicaoMem(const T& dado) const {
	Elemento<T> *aux;
	aux = this->head;
	
	int c;
	for (c = 0; c < this->size; c++) {
		if (aux->getInfo() == dado) {
			return aux->getInfo();
		}
		
		aux = this->aux->getProximo()->getInfo();
	}
	
	return NULL;
}
template<typename T>
bool ListaEnc<T>::contem(const T& dado) {
	return (this->posicao(dado) > -1);
}
template<typename T>
T ListaEnc<T>::retiraDaPosicao(int pos) {
	if (pos == 0) {
		return this->retiraDoInicio();
	}
	
	// Se ultrapassar o limite, adiciona no fim
	if (pos >= this->size) {
		throw "Posicao invalida";
	}
	
	Elemento<T> *anterior, *aux;
	anterior = NULL;
	aux = this->head;
	
	int c;
	for (c = 0; c < pos; c++) {
		anterior = aux;
		aux = aux->getProximo();
		
		if (c == pos-1) {
			anterior->setProximo(aux->setProximo());
			this->size--;
			return aux->getInfo();
		}
	}
	
	throw "Erro!";
}

template<typename T>
void ListaEnc<T>::adiciona(const T& dado) {
	this->adicionaNaPosicao(dado, this->size);
}
template<typename T>
T ListaEnc<T>::retira() { // Pronto
	this->retiraDaPosicao(this->size-1);
}

template<typename T>
T ListaEnc<T>::retiraEspecifico(const T& dado) {
	if (this->head == NULL) {
		throw "Lista vazia";
	}
	
	Elemento<T> *aux, anterior;
	aux = this->head;
	
	int c;
	for (c = 0; c < this->size; c++) {
		if (aux->getInfo() == dado) {
			if (c == 0) {
				return this->retiraDoInicio();
			}
			
			anterior->setProximo(aux->getProximo());
			return aux;
		}
		
		anterior = aux;
		aux = aux->getProximo()->getInfo();
	}
	
	throw "Erro! Não encontrado.";
}
template<typename T>
void ListaEnc<T>::adicionaEmOrdem(const T& data) {
	
}
template<typename T>
bool ListaEnc<T>::listaVazia() const {
	return (this->size == 0);
}
template<typename T>
bool ListaEnc<T>::igual(T dado1, T dado2) {
	return (dado1 == dado2);
}
template<typename T>
bool ListaEnc<T>::maior(T dado1, T dado2) {
	return (dado1 > dado2);
}
template<typename T>
bool ListaEnc<T>::menor(T dado1, T dado2) {
	return (dado1 < dado2);
}
template<typename T>
void ListaEnc<T>::destroiLista() {
	
}

template<typename T>
T ListaEnc<T>::get(int i) {
	int c;
	
	if (i == 0) {
		return this->head->getInfo();
	}
	
	Elemento<T> *aux;
	aux = this->head;
	
	for (c = 0; c < i; c++) {
		aux = aux->getProximo();
	}
	
	return aux->getInfo();
}
template<typename T>
int ListaEnc<T>::get_size() {
	return this->size;
}

#endif