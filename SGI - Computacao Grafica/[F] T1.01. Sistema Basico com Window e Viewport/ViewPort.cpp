/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef VIEWPORT_CPP
#define VIEWPORT_CPP

#include "hpp/ViewPort.hpp"
#include "hpp/Window.hpp"
#include "hpp/Ponto.hpp"
#include "ListaEnc.cpp"

ViewPort::ViewPort(GtkBuilder *builder, int minX, int minY, int maxX, int maxY) {
	this->xMin = minX;
	this->yMin = minY;
	
	this->xMax = maxX;
	this->yMax = maxY;
	
	this->inputPasso = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton_passo"));
}
void ViewPort::set_rgb(cairo_t* cairo, int red, int green, int blue) {
	double r, g, b;
	
	r = red/255;
	g = green/255;
	b = blue/255;
	
	cairo_set_source_rgb (cairo, r, g, b);
}
void ViewPort::limpar_tela(cairo_surface_t *surface) {
	cairo_t *cairo;
  	cairo = cairo_create (surface);

  	ViewPort::set_rgb(cairo, 255, 255, 255);
  	
	cairo_paint (cairo);
  	cairo_destroy (cairo);
}
void ViewPort::draw(cairo_surface_t *surface, Window *window) {
	this->limpar_tela(surface);
	
	ListaEnc<Ponto*> *lista;
	lista = window->get_objetos();
	
	int c;
	for (c = 0; c < lista->get_size(); c++) {
		Ponto *objeto;
		
		objeto = lista->get(c);
		objeto->draw(surface, window, this);
	}
}

int ViewPort::get_min_x() {
	return this->xMin;
}
int ViewPort::get_min_y() {
	return this->yMin;
}
int ViewPort::get_max_x() {
	return this->xMax;
}
int ViewPort::get_max_y() {
	return this->yMax;
}

double ViewPort::transformada_x(double xW, double xWMin, double xWMax, double xVMin, double xVMax) {
	//g_print("(xW - xWMin) / (xWMax - xWMin): %g\n", (double)(xW - xWMin) / (xWMax - xWMin));
	//g_print("(xW - xWMin) / (xWMax - xWMin) ) * (xVMax - xVMin): %g\n", (double)((xW - xWMin) / (xWMax - xWMin) ) * (xVMax - xVMin));
	//g_print("(xW - xWMin) * (xVMax - xVMin) : %g\n", (double)(xW - xWMin) * (xVMax - xVMin));
	//g_print("(xW - xWMin) * (xVMax - xVMin) ) / (xWMax - xWMin): %g\n", (double)((xW - xWMin) * (xVMax - xVMin) ) / (xWMax - xWMin));
	
	return ( (xW - xWMin) / (xWMax - xWMin) ) * (xVMax - xVMin);

	//return ((xW - xWMin) / (xWMax - xWMin)) * ((xVMax - xVMin));
}
double ViewPort::transformada_y(double yW, double yWMin, double yWMax, double yVMin, double yVMax) {
	//g_print("(yW - yWMin) / (yWMay - yWMin): %g\n", 						  (double)(yW - yWMin) / (yWMax - yWMin));
	//g_print("(yW - yWMin) / (yWMay - yWMin) ) * (yVMay - yVMin): %g\n", (double)((yW - yWMin) / (yWMax - yWMin) ) * (yVMax - yVMin));
	//g_print("1 - ( (yW - yWMin) / (yWMax - yWMin) ) ) * (yVMax - yVMin) : %g\n", 	(1 - ( (yW - yWMin) / (yWMax - yWMin) ) ) * (yVMax - yVMin));
	
	return ( 1 - ( (yW - yWMin) / (yWMax - yWMin) ) ) * (yVMax - yVMin);

	//return (yW - yWMin) / (yWMax - yWMin) * (yVMax - yVMin);
}

#endif



























