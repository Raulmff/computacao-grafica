/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef PONTO_CPP
#define PONTO_CPP

#include <gtk/gtk.h>
#include <cmath>

class Window;
class ViewPort;

#include "hpp/ViewPort.hpp"
#include "hpp/Window.hpp"
#include "hpp/Ponto.hpp"

Ponto::Ponto() {}
Ponto::Ponto(int x, int y) {
	this->objeto = PONTO;
	
	this->x = x;
	this->y = y;
}
Ponto::Ponto(GtkBuilder *builder) {
	this->objeto = PONTO;
	
	this->inputNome = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "entry_novo_ponto_nome"));
	this->inputX = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton_ponto_x"));
  	this->inputY = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton_ponto_y"));
	
	this->nome = gtk_entry_get_text(this->inputX);
	
	this->x = atoi(gtk_entry_get_text(this->inputX));
	this->y = atoi(gtk_entry_get_text(this->inputY));
}

int Ponto::get_x() {
	return this->x;
}
int Ponto::get_y() {
	return this->y;
}

void Ponto::draw(cairo_surface_t *surface, Window *window, ViewPort *viewport) {
	double x, y;
	x = ViewPort::transformada_x( this->x, window->get_min_x(), window->get_max_x(), viewport->get_min_x(), viewport->get_max_x() );
	y = ViewPort::transformada_y( this->y, window->get_min_y(), window->get_max_y(), viewport->get_min_y(), viewport->get_max_y() );
	
	cairo = cairo_create (surface);
	
	ViewPort::set_rgb(cairo, 0, 0, 0);
	
	cairo_set_line_width(cairo, 1);
	cairo_arc(cairo, x, y, 1, 0, 2 * M_PI);
	cairo_fill_preserve(cairo);
	cairo_stroke(cairo);
}
void Ponto::limpar_campos() {
	gtk_entry_set_text(this->inputX, "0");
	gtk_entry_set_text(this->inputY, "0");
}

#endif