/*
 * Copyright 2017.1
 * Universidade Federal de Santa Catarina
 * Disciplina: Computacao Grafica
 * Alunos: Raul Missfeldt Filho e Ricardo do Nascimento Boing
 */

#ifndef RETA_CPP
#define RETA_CPP

#include <gtk/gtk.h>

#include "hpp/ViewPort.hpp"
#include "hpp/Window.hpp"
#include "hpp/Reta.hpp"
#include "hpp/Ponto.hpp"

Reta::Reta(GtkBuilder *builder) : Ponto() {
	this->inputNome = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "entry_novo_ponto_nome"));
	
	this->inputX0 = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton_reta_x0"));
  	this->inputY0 = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton_reta_y0"));
	this->inputX = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton_reta_x"));
  	this->inputY = GTK_ENTRY(gtk_builder_get_object(GTK_BUILDER(builder), "spinbutton_reta_y"));
	
	this->nome = gtk_entry_get_text(this->inputX);
	this->objeto = RETA;
	
	this->x0 = atoi(gtk_entry_get_text(this->inputX0));
  	this->y0 = atoi(gtk_entry_get_text(this->inputY0));
	this->x = atoi(gtk_entry_get_text(this->inputX));
  	this->y = atoi(gtk_entry_get_text(this->inputY));
}
Reta::Reta(int x0, int y0, int x, int y) {
	this->objeto = RETA;
	
	this->x0 = x0;
  	this->y0 = y0;
	this->x = x;
  	this->y = y;
}

void Reta::draw(cairo_surface_t *surface, Window *window, ViewPort *viewport) {
	int x, y;
	x = (int) ViewPort::transformada_x( this->x, window->get_min_x(), window->get_max_x(), viewport->get_min_x(), viewport->get_max_x() );
	y = (int) ViewPort::transformada_y( this->y, window->get_min_y(), window->get_max_y(), viewport->get_min_y(), viewport->get_max_y() );
	
	int x0, y0;
	x0 = (int) ViewPort::transformada_x( this->x0, window->get_min_x(), window->get_max_x(), viewport->get_min_x(), viewport->get_max_x() );
	y0 = (int) ViewPort::transformada_y( this->y0, window->get_min_y(), window->get_max_y(), viewport->get_min_y(), viewport->get_max_y() );
	
	g_print("x0 %d | y0 %d | x %d | y %d \n", x0, y0, x, y);
	
	cairo_t *cairo;
  	cairo = cairo_create (surface);
	
	ViewPort::set_rgb(cairo, 0, 0, 0);
	
	cairo_set_line_width(cairo, 1);
	cairo_move_to(cairo, x0, y0);
	cairo_line_to(cairo, x, y);
	cairo_stroke(cairo);
}
void Reta::limpar_campos() {
	gtk_entry_set_text(this->inputX0, "0");
	gtk_entry_set_text(this->inputY0, "0");
	gtk_entry_set_text(this->inputX, "0");
	gtk_entry_set_text(this->inputY, "0");
}

#endif